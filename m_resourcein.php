﻿<?php require_once("include/web.config.php");

$Input = GDC( $_GET['c'] , 'mrin' );

$_ID		= $Input['mr_id'];

$_Result 		= $CM->GET_MR_LIST( $_ID );

$_html			= $_Result['Data'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];

$_Title = $_html[$_ID]['MR_Title'];
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="" rel="nofollow"  itemprop="url">
								<span itemprop="title">醫療及社區資源</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="m_resource.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">本鄉鎮醫療資源</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="m_resource.php" rel="nofollow"  itemprop="url">
								<span itemprop="title"><?=$_html[$_ID]['MR_Title']?></span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('resource_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">本鄉鎮醫療資源</h2>
					<div class="mainContent">
						<h3 class="photoTitle"><?=$_html[$_ID]['MR_Title']?></h3><br>
						<div class="textcontent">
							<?=TurnSymbol($_html[$_ID]['MR_Content'])?>
 	
						</div>
						<button class="btnstyle_return" onClick="location='m_resource.php';">回列表</button>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>