<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">中央內容區塊，為本頁主要內容區</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="agefriendly.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">健康主題</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="agefriendly.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">高齡友善</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('health_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">高齡友善</h2>
					<br>
					<div class="mainSearch nopd">
						<label for="searchgov">請輸入關鍵字查詢高齡友善</label>
						<input type="text" class="input__style01" id="searchgov" title="請輸入關鍵字查詢高齡友善" placeholder="請輸入關鍵字">
						<button>查詢</button>
					</div>
					<div class="text nopd">為配合行政院推動政府文件標準格式(ODF-CNS15251)，本網站提供民眾下載檔案以開放性PDF及ODF檔案格式為主 ，ODF檔案可於網路上免費下載自由軟體開啟編輯 ，如 LibreOffice(https://zh-tw.libreoffice.org) ，亦可用微軟Office2007(含)以上版本開啟。
					</div>
					
					<h3 class="photoTitle">高齡友善A</h3>
					<div class="mainContent nopd">
						<table class="mainTable">
							<tr>
								<th class="tb1"><span>日期</span></th>
								<th class="tb1"><span>類別</span></th>
								<th class="tb6"><span>標題</span></th>
							</tr>
							<tr>
								<td class="tb1" data-title="日期"><span>2019-06-28</span></td>
								<td class="tb1" data-title="類別"><span>醫療資源A</span></td>
								<td class="tb6 left" data-title="標題"><a href="agefriendlyin.php" title="「高齡友善健康照護機構表現指標」調查表">「高齡友善健康照護機構表現指標」調查表</a></td>
							</tr>
							<tr>
								<td class="tb1" data-title="日期"><span>2019-06-28</span></td>
								<td class="tb1" data-title="類別"><span>醫療資源A</span></td>
								<td class="tb6 left" data-title="標題"><a href="agefriendlyin.php" title="「高齡友善健康照護機構表現指標」調查表">「高齡友善健康照護機構表現指標」調查表</a></td>
							</tr>
							<tr>
								<td class="tb1" data-title="日期"><span>2019-06-28</span></td>
								<td class="tb1" data-title="類別"><span>醫療資源A</span></td>
								<td class="tb6 left" data-title="標題"><a href="agefriendlyin.php" title="「高齡友善健康照護機構表現指標」調查表">「高齡友善健康照護機構表現指標」調查表</a></td>
							</tr>
							<tr>
								<td class="tb1" data-title="日期"><span>2019-06-28</span></td>
								<td class="tb1" data-title="類別"><span>醫療資源A</span></td>
								<td class="tb6 left" data-title="標題"><a href="" title="「高齡友善健康照護機構表現指標」調查表">「高齡友善健康照護機構表現指標」調查表</a></td>
							</tr>
						</table>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>