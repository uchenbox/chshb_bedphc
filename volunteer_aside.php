<aside class="aside">
	<p class="aside__tit">志工專區</p>
	<ul class="mnav">
		<li class="mnav__item <?=PHP_NAME=='volunteer'?'current':''?>"><a href="volunteer.php" title="志工服務">志工服務</a></li>
		<li class="mnav__item <?=PHP_NAME=='volunteer_album'||PHP_NAME=='volunteer_albumin'?'current':''?>"><a href="volunteer_album.php" title="志工花絮">志工花絮</a></li>
	</ul>
</aside>