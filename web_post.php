<?php
require_once('include/web.config.php');

$json_array = array();
	
$_Type  = $_POST['_type'];//主執行case

ob_start();

if( !empty($_Type) ){
	
	$db 	= new MySQL();
	
	$Upload = new Upload();
	
	$sdate 	= date('Y-m-d H:i:s');
	
	switch( $_Type ){
		
		case "links_search":
			
			$_href			= trim($_POST['_href']);
			$_searchkey		= trim($_POST['_searchkey']);
			$_modekey		= trim($_POST['_modekey']);
			$_mode			= trim($_POST['_mode']);
			$_html_msg = '';

			if( empty($_searchkey) ){
				
				$_html_msg 	= '請輸入搜尋字串';
				break;
			}
			
			if( empty($_html_msg) ){
				
				$_html_href = $_href.'.php?c='.OEncrypt( $_modekey.'='.$_searchkey.'&mode=search' , $_mode );
			}else{
				
				$_html_href = $_href.'.php';
			}		
			
		break;
		
		case "dl_search":
			
			$_href			= trim($_POST['_href']);
			$_searchkey		= trim($_POST['_searchkey']);
			$_modekey		= trim($_POST['_modekey']);
			$_mode			= trim($_POST['_mode']);
			$_html_msg = '';

			if( empty($_searchkey) ){
				
				$_html_msg 	= '請輸入搜尋字串';
				break;
			}
			
			if( empty($_html_msg) ){
				
				$_html_href = $_href.'.php?c='.OEncrypt( $_modekey.'='.$_searchkey.'&mode=search' , $_mode );
			}else{
				
				$_html_href = $_href.'.php';
			}		
			
		break;
		
		case "gov_search":
			
			$_href			= trim($_POST['_href']);
			$_searchkey		= trim($_POST['_searchkey']);
			$_modekey		= trim($_POST['_modekey']);
			$_mode			= trim($_POST['_mode']);
			$_html_msg = '';

			if( empty($_searchkey) ){
				
				$_html_msg 	= '請輸入搜尋字串';
				break;
			}
			
			if( empty($_html_msg) ){
				
				$_html_href = $_href.'.php?c='.OEncrypt( $_modekey.'='.$_searchkey.'&mode=search' , $_mode );
			}else{
				
				$_html_href = $_href.'.php';
			}		
			
		break;
		
		case "health_search":
			
			$_href			= trim($_POST['_href']);
			$_searchkey1	= trim($_POST['_searchkey1']);
			$_searchkey2	= trim($_POST['_searchkey2']);
			$_modekey1		= trim($_POST['_modekey1']);
			$_modekey2		= trim($_POST['_modekey2']);
			$_mode			= trim($_POST['_mode']);
			$_html_msg = '';

			if( empty($_searchkey2) ){
				
				$_html_msg 	= '請輸入搜尋字串';
				break;
			}
			
			if( empty($_html_msg) ){
				
				$_html_href = $_href.'.php?c='.OEncrypt( $_modekey1.'='.$_searchkey1.'&'.$_modekey2.'='.$_searchkey2.'&mode=search' , $_mode );
			}else{
				
				$_html_href = $_href.'.php';
			}		
			
		break;
		
	}
}

//ob_end_clean();

if( $_Type != 'mailauth' ){
	
	$json_array['html_msg']     = $_html_msg ? $_html_msg : '';//訊息
	$json_array['html_href']    = $_html_href ? $_html_href : '';//連結
	$json_array['html_eval']    = $_html_eval ? $_html_eval : '';//確定後要執行的JS
	$json_array['html_content'] = $_html_content ? $_html_content : '';//輸出內容
	
	echo json_encode( $json_array );
}
?>