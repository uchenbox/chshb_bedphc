<?php require_once("include/web.config.php");


$Input = GDC( $_GET['c'] , 'newsin' );

$_ID		= explode('_',$Input['news_in']);
$news_cat	= $_ID[1];
$_Result 	= $CM->GET_NEWS_LIST( $_ID[0] , $news_cat , $PageData );

$_html			= $_Result['Data'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];

if( $news_cat=='new' ) {
	
	$_bread_h4 = "最新消息";
}else if( $news_cat=='hot' ){
	
	$_bread_h4 = "熱門議題";
}


$_Title = $_html[$_ID[0]]['News_Title'];
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="news.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">訊息專區</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="glory.php" rel="nofollow"  itemprop="url">
								<span itemprop="title"><?=$_bread_h4?></span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('news_aside.php')?>


				<div class="main">
					<h2 class="mainTitle"><?=$_bread_h4?></h2>
					<div class="mainContent">
						<h3 class="photoTitle"><?=$_html[$_ID[0]]['News_Title']?></h3>
						<p class="photoDate"><?=explode(" ",$_html[$_ID[0]]['News_PostDate'])[0]?></p>
						<div class="textcontent">
							<?=TurnSymbol($_html[$_ID[0]]['News_Content'])?>
						</div>
						<button class="btnstyle_return" onClick="location='news.php?c=<?=OEncrypt('news_cat='.$news_cat , 'newscat')?>';">回列表</button>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>