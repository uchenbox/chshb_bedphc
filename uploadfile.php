<?php require_once("include/web.config.php");
session_start();

#define('ROOT',dirname(__FILE__).'\\');
#
#$target_dir = ROOT."sys_files\kinmen\UploadFile\\";

define('ROOT',dirname(__FILE__));
$alert 	= '';
$target_dir = ROOT.DIRECTORY_SEPARATOR."sys_files".DIRECTORY_SEPARATOR."chshb_bedphc".DIRECTORY_SEPARATOR."contact".DIRECTORY_SEPARATOR;

$db = $db1 = new MySQL();
	
$name		= substr(trim($_POST['name']), 0, 30);//名字
$email		= trim($_POST['email']);//信箱
$tel		= substr(trim($_POST['tel']), 0, 20);//電話

$title		= trim($_POST['title']);
$content	= trim($_POST['content']);

$Verify 	= 'CT'.strtoupper(dechex(time()));

$formcode 	= trim($_POST['formcode']);


	
	$db_data = array(
		'Contact_Name'		=>$name,
		'Contact_Email'		=>$email,
		'Contact_Tel'		=>$tel,
		
		'Contact_Title'		=>$title,
		'Contact_Content'	=>$content,

		'Contact_Verify'	=>$Verify,
		'Contact_Sdate'		=>date('Y-m-d H:i:s')
	);
	
	$db->query_data('web_contact', $db_data, 'INSERT');
	$db_alert = !empty($db->error) ? '留言失敗, 請重新操作' : '留言成功，我們會盡快與您聯絡';


for( $i=0;$i<5;$i++){
	
	$target_file = $target_dir.$_FILES["file"]["name"][$i];

	$File_Name 		= $_FILES["file"]["name"][$i];//檔案名
	$File_Extension = pathinfo( $_FILES["file"]["name"][$i], PATHINFO_EXTENSION );//副檔名

	$ex = explode( '.',$File_Name);

	$File = $ex[0]."_".strtotime("now").".".$File_Extension;

	if( $_FILES["file"]["name"][$i]!='' && $_FILES["file"]["size"][$i] <= '5242880‬' && ($_FILES["file"]["type"][$i]=='application/pdf'||$_FILES["file"]["type"][$i]=='image/jpeg'||$_FILES["file"]["type"][$i]=='image/png'||$_FILES["file"]["type"][$i]=='image/bmp') ){
		
		// $TF = move_uploaded_file($_FILES["file"]["tmp_name"][$i], ICONV_CODE('UTF8_TO_BIG5', $target_dir.$File ) );
		// $TF = move_uploaded_file( $_FILES["file"]["tmp_name"][$i], $target_dir.$File );
		if( move_uploaded_file( $_FILES["file"]["tmp_name"][$i], ICONV_CODE('UTF8_TO_BIG5', $target_dir.$File) ) ){

			if( $_FILES["file"]["name"][$i]!=''/*  && $i==0 */ ){
				
				$db_data1 = array('Contact_File'.$i=> $File);
			}
			$db1->Where = " WHERE Contact_Verify = '" .$Verify. "'";
			$db1->query_data( 'web_contact' , $db_data1, 'UPDATE');
		}

	}else if( $_FILES["file"]["name"][$i]!='' && ($_FILES["file"]["type"][$i] != 'application/pdf‬'||$_FILES["file"]["type"][$i] != 'image/png‬'||$_FILES["file"]["type"][$i] != 'image/jpeg‬'||$_FILES["file"]["type"][$i] != 'image/bmp‬') ){
		
		$alert = $_FILES["file"]["name"][$i]."的".$_FILES["file"]["type"][$i].'格式為非可上傳之檔案格式';
	}else if( $_FILES["file"]["size"][$i] > '5242880‬' ){
		
		$alert = $_FILES["file"]["size"][$i].'檔案太大';
	}else if( $_FILES["file"]["name"][$i]!='' && !empty($_FILES["file"]["error"][$i]) ){
		
		$alert = $_FILES["file"]["error"][$i].'上傳發生錯誤';
	// }else{
		
		// $alert = '重新操作';
		// print_r($_FILES);
		// exit;
	}
}

if( !empty($alert) ){
?>
	<!doctype html>
	<html>
	<head> 
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<title>上傳錯誤 - 錯誤</title>
	</head>

	<body>
	<script type="text/javascript">
		alert('<?=$alert?>');
		window.history.go(-1);
	</script>
	</body>
	</html>
<?php 
}else if( !empty($db_alert) ){
?>
	<!doctype html>
	<html>
	<head> 
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<title>上傳完成</title>
	</head>

	<body>
	<script type="text/javascript">
		alert('<?=$db_alert?>');
		window.location.href = 'contact.php';
	</script>
	</body>
	</html>
<?php 
}

	if( !empty($db->Error)){
					
		$alert = '重新操作';
	}else{
		
		header('Location:contact.php');
	}
	if( !empty($db->Error)||!empty($db1->Error) ){
			
		$alert1	= '留言失敗, 請重新操作';
	}else{
		
		$FromName = !empty($_setting_['WO_Title']) ? $_setting_['WO_Title'] : '本網站';
		$FromName1 = '所信箱';
		$FromName2 = '承辦人';
		
		//收件人姓名 收件人信箱
		$mailto = array(
			$name => $email
		);
		
		$sbody = get_MailBody('custom_contact',$Verify);
		
		//寄給客戶
		$Send_TF = SendMail($_setting_, '您在 '.$FromName. ' 留言內容, 此信件為系統發出信件，請勿直接回覆', $sbody, $mailto);
		
		$alert1 	= '留言成功， 我們會盡快與您聯絡';
		
		if( !empty($_setting_['WO_Email']) ){
			
			$mailto = array(
				$FromName1 => $_setting_['WO_Email'],
				$FromName2 => $_setting_['WO_UTMail']
			);
			
			$sbody = get_MailBody('admin_contact',$Verify);
			
			//寄給管理者
			$Send_TF = SendMail($_setting_, '您有新的留言內容, 此信件為系統發出信件，請勿直接回覆', $sbody, $mailto);
		}
	}

?>
<script>

$(document).ready(function(e) {
	
	loading('close');

});

</script>