﻿<?php require_once("include/web.config.php");

$_Result 		= $CM->GET_OPC_LIST( $_ID );

$_htmlA			= $_Result['DataA'];
$_htmlP			= $_Result['DataP'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];

$_Title = "門診資訊";
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="opc_info.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">便民服務</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="opc_info.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">門診資訊</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('service_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">門診資訊</h2>
					<div class="mainContent nopd">
						<table class="opcTable">
							<tr>
								<th class="tb1 center"><span>時段</span></th>
								<th class="tb1 center"><span>星期一</span></th>
								<th class="tb1 center"><span>星期二</span></th>
								<th class="tb1 center"><span>星期三</span></th>
								<th class="tb1 center"><span>星期四</span></th>
								<th class="tb1 center"><span>星期五</span></th>
								<?php if( !empty($_htmlA['19070804']['OPs_Content']) || !empty($_htmlP['19070803']['OPs_Content']) ){?>
									<th class="tb1 center"><span>星期六</span></th>
								<?php }?>
								<?php if( !empty($_htmlA['19070802']['OPs_Content']) || !empty($_htmlP['19070801']['OPs_Content']) ){?>
									<th class="tb1 center"><span>星期日</span></th>
								<?php }?>
							</tr>
							<tr>
								<td class="tb1 center time" data-title="上午"><span>上午</span></td>
								<td class="tb1" data-title="<?=substr($_htmlA['19070814']['OPs_Name'],0,-6)?>"><span><?=nl2br($_htmlA['19070814']['OPs_Content'])?></span></td>
								<td class="tb1" data-title="<?=substr($_htmlA['19070812']['OPs_Name'],0,-6)?>"><span><?=nl2br($_htmlA['19070812']['OPs_Content'])?></span></td>
								<td class="tb1" data-title="<?=substr($_htmlA['19070810']['OPs_Name'],0,-6)?>"><span><?=nl2br($_htmlA['19070810']['OPs_Content'])?></span></td>
								<td class="tb1" data-title="<?=substr($_htmlA['19070808']['OPs_Name'],0,-6)?>"><span><?=nl2br($_htmlA['19070808']['OPs_Content'])?></span></td>
								<td class="tb1" data-title="<?=substr($_htmlA['19070806']['OPs_Name'],0,-6)?>"><span><?=nl2br($_htmlA['19070806']['OPs_Content'])?></span></td>
								<?php if( !empty($_htmlA['19070804']['OPs_Content']) || !empty($_htmlP['19070803']['OPs_Content']) ){?>
									<td class="tb1" data-title="<?=substr($_htmlA['19070804']['OPs_Name'],0,-6)?>"><span><?=!empty($_htmlA['19070804']['OPs_Content'])?nl2br($_htmlA['19070804']['OPs_Content']):''?></span></td>
								<?php }?>
								<?php if( !empty($_htmlA['19070802']['OPs_Content']) || !empty($_htmlP['19070801']['OPs_Content']) ){?>
									<td class="tb1" data-title="<?=substr($_htmlA['19070802']['OPs_Name'],0,-6)?>"><span><?=!empty($_htmlA['19070802']['OPs_Content'])?nl2br($_htmlA['19070802']['OPs_Content']):''?></span></td>
								<?php }?>
								<!--<?php foreach( $_htmlA as $key => $val ){?>
									<?php if(!empty($val['OPs_Content'])){?>
										<td class="tb1" data-title="<?=substr($val['OPs_Name'],0,-6)?>"><span><?=nl2br($val['OPs_Content'])?></span></td>
									<?php }?>
								<?php }?>-->
							</tr>
							<tr>
								<td class="tb1 center time" data-title="下午"><span>下午</span></td>
								<td class="tb1" data-title="<?=substr($_htmlP['19070813']['OPs_Name'],0,-6)?>"><span><?=nl2br($_htmlP['19070813']['OPs_Content'])?></span></td>
								<td class="tb1" data-title="<?=substr($_htmlP['19070811']['OPs_Name'],0,-6)?>"><span><?=nl2br($_htmlP['19070811']['OPs_Content'])?></span></td>
								<td class="tb1" data-title="<?=substr($_htmlP['19070809']['OPs_Name'],0,-6)?>"><span><?=nl2br($_htmlP['19070809']['OPs_Content'])?></span></td>
								<td class="tb1" data-title="<?=substr($_htmlP['19070807']['OPs_Name'],0,-6)?>"><span><?=nl2br($_htmlP['19070807']['OPs_Content'])?></span></td>
								<td class="tb1" data-title="<?=substr($_htmlP['19070805']['OPs_Name'],0,-6)?>"><span><?=nl2br($_htmlP['19070805']['OPs_Content'])?></span></td>
								<?php if( !empty($_htmlA['19070804']['OPs_Content']) || !empty($_htmlP['19070803']['OPs_Content']) ){?>
									<td class="tb1" data-title="<?=substr($_htmlP['19070803']['OPs_Name'],0,-6)?>"><span><?=!empty($_htmlP['19070803']['OPs_Content'])?nl2br($_htmlP['19070803']['OPs_Content']):''?></span></td>
								<?php }?>
								<?php if( !empty($_htmlA['19070802']['OPs_Content']) || !empty($_htmlP['19070801']['OPs_Content']) ){?>
									<td class="tb1" data-title="<?=substr($_htmlP['19070801']['OPs_Name'],0,-6)?>"><span><?=!empty($_htmlP['19070801']['OPs_Content'])?nl2br($_htmlP['19070801']['OPs_Content']):''?></span></td>
								<?php }?>
								<!--<?php foreach( $_htmlP as $key => $val ){?>
									<?php if(!empty($val['OPs_Content'])){?>
										<td class="tb1" data-title="<?=substr($val['OPs_Name'],0,-6)?>"><span><?=nl2br($val['OPs_Content'])?></span></td>
									<?php }?>
								<?php }?>-->
							</tr>
							
						</table>

						<br><br>
						
						<div class="textcontent">
							<?=TurnSymbol($_setting2_['Setting_OutpatientInfo'])?>
						</div>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>