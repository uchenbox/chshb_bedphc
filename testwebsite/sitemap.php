<?php require_once("include/web.config.php");

$_Title = "網站導覽";

?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="sitemap.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">網站導覽</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				


				<div class="main main--sitemap">
					<h2 class="mainTitle">網站導覽</h2>
					<div class="mainContent nopd">
						<div class="sitemap__header">
							<p>本網站依無障礙網頁設計原則建置，網站的主要內容分為三大區塊：</p>
							<p>1. 上方功能區塊、2. 中央內容區塊、3. 下方選單連結區塊</p>
							<p>本網站的快速鍵﹝Accesskey﹞設定如下：</p>
							<p>Alt+U：選單連結區塊，此區塊列有本網站主要連結。</p>
							<p>Alt+M：中央內容區塊，為本頁主要內容區。</p>
							<p>Alt+Z：頁尾網站資訊。</p>
						
						</div>

						<ul class="sitemap__list">
							<li>
								<a href="about.php" title="中心簡介">1. 中心簡介</a>
								<ul>
									<li><a href="about.php" title="本所簡介">1-1.本所簡介</a></li>
									<li><a href="business.php" title="業務職掌">1-2.業務職掌</a></li>
									<li><a href="glory.php" title="榮耀軌跡">1-3.榮耀軌跡</a></li>
									<li><a href="headman.php" title="歷任首長">1-4.歷任首長</a></li>
									<li><a href="traffic.php" title="交通資訊">1-5.交通資訊</a></li>
									<li><a href="album.php" title="活動花絮">1-6.活動花絮</a></li>
									<li><a href="<?=$_setting2_['Setting_OverviewofPHI']?>" title="另開新視窗前往<?=$_setting_['WO_Title']?>公衛指標概覽">1-7.公衛指標概覽</a></li>
									<li><a href="contact.php" title="民意信箱">1-8.民意信箱</a></li>
								</ul>
							</li>
							<li>
								<a href="m_resource.php" title="醫療及社區資源">2. 醫療及社區資源</a>
								<ul>
									<li><a href="m_resource.php"  title="本鄉鎮醫療資源">2-1.本鄉鎮醫療資源</a></li>
									<li><a href="c_resource.php"  title="社區資源">2-2.社區資源</a></li>
								</ul>
							<li>
								<a href="opc_info.php" title="便民服務">3. 便民服務</a>
								<ul>
									<li><a href="opc_info.php" title="門診資訊">3-1.門診資訊</a></li>
									<li><a href="service_process.php" title="為民服務流程">3-2.為民服務流程</a></li>
									<?php if( $GOV_TF==true ){?>
										<li><a href="gov_info.php" title="政府公開資訊">3-3.政府公開資訊</a></li>
									<?php }?>
									<li><a href="service_plan.php" title="服務品質計畫">3-4.服務品質計畫</a></li>
									<li><a href="download_all.php" title="檔案下載">3-5.檔案下載</a></li>
									<?php if( $VIDEO_TF==true ){?>
										<li><a href="video.php" title="影音資料">3-6.影音資料</a></li>
									<?php }?>
									<li><a href="links.php" title="網網相連">3-7.網網相連</a></li>
								</ul>
							</li>
							<li>
								<a href="news.php?c=<?=OEncrypt('news_cat=new' , 'newscat')?>" title="訊息專區">4. 訊息專區</a>
								<ul>
									<li><a href="news.php?c=<?=OEncrypt('news_cat=new' , 'newscat')?>" title="最新消息">4-1. 最新消息</a></li>
									<li><a href="news.php?c=<?=OEncrypt('news_cat=hot' , 'newscat')?>" title="熱門議題">4-2. 熱門議題</a></li>
								</ul>
							</li>
							<li>
								<a href="volunteer.php" title="志工專區">5. 志工專區</a>
								<ul>
									<li><a href="volunteer.php" title="志工服務">5-1. 志工服務</a></li>
									<li><a href="volunteer_album.php" title="志工花絮">5-2. 志工花絮</a></li>
								</ul>
							<li>
								<a href="" title="健康主題">6. 健康主題</a>
								<ul>
									<?php $i=1;?>
									<?php foreach( $_html_Ha as $keya => $vala ){?>
										<li><a href="health.php?c=<?=OEncrypt('health_cat='.$keya.'_all' , 'healthcat')?>" title="<?=$vala['TopicC_Name']?>">6-<?=$i?>. <?=$vala['TopicC_Name']?></a></li>
										<?php $i++;?>
									<?php }?>
								</ul>
							</li>

						</ul>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>