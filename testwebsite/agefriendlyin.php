<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">中央內容區塊，為本頁主要內容區</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="agefriendly.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">健康主題</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="agefriendly.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">高齡友善</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="agefriendly.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">「高齡友善健康照護機構表現指標」調查表</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('health_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">高齡友善</h2>
					<div class="mainContent">
						<h3 class="photoTitle">「高齡友善健康照護機構表現指標」調查表</h3>
						<p class="photoDate">2019-07-08</p>
						<div class="textcontent">
							二水鄉衛生所簡介<br>二水鄉概況<br>地理位置二水鄉位於彰化縣的東南端，本鄉東北依八卦山脈與南投縣交界，南隔濁水溪與雲林縣林內鄉相望，東端接南投縣名間鄉和竹山鎮，西臨溪州鄉北與田中鎮為鄰。<br><br>行政區劃二水鄉現有行政區共劃分為十七個村，分別為二水、聖化、文化、惠民、裕民、上豐、合和、十五、復興、合興、源泉、倡和、大園及修仁等十七村。<br><br>107年初人口分析本鄉轄區內現有住戶5,886戶，總人口數為15,325，其人口年增率皆為負成長，自然人口增加速度不及人口遷出速度。
						</div>
						<button class="btnstyle_return">回列表</button>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>