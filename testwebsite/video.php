<?php require_once("include/web.config.php");

$PageData['p'] = $_GET['p'];

$_Result 		= $CM->GET_VIDEO_LIST( $PageData );

$Pages_Data 	= $_Result['PageData'];
$_html			= $_Result['Data'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];

$_Title = "影音資料";
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="opc_info.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">便民服務</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="video.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">影音資料</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('service_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">影音資料</h2>
					<div class="mainContent nopd">
						<ul class="videolist">
							<?php foreach( $_html as $key => $val ){?>
								<li class="videolist__item">
									<div class="videolist__item__link" title="<?=$val['Video_Title']?>" href="">
										<iframe src="https://www.youtube.com/embed/<?=$val['Video_YouTube']?>" frameborder="0" allowfullscreen="" title="<?=$val['Video_Title']?>"></iframe>
									</div>
									<div class="date"><?=$val['Video_Postdate']?></div>
									<div class="tit"><?=$val['Video_Title']?></div>
									<div class="remark">影片來源頻道：<a  target="_blank" href="<?=$val['Video_Link']?>" title="另開新視窗前往<?=$val['Video_ChannelName']?>"><?=$val['Video_ChannelName']?></a></div>
								</li>
							<?php }?>
						</ul>
					</div>
					
					<?php require('page.php')?>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>