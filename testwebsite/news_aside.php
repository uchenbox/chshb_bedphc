<aside class="aside">
	<p class="aside__tit">訊息專區</p>
	<ul class="mnav">
		<li class="mnav__item <?=$news_cat=='new'?'current':''?>"><a href="news.php?c=<?=OEncrypt('news_cat=new' , 'newscat')?>" title="最新消息">最新消息</a></li>
		<li class="mnav__item <?=$news_cat=='hot'?'current':''?>"><a href="news.php?c=<?=OEncrypt('news_cat=hot' , 'newscat')?>" title="熱門議題">熱門議題</a></li>
	</ul>
</aside>