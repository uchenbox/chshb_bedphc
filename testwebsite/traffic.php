﻿<?php require_once("include/web.config.php");

$_Title = "交通資訊";

?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="about.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">關於本所</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="traffic.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">交通資訊</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('about_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">交通資訊</h2>
					<div class="mainContent traffic">
						<div class="traffic__imfo">
							<p><?=$_setting_['WO_Name']?></p>
							<address>地址：<?=$_setting_['WO_Addr'].$_setting_['WO_Addr1'].$_setting_['WO_Addr2']?></address>
							<p>電話：<?=$_setting_['WO_Tel']?></p>
							<p>傳真：<?=$_setting_['WO_Fax']?></p>
							<p>e-mail：<?=$_setting_['WO_Email']?></p>
						</div>

						<div class="traffic__map">
							<iframe  title="<?=$_setting_['WO_Addr'].$_setting_['WO_Addr1'].$_setting_['WO_Addr2']?>" src="<?=$_setting_['WO_GMAP']?>" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>

						<div class="traffic__text">
							<div class="textcontent">
								<?=TurnSymbol($_setting_['WO_TrafficInfo'])?>
							</div>
						</div>

						
						
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>