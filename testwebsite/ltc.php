<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">中央內容區塊，為本頁主要內容區</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="agefriendly.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">健康主題</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="ltc.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">長期照護-C據點</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('health_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">長期照護-C據點</h2>
					<br>
					<div class="mainSearch nopd">
						<label for="searchgov">請輸入關鍵字查詢長期照護-C據點</label>
						<input type="text" class="input__style01" id="searchgov" title="請輸入關鍵字查詢長期照護-C據點" placeholder="請輸入關鍵字">
						<button>查詢</button>
					</div>
					<div class="text nopd">社區健康營造主題介紹社區健康營造主題介紹社區健康營造主題介紹社區健康營造主題介紹社區健康營造主題介紹社區健康營造主題介紹社區健康營造主題介紹社區健康營造主題介紹社區健康營造主題介紹社區健康營造主題介紹社區健康營造主題介紹社區健康營造主題介紹。
					</div>
					
					<h3 class="photoTitle">長期照護-C據點A</h3>
					<div class="mainContent nopd">
						<table class="mainTable">
							<tr>
								<th class="tb1"><span>日期</span></th>
								<th class="tb1"><span>類別</span></th>
								<th class="tb6"><span>標題</span></th>
							</tr>
							<tr>
								<td class="tb1" data-title="日期"><span>2019-06-28</span></td>
								<td class="tb1" data-title="類別"><span>長期照護-C據點A</span></td>
								<td class="tb6 left" data-title="標題"><a href="ltcin.php" title="【彰化縣 彰化市】巷弄長照站- C 級據點">【彰化縣 彰化市】巷弄長照站- C 級據點</a></td>
							</tr>
							<tr>
								<td class="tb1" data-title="日期"><span>2019-06-28</span></td>
								<td class="tb1" data-title="類別"><span>長期照護-C據點A</span></td>
								<td class="tb6 left" data-title="標題"><a href="ltcin.php" title="【彰化縣 彰化市】巷弄長照站- C 級據點">【彰化縣 彰化市】巷弄長照站- C 級據點</a></td>
							</tr>
							<tr>
								<td class="tb1" data-title="日期"><span>2019-06-28</span></td>
								<td class="tb1" data-title="類別"><span>長期照護-C據點A</span></td>
								<td class="tb6 left" data-title="標題"><a href="ltcin.php" title="【彰化縣 彰化市】巷弄長照站- C 級據點">【彰化縣 彰化市】巷弄長照站- C 級據點</a></td>
							</tr>
							<tr>
								<td class="tb1" data-title="日期"><span>2019-06-28</span></td>
								<td class="tb1" data-title="類別"><span>長期照護-C據點A</span></td>
								<td class="tb6 left" data-title="標題"><a href="ltcin.php" title="【彰化縣 彰化市】巷弄長照站- C 級據點">【彰化縣 彰化市】巷弄長照站- C 級據點</a></td>
							</tr>
						</table>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>