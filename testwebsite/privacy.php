<?php require_once("include/web.config.php");

$_Title = "隱私權保護及資訊安全宣告";
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="privacy.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">隱私權保護及資訊安全宣告</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				


				<div class="main main--sitemap">
					<h2 class="mainTitle">隱私權保護及資訊安全宣告</h2>
					<div class="mainContent nopd">
						<div class="textcontent">
							<?=TurnSymbol($_setting2_['Setting_Privacy'])?>
						
						</div>

						
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>