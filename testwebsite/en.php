<?php require_once("include/web.config.php");

$_Title = "About Us";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php require('head.php')?>
	<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />
	<noscript>
		<style>
			body::before{
	    content: 'Your browser does not support JavaScript. Turn on JavaScript support in your browser. If you need to return to previous browsers can use Alt + left arrow key of (←) shortcuts';
		}
		</style>
	</noscript>
	
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		
		<header id="header">
			<div class="header__top container">
				<a class="go_main"  href="#Accesskey_M" title="Skip to main content">Skip to main content.</a>
				
				<h1 class="en"><a href="en.php" title="<?=$_setting2_['Setting_Title_EN']?>"><img src="images/logo.png" alt=""><?=$_setting2_['Setting_Title_EN']?></a></h1>
				<button class="mobile_btn">MENU
					<div id="nav-icon4"> <span></span> <span></span> <span></span> </div>
				</button>
				<div class="header__top__link">
					<div class="top">
						<a id="Accesskey_U" accesskey="U" href="#Accesskey_U" title="Upper block, containing the links to the services of this site, search box, font size setting and versions of this site.">:::</a><a href="index.php" title="Link to the Chinese website of <?=$_setting2_['Setting_Title_EN']?>">中文版</a>	|<a   title="sitemap" href="#en_sitemap">Site Map</a>
					</div>

					
				</div>
			</div>


			<nav class="header__nav">
				<div class="container">
					<ul class="nav">
						<li class="nav__item"><a class="nav__item__a" href="en.php"  title="About Us">About Us</a></li>
					</ul>
				</div>
			</nav>
		</header>
		<article class="layout" id="main">
			
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="The middle content zone in which contents of all web pages are exhibited.">:::</a>
				</section>
				

				<section class="mainbody">
				
				<div class="main main--en">
							
					<h2 class="maintit maintit--en">About Us</h2>


					<div class="textcontent"><?=TurnSymbol($_setting2_['Setting_AboutUs'])?></div><br><br><br>


					<h2 class="maintit maintit--en" id="en_sitemap">Site Map</h2>

					<div class="sitemap__header">
							<p>This website is based on the design principle of accessibility and uses keyboard shortcuts to strengthen the browsing convenience Layout is divided in 3 areas：</p>
							<p>1. the main menu zone at the top、2. the content zone at the center、3. the related information zone at the bottom</p>
							<p>The following keys are set as the Access keys ( or speed keys) of this website.：</p>
							<p>Alt+U：Upper block, containing the links to the services of this site, search box, font size setting and versions of this site.</p>
							<p>Alt+M：Main content area shows the content of each page.</p>
							<p>Alt+Z：Bottom Link area.</p>
							<p>If your browser is Firefox, Press Shift + Alt + Key to jump. For example, press "Shift + Alt + C" to jump to the center of the page.</p>
						
						</div>

						<ul class="sitemap__list">
							<li><a href="en.php" title="About Us">1. About Us</a></li>
						</ul>


				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<footer>
			<div class="footerLink">
				<div class="container">
					<button class="footerBtn footerBtn--en" data-switch="1" title="Close Menu">▼ Close Menu</button>
					<ul class="footerLink__list footerLink__list--en">
						<li class="footerLink__list__item">
							<p>About Us</p>
							<a href="en.php" title="About Us">About Us</a>
						</li>
						
					</ul>
				</div>
			</div>
			<div class="footer">
				<div class="container">
					<a id="Accesskey_Z" accesskey="Z" href="#Accesskey_Z" title="Fooer Information,this section lists the contact information of this website">:::</a>
					</br></br>
					<div class="footer__imfo">
						<p>© 2019 <?=$_setting2_['Setting_Title_EN']?></p>
						<p>
							<span><?=$_setting2_['Setting_Addr_EN']?></span><br>
							<span>Tel：<?=$_setting_['WO_Tel']?> </span>
							<span>Fax：<?=$_setting_['WO_Fax']?></span>
							<span>E-mail：<?=$_setting_['WO_Email']?></span>
						</p>
					</div>
					<!--<div class="AA">
						<a target="__blank"  href="" title="Web Priority AA Accessibility Approval(open new window)"><img src="images/AA.png" alt="Web Priority AA Accessibility Approval"></a>
					</div>-->
					
				</div>
			</div>

		</footer>


	</div>
	
</body>
</html>