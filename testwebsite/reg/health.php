<?php require_once("include/web.config.php");

$Input = GDC( $_GET['c'] , 'healthcat' );

$Input		= explode('_',$Input['health_cat']);

$health_cat		= $Input[0];
$tmp			= $Input[1];
/*==============================================*/
$Input1 = GDC( $_GET['c'] , 'healthcat' );

$cat		= explode('_',$Input1['cat']);
$cats		= $cat[0];
$tmps		= $cat[1];
$search		= $Input1['health_searchwords'];
$mode		= $Input1['mode'];
/*==============================================*/
if( $tmp=='all' ){
	
	$_Result 		= $CM->GET_HEALTH_CAT1( $health_cat );
	$_Cat1			= $_Result['Data'];
	
	$PageData['p'] = $_GET['p'];
	$_Result 		= $CM->GET_HEALTH_LIST( $_ID , $health_cat , $PageData );

	$Pages_Data 	= $_Result['PageData'];
	$_html			= $_Result['Data'];
	
}else if( $tmps=='all' ){
	
	$_Result 		= $CM->GET_HEALTH_CAT1( $cats );
	$_Cat1			= $_Result['Data'];
	
		
	$_Result 		= $CM->GET_HEALTH_SEARCH( $search , $cats );
	$_html			= $_Result['Data'];

}else if( !empty($health_cat) ){
	
	$_Result 		= $CM->GET_HEALTH_CAT2( $health_cat );
	$_Cat2			= $_Result['Data']['Data'];
	$_Lv2Name		= $_Result['Data']['lv2name'];
	$_Lv2ID			= $_Result['Data']['lv2id'];
	
	$PageData['p'] = $_GET['p'];
	$_Result 		= $CM->GET_HEALTH_LIST( $_ID , $health_cat , $PageData );

	$Pages_Data 	= $_Result['PageData'];
	$_html			= $_Result['Data'];
	
}else if( !empty($cats) ){
	
	$_Result 		= $CM->GET_HEALTH_CAT2( $cats );
	$_Cat2			= $_Result['Data']['Data'];
	$_Lv2Name		= $_Result['Data']['lv2name'];
	$_Lv2ID			= $_Result['Data']['lv2id'];
	
	$_Result 		= $CM->GET_HEALTH_SEARCH( $search , $cats );
	$_html			= $_Result['Data'];
	
}



$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];

if( $tmp=='all' ){
	
	$_Title = $_Cat1['TopicC_Name'];
}else if( $tmps=='all' ){
	
	$_Title = $_Cat1['TopicC_Name'];
}else{
	
	$_Title = $_Cat2['TopicC_Name'];
}
// $_Title = $tmp=='all'?$_Cat1['TopicC_Name']:$_Cat2['TopicC_Name'];
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="health.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">健康主題</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="" rel="nofollow"  itemprop="url">
								<span itemprop="title"><?=$_Title?></span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
					<?php require('health_aside.php')?>

					
					<div class="main">
						<h2 class="mainTitle"><?=$_Title?></h2>
						<br>
						<form id="forms" onSubmit="return false;">
							<div class="mainSearch nopd">
								<label for="searchhealth">請輸入關鍵字查詢<?=$_Cat1['TopicC_Name']?></label>
								<input type="text" class="input__style01" id="searchhealth" title="請輸入關鍵字查詢<?=$_Cat1['TopicC_Name']?>" placeholder="請輸入關鍵字" input-type="special" value="<?=$search?>">
								<button class="searchhealthbtn">查詢</button>
							</div>
							<?php if( $tmp=='all' ){?>
								
								<input type="hidden" id="cat" name="cat" value="<?=$tmp!='all'?$_Lv2ID:$_Cat1['TopicC_ID'].'_all'?>">
							<?php }else if( $tmps=='all' ){?>
								<input type="hidden" id="cat" name="cat" value="<?=$tmps!='all'?$_Lv2ID:$_Cat1['TopicC_ID'].'_all'?>">
							<?php }else if( ($tmp!='all'||$tmps!='all')&&(!empty($health_cat)||!empty($cats)) ){?>
								<input type="hidden" id="cat" name="cat" value="<?=$_Lv2ID?>">
							<?php }?>
							
							
						</form>
						<?php if( $tmp=='all'&&!isset($mode) ){?>
							<div class="text nopd"><?=TurnSymbol($_Cat1['TopicC_Content'])?>
							</div>
						
							<!--<h3 class="photoTitle"><?=$tmp=='all'?'':$_Lv2Name?></h3>-->
							<div class="mainContent nopd">
								<table class="mainTable">
									<tr>
										<th class="tb1"><span>日期</span></th>
										<th class="tb1"><span>類別</span></th>
										<th class="tb6"><span>標題</span></th>
									</tr>
									<?php foreach( $_html as $key => $val ){?>
										<tr>
											<td class="tb1" data-title="日期"><span><?=explode(" ",$val['Topic_Postdate'])[0]?></span></td>
											<td class="tb1" data-title="類別"><span><?=$val['TopicC_Name']?></span></td>
											<td class="tb6 left" data-title="標題"><a href="healthin.php?c=<?=OEncrypt('health_in='.$key.'_'.$_Title , 'healthin')?>" title="<?=$val['Topic_Title']?>"><?=$val['Topic_Title']?></a></td>
										</tr>
									<?php }?>
								</table>
							</div>
						<?php }else if( $tmps=='all'&&$mode=='search' ){?>
							<div class="text nopd"><?=TurnSymbol($_Cat1['TopicC_Content'])?>
							</div>
						
							<!--<h3 class="photoTitle"><?=$tmp=='all'?'':$_Lv2Name?></h3>-->
							<div class="mainContent nopd">
								<table class="mainTable">
									<tr>
										<th class="tb1"><span>日期</span></th>
										<th class="tb1"><span>類別</span></th>
										<th class="tb6"><span>標題</span></th>
									</tr>
									<?php foreach( $_html as $key => $val ){?>
										<tr>
											<td class="tb1" data-title="日期"><span><?=explode(" ",$val['Topic_Postdate'])[0]?></span></td>
											<td class="tb1" data-title="類別"><span><?=$val['TopicC_Name']?></span></td>
											<td class="tb6 left" data-title="標題"><a href="healthin.php?c=<?=OEncrypt('health_in='.$key.'_'.$_Title , 'healthin')?>" title="<?=$val['Topic_Title']?>"><?=$val['Topic_Title']?></a></td>
										</tr>
									<?php }?>
								</table>
							</div>
						<?php }else if( !isset($tmp)&&!isset($mode) ){?>
							<div class="text nopd"><?=TurnSymbol($_Cat2['TopicC_Content'])?>
							</div>
							<h3 class="photoTitle"><?=$_Lv2Name?></h3>
							<div class="mainContent nopd">
								<table class="mainTable">
									<tr>
										<th class="tb1"><span>日期</span></th>
										<!--<th class="tb1"><span>類別</span></th>-->
										<th class="tb6"><span>標題</span></th>
									</tr>
									<?php foreach( $_html as $key => $val ){?>
										<tr>
											<td class="tb1" data-title="日期"><span><?=explode(" ",$val['Topic_Postdate'])[0]?></span></td>
											<!--<td class="tb1" data-title="類別"><span><?=$val['TopicC_Name']?></span></td>-->
											<td class="tb6 left" data-title="標題"><a href="healthin.php?c=<?=OEncrypt('health_in='.$key.'_'.$_Title , 'healthin')?>" title="<?=$val['Topic_Title']?>"><?=$val['Topic_Title']?></a></td>
										</tr>
									<?php }?>
								</table>
							</div>
						<?php }else if( empty($tmp)&&$mode=='search' ){?>
							<div class="text nopd"><?=TurnSymbol($_Cat2['TopicC_Content'])?>
							</div>
							<h3 class="photoTitle"><?=$_Lv2Name?></h3>
							<div class="mainContent nopd">
								<table class="mainTable">
									<tr>
										<th class="tb1"><span>日期</span></th>
										<!--<th class="tb1"><span>類別</span></th>-->
										<th class="tb6"><span>標題</span></th>
									</tr>
									<?php foreach( $_html as $key => $val ){?>
										<tr>
											<td class="tb1" data-title="日期"><span><?=explode(" ",$val['Topic_Postdate'])[0]?></span></td>
											<!--<td class="tb1" data-title="類別"><span><?=$val['TopicC_Name']?></span></td>-->
											<td class="tb6 left" data-title="標題"><a href="healthin.php?c=<?=OEncrypt('health_in='.$key.'_'.$_Title , 'healthin')?>" title="<?=$val['Topic_Title']?>"><?=$val['Topic_Title']?></a></td>
										</tr>
									<?php }?>
								</table>
							</div>
							
						<?php }?>
						<?php 
							if( $mode != 'search' && !empty($_html) ) { 
								require('page.php'); 
							}
						?>
					</div>
				</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>
<script>
$(document).ready(function(e) {
	
	$('.searchhealthbtn').click(function() {
		
		var searchhealth = $('#searchhealth').val();
		var cat = $('#cat').val();
		var type  = $(this).attr('data-type');
		var field = '#'+$(this).closest('form').attr('id');
		if( CheckInput(field) ){
			
			var Form_Data = '';
			Form_Data += '_href=health';
			Form_Data += '&_searchkey1='+cat;
			Form_Data += '&_searchkey2='+searchhealth;
			Form_Data += '&_mode=healthcat';
			Form_Data += '&_modekey1=cat';
			Form_Data += '&_modekey2=health_searchwords';
			Form_Data += '&_type=health_search';
			
			Post_JS(Form_Data, 'web_post.php');
		}
	});
});
</script>