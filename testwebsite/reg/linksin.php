<?php require_once("include/web.config.php");

$Input = GDC( $_GET['c'] , 'lkin' );

$_LK		= $Input['lk_id'];

$PageData['p'] = $_GET['p'];

$_Result 		= $CM->GET_LINK_LIST( $_LK , $PageData );

$Pages_Data 	= $_Result['PageData'];
$_html			= $_Result['Data'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];

$_Title = $_html_LK[$_LK]['LinksC_Name'];
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="opc_info.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">便民服務</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="links.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">網網相連</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="linksin.php" rel="nofollow"  itemprop="url">
								<span itemprop="title"><?=$_html_LK[$_LK]['LinksC_Name']?></span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('service_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">網網相連</h2>
					<div class="mainContent nopd">
						<h3 class="photoTitle"><?=$_html_LK[$_LK]['LinksC_Name']?></h3><br>
						<?php if( !empty($_html) ){ ?>
							<table class="mainTable">
								<tr>
									<th class="tb6"><span>標題</span></th>
								</tr>
								<?php foreach( $_html as $key => $val ){?>
									<tr>
										<td class="tb6 left" data-title="標題"><a target="_blank" href="<?=$val['Links_Link']?>" title="另開新視窗前往<?=$val['Links_Title']?>"><?=$val['Links_Title']?></a></td>
									</tr>
								<?php }?>
								
							</table>
						<?php }else{?>
						
							<p class="nodata">尚無資料</p>
							
						<?php }?>
						<?php 
							if( !empty($_html) ) { 
								require('page.php'); 
							}
						?>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>