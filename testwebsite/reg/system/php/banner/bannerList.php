<?php
if( !function_exists('Chk_Login') ) header('Location: ../../index.php');

global $open_states;

$Table_Field_Arr = $db->get_table_info($Main_Table, 'Comment');

$Sheet = $Main_Table;

//查詢欄位名稱設定
$_Search_Option['Banner_ID']['name']		= $Table_Field_Arr['Banner_ID'];
$_Search_Option['Banner_Title']['name']		= $Table_Field_Arr['Banner_Title'];
$_Search_Option['Banner_Sort']['name']		= $Table_Field_Arr['Banner_Sort'];
$_Search_Option['Banner_Open']['name']		= $Table_Field_Arr['Banner_Open'];
//查詢欄位設定種類
$_Search_Option['Banner_ID']['type']		= 'text';
$_Search_Option['Banner_Title']['type']		= 'text';
$_Search_Option['Banner_Sort']['type']		= 'datetime';
$_Search_Option['Banner_Open']['type']		= 'select';
//查詢欄位設定選擇內容
$_Search_Option['Banner_Open']['select']	= $open_states;

if( !empty($POST['search_field']) ){
				
	$db->Where = Search_Fun($db->Where, $POST, $_Search_Option);
}

$db->Search = array("Banner_ID", "Banner_Title", "Banner_Sort", "Banner_Open");//設定可搜尋欄位
$db->query_search($SearchKey);//串接搜尋子句

$Page_Total_Num = $db->query_count($Sheet);//總資料筆數

$Page_Calss  = new Pages($Pages, $Page_Total_Num, $Page_Size);//頁碼程式
$Pages    = $Page_Calss->Pages;//頁碼
$StartNum = $Page_Calss->StartNum;//從第幾筆開始撈
	
$Title_Array['Ordersn']				= "序號";
$Title_Array['Banner_Title']	= $Table_Field_Arr['Banner_Title'];
$Title_Array['Banner_Mcp']		= $Table_Field_Arr['Banner_Mcp'];
$Title_Array['Banner_Sort']		= $Table_Field_Arr['Banner_Sort'];
$Title_Array['Banner_Open']		= $Table_Field_Arr['Banner_Open'];

$sn = ( $StartNum + 1 );

if( !empty($Order_By) ){
	
	$db->Order_By = $Order_By;
}else if( !empty($Now_List['Menu_OrderBy']) ){
	
	$db->Order_By = $Now_List['Menu_OrderBy'];
}else{
	
	$db->Order_By = " ORDER BY Banner_Sort DESC, Banner_Sdate DESC";
}

$db->query_sql($Sheet, "*", $StartNum, $Page_Size);
while( $row = $db->query_fetch() ){
	
	$Value_Array['Ordersn'][$sn]	 		= $sn;
	$Value_Array[$Main_Key][$sn]	 		= $row[$Main_Key];
	$Value_Array['Banner_ID'][$sn]		= $row['Banner_ID'];
	$Value_Array['Banner_Title'][$sn]	= $row['Banner_Title'];
	
	$Value_Array['Banner_Mcp'][$sn]		= $row['Banner_Mcp'];
	
	if( !empty($row['Banner_Mcp']) ){
			
		$Value_Array['Banner_Mcp_bUrl'][$sn] = $Pathweb.$row['Banner_Mcp'];
		$Value_Array['Banner_Mcp_sUrl'][$sn] = $Pathweb.'sm_'.$row['Banner_Mcp'].'?'.time();
	}

	$Value_Array['Banner_Sort'][$sn]	= $row['Banner_Sort'];
	$Value_Array['Banner_Open'][$sn]	= $row['Banner_Open'];
	
	//呈現的種類
	$Vtype_Array['Banner_Mcp'][$sn]		= 'uploadimg';
	$Vtype_Array['Banner_Sort'][$sn]	= 'number';
	$Vtype_Array['Banner_Open'][$sn]	= 'checkbox';
	$sn++;
}

$table_option_arr['Banner_Sort']['TO_OutEdit']	= 1;
$table_option_arr['Banner_Open']['TO_OutEdit']	= 1;

$Order_Array = array("Banner_ID" => "", "Banner_Title" => "", "Banner_Sort" => "", "Banner_Open" => "");//允許排序項目	

//第一個值設定對應KEY,第二個值設定對應名稱
$_FT = new Fixed_Table( $Value_Array[$Main_Key], $Value_Array['Ordersn'] );
$_FT->Input_TitleArray( $Title_Array );//設定顯示欄位
$_FT->Input_ValueArray( $Value_Array );//設定顯示欄位裡面的值
$_FT->Input_VtypeArray( $Vtype_Array );//設定顯示欄位呈現方式
$_FT->Input_OrderArray( $Order_Array, $Sort, $Field );//設定可排序的欄位, 升序或降序, 要排序欄位名稱
$_FT->Pages_Data 		= $Page_Calss->Pages_Data;//頁碼資料
$_FT->Pages_Html 		= $Page_Calss->Pages_Html;//頁碼頁面
$_FT->SearchKey  		= $SearchKey;//搜尋字串
$_FT->Operating	 		= array("View" => $Now_List['Menu_View'], "Edit" => $Now_List['Menu_Edt'], "Delete" =>  $Now_List['Menu_Del']);//設定操作類型
$_FT->Path		 		= SYS_PATH.$Now_List['Menu_Path'].DIRECTORY_SEPARATOR;//檔案路徑
$_FT->TableHtml	 		= $Now_List['Menu_Exec_Name'].'.table.'.$Now_List['Exec_Sub_Name'];
$_FT->table_option_arr	= $table_option_arr;//資料表的設定陣列

$_html = $_FT->CreatTable();//創建表格

if( !empty($POST) ){
	
	echo $_html;	
}else{
?>

<script type="text/javascript" src="assets/js/sys-table.js"></script>
<script type="text/javascript">

var Exec_Url = '<?=$Now_List['Menu_Path']?>/<?=$Now_List['Menu_Exec_Name']?>.post.php?fun=' + getUrlVal(location.search, 'fun');

</script>

<div class="table-header">
	<span><?=$Now_List['Menu_Name']?></span>
    	
<?php if( !empty($_Search_Option) ){ ?>
    <span class="extra-fun">
    	<i class="fa fa-tasks"></i>進階功能
    </span>
<?php } ?>
</div>

<?php require_once(SYS_PATH.'php_sys/extra_div.php')?>

<div id="table_content"><?=$_html?></div>

<div id="table_content_edit" class="display_none"></div>

<div id="table_content_view" class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
    <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
    <div class="contents"></div>
</div>
<?php 
}?>