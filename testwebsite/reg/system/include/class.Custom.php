<?php

class Custom{
	
	var $Lv1_List;				//第一層所有目錄
	var $Now_List;				//第二層目前目錄
	
	
	function __construct(){
		
		$db = new MySQL();
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------
	//業務職掌
	function GET_BUSINESS_LIST( ){
		
		$Sheet = "web_business";
				
		$db = new MySQL();
		$db->Where = " WHERE Business_Open = 1 ";
		
		$db->Order_By = " ORDER BY Business_Sort DESC, Business_ID DESC ";
		
		$db->query_sql( $Sheet , '*' );
		while( $row = $db->query_fetch() ){
					
			$_data[$row['Business_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Business_Content'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Business_Content'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------	
	//榮耀軌跡
	function GET_GLORY_LIST( $_PageData = array() ){
		
		$Sheet = "web_glory";
				
		$db = new MySQL();
		
		$db->Where = " WHERE Glory_Open = 1 ";
		
		$db->Order_By = " ORDER BY Glory_Sort DESC, Glory_ID DESC ";
		//-------------------------------頁碼----------------------
		$Page_Total_Num = $db->query_count($Sheet);//總資料筆數
		
		$Pages		= !empty($_PageData['p']) && is_numeric($_PageData['p']) ? $_PageData['p'] : 1;//目前頁碼
		$Page_Size	= 10;//顯示筆數
		$Page_Url   = PHP_SELF.'?c='.OEncrypt('glory_id='.$Input , 'glory').'&p=';
		$Page_Calss = new Pages($Pages, $Page_Total_Num, $Page_Size, $Page_Url);//頁碼程式
		$Pages    	= $Page_Calss->Pages;//頁碼
		$StartNum 	= $Page_Calss->StartNum;//從第幾筆開始撈
		$Pages_Data = $Page_Calss->Pages_Data;
		//----------------------------------------------------------------
		$db->query_sql( $Sheet , '*' , $StartNum, $Page_Size);
		while( $row = $db->query_fetch() ){
					
			$_data[$row['Glory_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Glory_Title'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Glory_Title'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['PageData'] 	= $Pages_Data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------	
	//歷任首長
	function GET_HEADMAN_LIST( ){
		
		$Sheet = "web_headman";
				
		$db = new MySQL();
		$db->Where = " WHERE Head_Open = 1 ";
		
		$db->Order_By = " ORDER BY Head_Sort DESC, Head_ID DESC ";
		
		$db->query_sql( $Sheet , '*' );
		while( $row = $db->query_fetch() ){
					
			$_data[$row['Head_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Head_Name'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Head_Name'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
		
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------
	//活動花絮
	function GET_ALBUM_LIST( $Input ){
		
		$Sheet = "web_album";
				
		$db = new MySQL();
		
		$db->Where = " WHERE Album_Open = 1 ";
		$db->Where .= " AND ( Album_Postdate < '".date("Y-m-d")."'";
		$db->Where .= " OR  Album_Postdate = '".date("Y-m-d")."' ) ";
		
		if( !empty($Input) ) {
			
			$db->Where .= " AND Album_ID = '".$Input."'";
		}
		
		$db->Order_By = " ORDER BY Album_Sort DESC, Album_ID DESC ";
		//-------------------------------頁碼----------------------
		$Page_Total_Num = $db->query_count($Sheet);//總資料筆數
		
		$Pages		= !empty($_PageData['p']) && is_numeric($_PageData['p']) ? $_PageData['p'] : 1;//目前頁碼
		$Page_Size	= 6;//顯示筆數
		$Page_Url   = PHP_SELF.'?c='.OEncrypt('album_id='.$Input , 'album').'&p=';
		$Page_Calss = new Pages($Pages, $Page_Total_Num, $Page_Size, $Page_Url);//頁碼程式
		$Pages    	= $Page_Calss->Pages;//頁碼
		$StartNum 	= $Page_Calss->StartNum;//從第幾筆開始撈
		$Pages_Data = $Page_Calss->Pages_Data;
		//----------------------------------------------------------------
		$db->query_sql( $Sheet , '*' , $StartNum, $Page_Size);
		while( $row = $db->query_fetch() ){
					
			$_data[$row['Album_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Album_Title'] , 'K' );
			// $_SEO['WO_Description'] .= GET_HEAD_KD( $row['Blog_Content'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['PageData'] 	= $Pages_Data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//活動花絮
	function GET_ALBUM_SINGLE( $Input ){
		
		$Sheet = "web_album";
				
		$db = new MySQL();
		
		$db->Where = " WHERE Album_Open = 1 ";
		
		if( !empty($Input) ) {
			
			$db->Where .= " AND Album_ID = '".$Input."'";
		}
		
		$db->Order_By = " ORDER BY Album_Sort DESC, Album_ID DESC ";
		//-------------------------------頁碼----------------------
		$Page_Total_Num = $db->query_count($Sheet);//總資料筆數
		
		$Pages		= !empty($_PageData['p']) && is_numeric($_PageData['p']) ? $_PageData['p'] : 1;//目前頁碼
		$Page_Size	= 6;//顯示筆數
		$Page_Url   = PHP_SELF.'?c='.OEncrypt('album_id='.$Input , 'album').'&p=';
		$Page_Calss = new Pages($Pages, $Page_Total_Num, $Page_Size, $Page_Url);//頁碼程式
		$Pages    	= $Page_Calss->Pages;//頁碼
		$StartNum 	= $Page_Calss->StartNum;//從第幾筆開始撈
		$Pages_Data = $Page_Calss->Pages_Data;
		//----------------------------------------------------------------
		$db->query_sql( $Sheet , '*' , $StartNum, $Page_Size);
		while( $row = $db->query_fetch() ){
					
			$row['Album_ID']	= $row;	
			
			$_data = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Album_Title'] , 'K' );
			// $_SEO['WO_Description'] .= GET_HEAD_KD( $row['Blog_Content'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['PageData'] 	= $Pages_Data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//活動花絮
	function GET_ALBUM_DATA( $Input = '', $_PageData = array() ){
		
		$Sheet = "web_albuml as a LEFT JOIN web_album as b ON a.Album_ID=b.Album_ID";
			
		$db = new MySQL();
		$db->Where = " WHERE b.Album_Open = 1 ";
		$db->Where .= " AND a.Albuml_Open = 1 ";
		$db->Where .= " AND a.Album_ID = '".$Input."'";
		//-------------------------------頁碼----------------------
		$Page_Total_Num = $db->query_count($Sheet);//總資料筆數
		
		$Pages		= !empty($_PageData['p']) && is_numeric($_PageData['p']) ? $_PageData['p'] : 1;//目前頁碼
		$Page_Size	= 6;//顯示筆數
		$Page_Url   = PHP_SELF.'?c='.OEncrypt('albumin_id='.$Input , 'albumin').'&p=';
		$Page_Calss = new Pages($Pages, $Page_Total_Num, $Page_Size, $Page_Url);//頁碼程式
		$Pages    	= $Page_Calss->Pages;//頁碼
		$StartNum 	= $Page_Calss->StartNum;//從第幾筆開始撈
		$Pages_Data = $Page_Calss->Pages_Data;
		//----------------------------------------------------------------

		$db->query_sql( $Sheet , 'a.*,b.*' , $StartNum, $Page_Size);
		while( $row = $db->query_fetch() ){
			
			$_data[$row['Albuml_ID']]	= $row;	

			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Albuml_Title'] , 'K' );
			// $_SEO['WO_Description'] .= GET_HEAD_KD( $row['Blog_Content'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['PageData'] 	= $Pages_Data;
		$_rs['SEO'] 		= $_SEO;
		

		return $_rs;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------
	//志工花絮
	function GET_VAL_ALBUM_LIST( $Input ){
		
		$Sheet = "web_volunteeralbum";
				
		$db = new MySQL();
		
		$db->Where = " WHERE VTalbum_Open = 1 ";
		$db->Where .= " AND ( VTalbum_Postdate < '".date("Y-m-d")."'";
		$db->Where .= " OR  VTalbum_Postdate = '".date("Y-m-d")."' ) ";
		
		if( !empty($Input) ) {
			
			$db->Where .= " AND VTalbum_ID = '".$Input."'";
		}
		
		$db->Order_By = " ORDER BY VTalbum_Sort DESC, VTalbum_ID DESC ";
		//-------------------------------頁碼----------------------
		$Page_Total_Num = $db->query_count($Sheet);//總資料筆數
		
		$Pages		= !empty($_PageData['p']) && is_numeric($_PageData['p']) ? $_PageData['p'] : 1;//目前頁碼
		$Page_Size	= 6;//顯示筆數
		$Page_Url   = PHP_SELF.'?c='.OEncrypt('vtalbum_id='.$Input , 'vtalbum').'&p=';
		$Page_Calss = new Pages($Pages, $Page_Total_Num, $Page_Size, $Page_Url);//頁碼程式
		$Pages    	= $Page_Calss->Pages;//頁碼
		$StartNum 	= $Page_Calss->StartNum;//從第幾筆開始撈
		$Pages_Data = $Page_Calss->Pages_Data;
		//----------------------------------------------------------------
		$db->query_sql( $Sheet , '*' , $StartNum, $Page_Size);
		while( $row = $db->query_fetch() ){
					
			$_data[$row['VTalbum_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['VTalbum_Title'] , 'K' );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['PageData'] 	= $Pages_Data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//志工花絮
	function GET_VAL_ALBUM_SINGLE( $Input ){
		
		$Sheet = "web_volunteeralbum";
				
		$db = new MySQL();
		
		$db->Where = " WHERE VTalbum_Open = 1 ";
		
		if( !empty($Input) ) {
			
			$db->Where .= " AND VTalbum_ID = '".$Input."'";
		}
		
		$db->Order_By = " ORDER BY VTalbum_Sort DESC, VTalbum_ID DESC ";
		//-------------------------------頁碼----------------------
		$Page_Total_Num = $db->query_count($Sheet);//總資料筆數
		
		$Pages		= !empty($_PageData['p']) && is_numeric($_PageData['p']) ? $_PageData['p'] : 1;//目前頁碼
		$Page_Size	= 6;//顯示筆數
		$Page_Url   = PHP_SELF.'?c='.OEncrypt('vtalbum_id='.$Input , 'vtalbum').'&p=';
		$Page_Calss = new Pages($Pages, $Page_Total_Num, $Page_Size, $Page_Url);//頁碼程式
		$Pages    	= $Page_Calss->Pages;//頁碼
		$StartNum 	= $Page_Calss->StartNum;//從第幾筆開始撈
		$Pages_Data = $Page_Calss->Pages_Data;
		//----------------------------------------------------------------
		$db->query_sql( $Sheet , '*' , $StartNum, $Page_Size);
		while( $row = $db->query_fetch() ){
					
			$row['VTalbum_ID']	= $row;	
			
			$_data = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['VTalbum_Title'] , 'K' );
			// $_SEO['WO_Description'] .= GET_HEAD_KD( $row['Blog_Content'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['PageData'] 	= $Pages_Data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//志工花絮
	function GET_VAL_ALBUM_DATA( $Input = '', $_PageData = array() ){
		
		$Sheet = "web_volunteeralbuml as a LEFT JOIN web_volunteeralbum as b ON a.VTalbum_ID=b.VTalbum_ID";
			
		$db = new MySQL();
		$db->Where = " WHERE b.VTalbum_Open = 1 ";
		$db->Where .= " AND a.VTalbuml_Open = 1 ";
		$db->Where .= " AND a.VTalbum_ID = '".$Input."'";
		//-------------------------------頁碼----------------------
		$Page_Total_Num = $db->query_count($Sheet);//總資料筆數
		
		$Pages		= !empty($_PageData['p']) && is_numeric($_PageData['p']) ? $_PageData['p'] : 1;//目前頁碼
		$Page_Size	= 6;//顯示筆數
		$Page_Url   = PHP_SELF.'?c='.OEncrypt('vtalbumin_id='.$Input , 'vtalbumin').'&p=';
		$Page_Calss = new Pages($Pages, $Page_Total_Num, $Page_Size, $Page_Url);//頁碼程式
		$Pages    	= $Page_Calss->Pages;//頁碼
		$StartNum 	= $Page_Calss->StartNum;//從第幾筆開始撈
		$Pages_Data = $Page_Calss->Pages_Data;
		//----------------------------------------------------------------

		$db->query_sql( $Sheet , 'a.*,b.*' , $StartNum, $Page_Size);
		while( $row = $db->query_fetch() ){
			
			$_data[$row['VTalbuml_ID']]	= $row;	

			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Albuml_Title'] , 'K' );
			// $_SEO['WO_Description'] .= GET_HEAD_KD( $row['Blog_Content'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['PageData'] 	= $Pages_Data;
		$_rs['SEO'] 		= $_SEO;
		

		return $_rs;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------
	//本鄉鎮醫療資源
	function GET_MR_LIST( $Input ){
		
		$Sheet = "web_mr";
				
		$db = new MySQL();
		
		$db->Where = " WHERE MR_Open = 1 ";
		$db->Where .= " AND ( MR_Postdate < '".date("Y-m-d")."'";
		$db->Where .= " OR  MR_Postdate = '".date("Y-m-d")."' ) ";
		
		if( !empty($Input) ) {
			
			$db->Where .= " AND MR_ID = '".$Input."'";
		}
		
		$db->Order_By = " ORDER BY MR_Sort DESC, MR_ID DESC ";
		
		$db->query_sql( $Sheet , '*' );
		while( $row = $db->query_fetch() ){
					
			$_data[$row['MR_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['MR_Title'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['MR_Content'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------
	//門診資訊
	function GET_OPC_LIST( $Input ){
		
		$Sheet = "web_outpatient_times";
				
		$db = new MySQL();
		
		$db->Where = " WHERE OPs_Name LIKE '%".上午."' ";
		
		$db->Order_By = " ORDER BY OPs_ID DESC ";
		
		$db->query_sql( $Sheet , 'OPs_ID, OPs_Name, OPs_Content' );
		while( $row = $db->query_fetch() ){
					
			$_data1[$row['OPs_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['OPs_Name'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['OPs_Content'] );
		}
		$_rs['DataA'] 		= $_data1;
		$db = new MySQL();
		
		$db->Where = " WHERE OPs_Name LIKE '%".下午."' ";
		
		$db->Order_By = ' ORDER BY OPs_ID DESC';
		
		$db->query_sql( $Sheet , 'OPs_ID, OPs_Name, OPs_Content' );
		while( $row = $db->query_fetch() ){
					
			$_data2[$row['OPs_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['OPs_Name'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['OPs_Content'] );
		}
		
		$_rs['DataP'] 		= $_data2;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
		
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------
	//為民服務流程
	function GET_SP_LIST( $Input ){
		
		$Sheet = "web_sp";
				
		$db = new MySQL();
		
		$db->Where = " WHERE SP_Open = 1 ";
		$db->Where .= " AND ( SP_Postdate < '".date("Y-m-d")."'";
		$db->Where .= " OR  SP_Postdate = '".date("Y-m-d")."' ) ";
		
		if( !empty($Input) ) {
			
			$db->Where .= " AND SP_ID = '".$Input."'";
		}
		
		$db->Order_By = " ORDER BY SP_Sort DESC, SP_ID DESC ";
		
		$db->query_sql( $Sheet , '*' );
		while( $row = $db->query_fetch() ){
					
			$_data[$row['SP_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['SP_Title'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['SP_Content'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------
	//政府公開資訊
	function GET_GOV_LIST( $_PageData = array() ){
		
		$Sheet = "web_gpi";
			
		$db = new MySQL();
		
		$db->Where = " WHERE GPI_Lv = 1 ";
		$db->Where .= " And GPI_Open = 1 ";
		
		$db->Order_By = " ORDER BY GPI_Sort DESC, GPI_ID DESC ";
		//-------------------------------頁碼----------------------
		$Page_Total_Num = $db->query_count($Sheet);//總資料筆數
		
		$Pages		= !empty($_PageData['p']) && is_numeric($_PageData['p']) ? $_PageData['p'] : 1;//目前頁碼
		$Page_Size	= 6;//顯示筆數
		$Page_Url   = PHP_SELF.'?p=';
		$Page_Calss = new Pages($Pages, $Page_Total_Num, $Page_Size, $Page_Url);//頁碼程式
		$Pages    	= $Page_Calss->Pages;//頁碼
		$StartNum 	= $Page_Calss->StartNum;//從第幾筆開始撈
		$Pages_Data = $Page_Calss->Pages_Data;
		//----------------------------------------------------------------
		$db->query_sql( $Sheet , '*', $StartNum, $Page_Size );
		while( $row = $db->query_fetch() ){
					
			$_dataA[$row['GPI_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['GPI_Name'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['GPI_Name'] );
		}
		
		$_rs['DataA'] 		= $_dataA;
		
		$db = new MySQL();
		
		$db->Where = " WHERE GPI_Lv = 2 ";
		
		$db->Order_By = ' ORDER BY GPI_Sort DESC, GPI_ID DESC';
		
		$db->query_sql( $Sheet , '*' );
		while( $row = $db->query_fetch() ){
					
			$_dataB[$row['GPI_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['GPI_Name'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['GPI_Name'] );
		}
		
		$_rs['DataB'] 		= $_dataB;
		$_rs['PageData'] 	= $Pages_Data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//政府公開資訊_搜尋
	function GET_GOV_SEARCH( $INPUT = array() ){
		
		$Sheet = "web_gpi";

		$db = new MySQL();
		
		$db->Where = " WHERE GPI_Open = 1 ";
		$db->Where .= " And GPI_Lv = 1 ";
		$db->Where .= " And GPI_Name LIKE '%".$INPUT."%' ";
		
		$db->Order_By = " ORDER BY GPI_Sort DESC, GPI_ID DESC ";
		
		$db->query_sql( $Sheet , '*' );
		while( $row = $db->query_fetch() ){
			
			$_data[$row['GPI_ID']] = $row;
		}
		if( empty($_data) ){
			
			$db->Where = " WHERE GPI_Open = 1 ";
			$db->Where .= " And GPI_Lv = 2 ";
			$db->Where .= " And GPI_Name LIKE '%".$INPUT."%' ";
			
			$db->Order_By = " ORDER BY GPI_Sort DESC, GPI_ID DESC ";
			
			$db->query_sql( $Sheet , '*' );
			while( $row = $db->query_fetch() ){
				
				$_datax[$row['GPI_ID']] = $row;
			}
			foreach( $_datax as $key => $val ){
				
				$db->Where = " WHERE GPI_Open = 1 ";
				$db->Where .= " And GPI_Lv = 1 ";
				$db->Where .= " And GPI_ID = '".$val['GPI_UpMID']."' ";
				
				$db->Order_By = " ORDER BY GPI_Sort DESC, GPI_ID DESC ";
				
				$db->query_sql( $Sheet , '*' );
				while( $row = $db->query_fetch() ){
					
					$_datax1[$row['GPI_ID']] = $row;
				}
			}
		}else{
			foreach( $_data as $key => $val ){
				
				$db->Where = " WHERE GPI_Open = 1 ";
				$db->Where .= " And GPI_Lv = 2 ";
				$db->Where .= " And GPI_UpMID = '".$key."' ";
				
				$db->Order_By = " ORDER BY GPI_Sort DESC, GPI_ID DESC ";
				
				$db->query_sql( $Sheet , '*' );
				while( $row = $db->query_fetch() ){
					
					$_data2[$row['GPI_ID']] = $row;
				}
			}
				
		}
		
		
		$_rs['DataS'] 		= $_data;
		$_rs['DataS2'] 		= $_data2;
		$_rs['DataX'] 		= $_datax;
		$_rs['DataX1'] 		= $_datax1;
		
		$_rs['SEO'] 		= $_SEO;
		$_rs['sql'] 		= $db->sql;
		
		return $_rs;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------
	//服務品質計畫
	function GET_SQ_LIST( $Input , $_PageData = array() ){
		
		$Sheet = "web_sqplan";
			
		$db = new MySQL();
		
		$db->Where = " WHERE SQplan_Open = 1 ";
		
		if( !empty($Input) ) {
			
			$db->Where .= " AND SQplan_ID = '".$Input."'";
		}
		
		$db->Order_By = " ORDER BY SQplan_Sort DESC, SQplan_ID DESC ";
		//-------------------------------頁碼----------------------
		$Page_Total_Num = $db->query_count($Sheet);//總資料筆數
		
		$Pages		= !empty($_PageData['p']) && is_numeric($_PageData['p']) ? $_PageData['p'] : 1;//目前頁碼
		$Page_Size	= 10;//顯示筆數
		$Page_Url   = PHP_SELF.'?p=';
		$Page_Calss = new Pages($Pages, $Page_Total_Num, $Page_Size, $Page_Url);//頁碼程式
		$Pages    	= $Page_Calss->Pages;//頁碼
		$StartNum 	= $Page_Calss->StartNum;//從第幾筆開始撈
		$Pages_Data = $Page_Calss->Pages_Data;
		//----------------------------------------------------------------
		$db->query_sql( $Sheet , '*' );
		while( $row = $db->query_fetch() ){
					
			$_data[$row['SQplan_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['SQplan_Title'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['SQplan_Content'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['PageData'] 	= $Pages_Data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------
	//檔案下載
	function GET_DOWNLOAD_CAT(){
	
		$Sheet = "web_downloadclass";
				
		$db = new MySQL();

		$db->Where = " WHERE DownloadC_Open = 1 ";
		
		$db->Order_By = " ORDER BY DownloadC_Sort DESC, DownloadC_ID DESC ";

		$db->query_sql( $Sheet , 'DownloadC_ID, DownloadC_Name');
		
		while( $row = $db->query_fetch() ){
					
			$_data[$row['DownloadC_ID']] = $row;
			
			$_SEO['WO_Keywords'] 		.= GET_HEAD_KD( $row['DownloadC_Name'] , 'K' );
			$_SEO['WO_Description'] 	.= GET_HEAD_KD( $row['DownloadC_Name'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//檔案下載
	function GET_DOWNLOAD_LIST( $Input = '' ){
		
		$FilePath = WEB_PATH.'sys_files'.DIRECTORY_SEPARATOR.'chshb_bedphc'.DIRECTORY_SEPARATOR.'Download'.DIRECTORY_SEPARATOR;
		$JpgPath = WEB_PATH.'sys_images'.DIRECTORY_SEPARATOR.'chshb_bedphc'.DIRECTORY_SEPARATOR.'Download'.DIRECTORY_SEPARATOR;
			
		$Sheet = "web_download as a LEFT JOIN web_downloadclass as b ON a.DownloadC_ID = b.DownloadC_ID";
				
		$db = new MySQL();
		
		$db->Where = " WHERE Download_Open = 1 ";
		
		$db->Where .= " AND DownloadC_Open = 1 ";
		
		if( !empty($Input) ) {
			
			$db->Where .= " AND a.DownloadC_ID = '".$Input."'";
		}
		
		$db->Order_By = " ORDER BY Download_Sort DESC, Download_ID DESC ";

		$db->query_sql( $Sheet , '*' );
		while( $row = $db->query_fetch() ){
					
			$_data[$row['Download_ID']] = $row;
			
			if( is_file(ICONV_CODE( 'UTF8_TO_BIG5', $FilePath.$row['Download_FilePDF'])) ){

				$_data[$row['Download_ID']]['Name'] = $row['Download_FilePDF'];
				$_data[$row['Download_ID']]['PDF'] = Turnencode('?t=web_download&k=Download_ID&f=Download_FilePDF&i='.$row['Download_ID'].'&p='.$FilePath, 'downfile');
			}
			
			if( is_file(ICONV_CODE( 'UTF8_TO_BIG5', $FilePath.$row['Download_FileDOC'])) ){

				$_data[$row['Download_ID']]['Name'] = $row['Download_FileDOC'];
				$_data[$row['Download_ID']]['DOC'] = Turnencode('?t=web_download&k=Download_ID&f=Download_FileDOC&i='.$row['Download_ID'].'&p='.$FilePath, 'downfile');
			}
			
			if( is_file(ICONV_CODE( 'UTF8_TO_BIG5', $FilePath.$row['Download_FileODT'])) ){

				$_data[$row['Download_ID']]['Name'] = $row['Download_FileODT'];
				$_data[$row['Download_ID']]['ODT'] = Turnencode('?t=web_download&k=Download_ID&f=Download_FileODT&i='.$row['Download_ID'].'&p='.$FilePath, 'downfile');
			}
			
			if( is_file(ICONV_CODE( 'UTF8_TO_BIG5', $JpgPath.$row['Download_FileJPG'])) ){

				$_data[$row['Download_ID']]['Name'] = $row['Download_FileJPG'];
				$_data[$row['Download_ID']]['JPG'] = Turnencode('?t=web_download&k=Download_ID&f=Download_FileJPG&i='.$row['Download_ID'].'&p='.$JpgPath, 'downfile');
			}
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Download_Title'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Download_Title'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['SEO'] 		= $_SEO;
		$_rs['sql'] 		= $db->sql;
		
		return $_rs;
	}

	//檔案下載_搜尋
	function GET_DOWNLOAD_SEARCH( $Input = '' ){
		
		$FilePath = WEB_PATH.'sys_files'.DIRECTORY_SEPARATOR.'chshb_bedphc'.DIRECTORY_SEPARATOR.'Download'.DIRECTORY_SEPARATOR;
		$JpgPath = WEB_PATH.'sys_images'.DIRECTORY_SEPARATOR.'chshb_bedphc'.DIRECTORY_SEPARATOR.'Download'.DIRECTORY_SEPARATOR;
			
		$Sheet = "web_download as a LEFT JOIN web_downloadclass as b ON a.DownloadC_ID = b.DownloadC_ID";
				
		$db = new MySQL();
		
		$db->Where = " WHERE Download_Open = 1 ";
		
		$db->Where .= " AND DownloadC_Open = 1 ";
			
		$db->Where .= " AND a.Download_Title LIKE '%".$Input."%' OR b.DownloadC_Name LIKE '%".$Input."%'";
		
		
		$db->Order_By = " ORDER BY Download_Sort DESC, Download_ID DESC ";

		$db->query_sql( $Sheet , '*' );
		while( $row = $db->query_fetch() ){
					
			$_data[$row['Download_ID']] = $row;
			
			if( is_file(ICONV_CODE( 'UTF8_TO_BIG5', $FilePath.$row['Download_FilePDF'])) ){

				$_data[$row['Download_ID']]['Name'] = $row['Download_FilePDF'];
				$_data[$row['Download_ID']]['PDF'] = Turnencode('?t=web_download&k=Download_ID&f=Download_FilePDF&i='.$row['Download_ID'].'&p='.$FilePath, 'downfile');
			}
			
			if( is_file(ICONV_CODE( 'UTF8_TO_BIG5', $FilePath.$row['Download_FileDOC'])) ){

				$_data[$row['Download_ID']]['Name'] = $row['Download_FileDOC'];
				$_data[$row['Download_ID']]['DOC'] = Turnencode('?t=web_download&k=Download_ID&f=Download_FileDOC&i='.$row['Download_ID'].'&p='.$FilePath, 'downfile');
			}
			
			if( is_file(ICONV_CODE( 'UTF8_TO_BIG5', $FilePath.$row['Download_FileODT'])) ){

				$_data[$row['Download_ID']]['Name'] = $row['Download_FileODT'];
				$_data[$row['Download_ID']]['ODT'] = Turnencode('?t=web_download&k=Download_ID&f=Download_FileODT&i='.$row['Download_ID'].'&p='.$FilePath, 'downfile');
			}
			
			if( is_file(ICONV_CODE( 'UTF8_TO_BIG5', $JpgPath.$row['Download_FileJPG'])) ){

				$_data[$row['Download_ID']]['Name'] = $row['Download_FileJPG'];
				$_data[$row['Download_ID']]['JPG'] = Turnencode('?t=web_download&k=Download_ID&f=Download_FileJPG&i='.$row['Download_ID'].'&p='.$JpgPath, 'downfile');
			}
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Download_Title'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Download_Title'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['SEO'] 		= $_SEO;
		$_rs['sql'] 		= $db->sql;
		
		return $_rs;
	}

	//-------------------------------------------------------------------------------------------------------------------------------------------------------------
	//影音資料
	function GET_VIDEO_LIST( $_PageData = array() ){
		
		$Sheet = "web_video";
			
		$db = new MySQL();
		
		$db->Where = " WHERE Video_Open = 1 ";
		$db->Where .= " AND ( Video_Postdate < '".date("Y-m-d")."'";
		$db->Where .= " OR  Video_Postdate = '".date("Y-m-d")."' ) ";
		
		$db->Order_By = " ORDER BY Video_Sort DESC, Video_ID DESC ";
		//-------------------------------頁碼----------------------
		$Page_Total_Num = $db->query_count($Sheet);//總資料筆數
		
		$Pages		= !empty($_PageData['p']) && is_numeric($_PageData['p']) ? $_PageData['p'] : 1;//目前頁碼
		$Page_Size	= 10;//顯示筆數
		$Page_Url   = PHP_SELF.'?p=';
		$Page_Calss = new Pages($Pages, $Page_Total_Num, $Page_Size, $Page_Url);//頁碼程式
		$Pages    	= $Page_Calss->Pages;//頁碼
		$StartNum 	= $Page_Calss->StartNum;//從第幾筆開始撈
		$Pages_Data = $Page_Calss->Pages_Data;
		//----------------------------------------------------------------
		$db->query_sql( $Sheet , '*' );
		while( $row = $db->query_fetch() ){
					
			$_data[$row['Video_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Video_Title'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Video_ChannelName'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['PageData'] 	= $Pages_Data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------
	//網網相連
	function GET_LINK_CAT(){
	
		$Sheet = "web_linksclass";
				
		$db = new MySQL();

		$db->Where = " WHERE LinksC_Open = 1 ";
		
		$db->Order_By = " ORDER BY LinksC_Sort DESC, LinksC_ID DESC ";

		$db->query_sql( $Sheet , 'LinksC_ID, LinksC_Name');
		
		while( $row = $db->query_fetch() ){
					
			$_data[$row['LinksC_ID']] = $row;
			
			$_SEO['WO_Keywords'] 		.= GET_HEAD_KD( $row['LinksC_Name'] , 'K' );
			$_SEO['WO_Description'] 	.= GET_HEAD_KD( $row['LinksC_Name'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//網網相連
	function GET_LINK_LIST( $Input = '' , $_PageData = array() ){
		
		$Sheet = "web_links as a LEFT JOIN web_linksclass as b ON a.LinksC_ID = b.LinksC_ID";
				
		$db = new MySQL();
		
		$db->Where = " WHERE Links_Open = 1 ";
		
		$db->Where .= " AND LinksC_Open = 1 ";
		
		if( !empty($Input) ) {
			
			$db->Where .= " AND a.LinksC_ID = '".$Input."'";
		}
		
		$db->Order_By = " ORDER BY Links_Sort DESC, Links_ID DESC ";
		//-------------------------------頁碼----------------------
		$Page_Total_Num = $db->query_count($Sheet);//總資料筆數
		
		$Pages		= !empty($_PageData['p']) && is_numeric($_PageData['p']) ? $_PageData['p'] : 1;//目前頁碼
		$Page_Size	= 10;//顯示筆數
		$Page_Url   = PHP_SELF.'?p=';
		$Page_Calss = new Pages($Pages, $Page_Total_Num, $Page_Size, $Page_Url);//頁碼程式
		$Pages    	= $Page_Calss->Pages;//頁碼
		$StartNum 	= $Page_Calss->StartNum;//從第幾筆開始撈
		$Pages_Data = $Page_Calss->Pages_Data;
		//----------------------------------------------------------------

		$db->query_sql( $Sheet , '*' , $StartNum, $Page_Size );
		while( $row = $db->query_fetch() ){
					
			$_data[$row['Links_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Links_Title'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Links_Title'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['PageData'] 	= $Pages_Data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//網網相連_搜尋
	function GET_LINK_SEARCH( $Input = '' ){
		
		$Sheet = "web_links as a LEFT JOIN web_linksclass as b ON a.LinksC_ID = b.LinksC_ID";
				
		$db = new MySQL();
		
		$db->Where = " WHERE Links_Open = 1 ";
		
		$db->Where .= " AND LinksC_Open = 1 ";

		$db->Where .= " AND ( a.Links_Title LIKE '%".$Input."%'";
		$db->Where .= " OR b.LinksC_Name LIKE '%".$Input."%' ) ";
		
		$db->Order_By = " ORDER BY Links_Sort DESC, Links_ID DESC ";
		
		$db->query_sql( $Sheet , '*' );
		while( $row = $db->query_fetch() ){
					
			$_data[$row['Links_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Links_Title'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Links_Title'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['SEO'] 		= $_SEO;
		$_rs['sql'] 		= $db->sql;
		
		return $_rs;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------
	//訊息專區
	function GET_NEWS_LIST( $Input = '' , $Cat = '' ,  $_PageData = array() ){
		
		$Sheet = "web_news";
				
		$db = new MySQL();
		
		$db->Where = " WHERE News_Open = 1 ";
		$db->Where .= " AND '".date("Y-m-d H:i:s")."' Between News_PostDate AND News_EndDate ";
		
		if( !empty($Input) ) {
			
			$db->Where .= " AND News_ID = '".$Input."'";
		}
		
		if( !empty($Cat) && $Cat=='hot' ) {
			
			$db->Where .= " AND News_Hot = '1'";
		}
		
		$db->Order_By = " ORDER BY News_Sort DESC, News_ID DESC ";
		//-------------------------------頁碼----------------------
		$Page_Total_Num = $db->query_count($Sheet);//總資料筆數
		
		$Pages		= !empty($_PageData['p']) && is_numeric($_PageData['p']) ? $_PageData['p'] : 1;//目前頁碼
		$Page_Size	= 10;//顯示筆數
		$Page_Url   = PHP_SELF.'?c='.OEncrypt('news_cat='.$Cat , 'newscat').'&p=';
		$Page_Calss = new Pages($Pages, $Page_Total_Num, $Page_Size, $Page_Url);//頁碼程式
		$Pages    	= $Page_Calss->Pages;//頁碼
		$StartNum 	= $Page_Calss->StartNum;//從第幾筆開始撈
		$Pages_Data = $Page_Calss->Pages_Data;
		//----------------------------------------------------------------

		$db->query_sql( $Sheet , '*' , $StartNum, $Page_Size);
		while( $row = $db->query_fetch() ){
					
			$_data[$row['News_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['News_Title'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['News_Content'] );
		}
		if( count($_data)==1 && !empty($Input) ){
				
			$db_data['News_Click'] = $_data[$Input]['News_Click'] + 1;
			$db->Where = " WHERE News_ID = '".$Input."'";
			$db->query_data($Sheet, $db_data, 'UPDATE');
		}
		$_rs['Data'] 		= $_data;
		$_rs['PageData'] 	= $Pages_Data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------
	//健康主題
	function GET_HEALTH_CAT1( $Cat = '' ){
	
		$Sheet = "web_topicclass";
				
		$db = new MySQL();

		$db->Where = " WHERE TopicC_Open = 1 ";
		
		$db->Where .= " AND TopicC_Lv = '1' ";
		
		$db->Where .= " AND TopicC_ID = '".$Cat."' ";
		
		$db->Order_By = " ORDER BY TopicC_Sort DESC, TopicC_ID DESC ";

		$db->query_sql( $Sheet , '*');
		
		while( $row = $db->query_fetch() ){
					
			$_data = $row;
			
			$_SEO['WO_Keywords'] 		.= GET_HEAD_KD( $row['TopicC_Name'] , 'K' );
			$_SEO['WO_Description'] 	.= GET_HEAD_KD( $row['TopicC_Content'] );
		}
		
		$_rs['Data'] 		= $_data;		
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//健康主題
	function GET_HEALTH_CAT2( $Cat = '' ){
	
		$Sheet = "web_topicclass";
				
		$db = new MySQL();

		$db->Where = " WHERE TopicC_Open = 1 ";
		
		$db->Where .= " AND TopicC_Lv = '2' ";
		
		$db->Where .= " AND TopicC_ID = '".$Cat."' ";
		
		$db->Order_By = " ORDER BY TopicC_Sort DESC, TopicC_ID DESC ";

		$db->query_sql( $Sheet , 'TopicC_ID, TopicC_Name, TopicC_UpMID');
		
		if( $row = $db->query_fetch() ){
					
			// $row;
			$_data = $this->GET_HEALTH_CAT1($row['TopicC_UpMID']);
			$_data['lv2name']	= $row['TopicC_Name'];
			$_data['lv2id'] 	= $row['TopicC_ID'];
			
		}
		
		$_rs['Data'] 		= $_data;		
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//健康主題
	function GET_HEALTH_CAT( $Cat = '' ){
	
		$Sheet = "web_topicclass";
				
		$db = new MySQL();

		$db->Where = " WHERE TopicC_Open = 1 ";
		
		$db->Where .= " AND TopicC_Lv = '1' ";
		
		$db->Order_By = " ORDER BY TopicC_Sort DESC, TopicC_ID DESC ";

		$db->query_sql( $Sheet , '*');
		
		while( $row = $db->query_fetch() ){
					
			$_dataA[$row['TopicC_ID']] = $row;
			
			$_SEO['WO_Keywords'] 		.= GET_HEAD_KD( $row['TopicC_Name'] , 'K' );
			$_SEO['WO_Description'] 	.= GET_HEAD_KD( $row['TopicC_Content'] );
		}
		
		$db = new MySQL();

		$db->Where = " WHERE TopicC_Open = 1 ";
		
		$db->Where .= " AND TopicC_Lv = '2' ";
		
		$db->Order_By = " ORDER BY TopicC_Sort DESC, TopicC_ID DESC ";

		$db->query_sql( $Sheet , '*');
		
		while( $row = $db->query_fetch() ){
					
			$_dataB[$row['TopicC_ID']] = $row;
			
			$_SEO['WO_Keywords'] 		.= GET_HEAD_KD( $row['TopicC_Name'] , 'K' );
			$_SEO['WO_Description'] 	.= GET_HEAD_KD( $row['TopicC_Content'] );
		}
		
		$_rs['DataA'] 		= $_dataA;		
		$_rs['DataB'] 		= $_dataB;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//健康主題
	function GET_HEALTH_LIST( $Input = '' , $Cat = '' ,  $_PageData = array() ){
		
		$Sheet = "web_topic as a LEFT JOIN web_topicclass as b ON a.TopicC_ID = b.TopicC_ID";
				
		$db = new MySQL();
		
		$db->Where = " WHERE Topic_Open = 1 ";
		$db->Where .= " AND ( Topic_Postdate < '".date("Y-m-d")."'";
		$db->Where .= " OR  Topic_Postdate = '".date("Y-m-d")."' ) ";
		
		$db->Where .= " AND TopicC_Open = 1 ";
		
		if( !empty($Cat) ) {
			
			$db->Where .= " AND a.TopicC_ID = '".$Cat."'";
		}
		
		$db->Order_By = " ORDER BY Topic_Sort DESC, Topic_ID DESC ";
		//-------------------------------頁碼----------------------
		$Page_Total_Num = $db->query_count($Sheet);//總資料筆數
		
		$Pages		= !empty($_PageData['p']) && is_numeric($_PageData['p']) ? $_PageData['p'] : 1;//目前頁碼
		$Page_Size	= 10;//顯示筆數
		$Page_Url   = PHP_SELF.'?c='.OEncrypt('health_cat='.$Cat , 'healthcat').'&p=';
		$Page_Calss = new Pages($Pages, $Page_Total_Num, $Page_Size, $Page_Url);//頁碼程式
		$Pages    	= $Page_Calss->Pages;//頁碼
		$StartNum 	= $Page_Calss->StartNum;//從第幾筆開始撈
		$Pages_Data = $Page_Calss->Pages_Data;
		//----------------------------------------------------------------

		$db->query_sql( $Sheet , '*' , $StartNum, $Page_Size);
		while( $row = $db->query_fetch() ){
					
			$_data[$row['Topic_ID']] = $row;
			$_other['catname'] = $row['TopicC_Name'];
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Topic_Title'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Topic_Content'] );
		}
		if( empty($_data) ){
			
			$db = new MySQL();
		
			$db->Where = " WHERE Topic_Open = 1 ";
			$db->Where .= " AND ( Topic_Postdate < '".date("Y-m-d")."'";
			$db->Where .= " OR  Topic_Postdate = '".date("Y-m-d")."' ) ";
			
			$db->Where .= " AND TopicC_Open = 1 ";
			
			if( !empty($Cat) ) {
				
				$db->Where .= " AND TopicC_UpMID = '".$Cat."'";
			}
			
			$db->Order_By = " ORDER BY Topic_Sort DESC, Topic_ID DESC ";
			//-------------------------------頁碼----------------------
			$Page_Total_Num = $db->query_count($Sheet);//總資料筆數
			
			$Pages		= !empty($_PageData['p']) && is_numeric($_PageData['p']) ? $_PageData['p'] : 1;//目前頁碼
			$Page_Size	= 10;//顯示筆數
			$Page_Url   = PHP_SELF.'?c='.OEncrypt('health_cat='.$Cat , 'healthcat').'&p=';
			$Page_Calss = new Pages($Pages, $Page_Total_Num, $Page_Size, $Page_Url);//頁碼程式
			$Pages    	= $Page_Calss->Pages;//頁碼
			$StartNum 	= $Page_Calss->StartNum;//從第幾筆開始撈
			$Pages_Data = $Page_Calss->Pages_Data;
			//----------------------------------------------------------------

			$db->query_sql( $Sheet , '*' , $StartNum, $Page_Size);
			while( $row = $db->query_fetch() ){
						
				$_data[$row['Topic_ID']] = $row;
				
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Topic_Title'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Topic_Content'] );
			}
		}
		
		$_rs['Data'] 		= $_data;
		
		$_rs['PageData'] 	= $Pages_Data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------
	//健康主題_搜尋
	function GET_HEALTH_SEARCH( $Input = '', $Cat = '' ){
	
		$Sheet = "web_topic as a LEFT JOIN web_topicclass as b ON a.TopicC_ID = b.TopicC_ID";
				
		$db = new MySQL();
		
		$db->Where = " WHERE Topic_Open = 1 ";
		$db->Where .= " AND ( Topic_Postdate < '".date("Y-m-d")."'";
		$db->Where .= " OR  Topic_Postdate = '".date("Y-m-d")."' ) ";
		
		$db->Where .= " AND TopicC_Open = 1 ";
		$db->Where .= " AND Topic_Title LIKE '%".$Input."%' ";
		
		if( !empty($Cat) ) {
			
			$db->Where .= " AND a.TopicC_ID = '".$Cat."'";
		}
		
		$db->Order_By = " ORDER BY Topic_Sort DESC, Topic_ID DESC ";
		

		$db->query_sql( $Sheet , '*' );
		while( $row = $db->query_fetch() ){
					
			$_data[$row['Topic_ID']] = $row;
			$_other['catname'] = $row['TopicC_Name'];
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Topic_Title'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Topic_Content'] );
		}
		if( empty($_data) ){
			
			$db = new MySQL();
		
			$db->Where = " WHERE Topic_Open = 1 ";
			$db->Where .= " AND ( Topic_Postdate < '".date("Y-m-d")."'";
			$db->Where .= " OR  Topic_Postdate = '".date("Y-m-d")."' ) ";
			
			$db->Where .= " AND TopicC_Open = 1 ";
			$db->Where .= " AND Topic_Title LIKE '%".$Input."%' ";
			
			if( !empty($Cat) ) {
				
				$db->Where .= " AND TopicC_UpMID = '".$Cat."'";
			}
			
			$db->Order_By = " ORDER BY Topic_Sort DESC, Topic_ID DESC ";

			$db->query_sql( $Sheet , '*' );
			while( $row = $db->query_fetch() ){
						
				$_data[$row['Topic_ID']] = $row;
				
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Topic_Title'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Topic_Content'] );
			}
		}
		
		$_rs['Data'] 		= $_data;
		
		$_rs['sql'] 	= $db->sql;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//健康主題_搜尋
	function GET_HEALTH_CAT2_SEARCH( $Cat = '' ){
	
		$Sheet = "web_topicclass";
				
		$db = new MySQL();

		$db->Where = " WHERE TopicC_Open = 1 ";
		
		$db->Where .= " AND TopicC_Lv = '2' ";
		
		$db->Where .= " AND TopicC_ID = '".$Cat."' ";
		
		$db->Order_By = " ORDER BY TopicC_Sort DESC, TopicC_ID DESC ";

		$db->query_sql( $Sheet , 'TopicC_ID, TopicC_Name, TopicC_UpMID');
		
		if( $row = $db->query_fetch() ){
					
			// $row;
			$_data = $this->GET_HEALTH_CAT1($row['TopicC_UpMID']);
			$_data['lv2name']	= $row['TopicC_Name'];
			$_data['lv2id'] 	= $row['TopicC_ID'];
			
		}
		
		$_rs['Data'] 		= $_data;		
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------	
	//首頁輪播圖
	function GET_BANNER_LIST( ){
		
		$Sheet = "web_banner";
				
		$db = new MySQL();
		$db->Where = " WHERE Banner_Open = 1 ";
		
		$db->Order_By = " ORDER BY Banner_Sort DESC, Banner_ID DESC ";
		
		$db->query_sql( $Sheet , '*' );
		while( $row = $db->query_fetch() ){
					
			$_data[$row['Banner_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Banner_Title'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Banner_Title'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------	
	//首頁跑馬燈
	function GET_MARQUEE_LIST( ){
		
		$Sheet = "web_marquee";
				
		$db = new MySQL();
		$db->Where = " WHERE Marquee_Open = 1 ";
		
		$db->Order_By = " ORDER BY Marquee_Sort DESC, Marquee_ID DESC ";
		
		$db->query_sql( $Sheet , '*' );
		while( $row = $db->query_fetch() ){
					
			$_data[$row['Marquee_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Marquee_Title'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Marquee_Title'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------	
	//首頁訊息專區
	function GET_NEWS_INDEX( ){
		
		$Sheet = "web_news";
				
		$db = new MySQL();
		$db->Where = " WHERE News_Open = 1 ";
		$db->Where .= " AND '".date("Y-m-d H:i:s")."' Between News_PostDate AND News_EndDate ";
		
		$db->Order_By = " ORDER BY News_Sort DESC, News_ID DESC LIMIT 5";
		
		$db->query_sql( $Sheet , '*' );
		while( $row = $db->query_fetch() ){
					
			$_data_new[$row['News_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['News_Title'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['News_Title'] );
		}
				
		$db = new MySQL();
		$db->Where = " WHERE News_Open = 1 ";
		$db->Where .= " AND '".date("Y-m-d")."' Between News_PostDate AND News_EndDate ";
		$db->Where .= " AND News_Hot = 1 ";
		
		$db->Order_By = " ORDER BY News_Sort DESC, News_ID DESC LIMIT 5";
		
		$db->query_sql( $Sheet , '*' );
		while( $row = $db->query_fetch() ){
					
			$_data_hot[$row['News_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['News_Title'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['News_Title'] );
		}
		
		$_rs['Data_new'] 	= $_data_new;
		$_rs['Data_hot'] 	= $_data_hot;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------	
	//首頁網網相連
	function GET_LINK_INDEX( ){
		
		$Sheet = "web_links";
				
		$db = new MySQL();
		$db->Where = " WHERE Links_Open = 1 ";
		$db->Where .= " AND Links_IndexOpen = 1 ";
		
		$db->Order_By = " ORDER BY Links_Sort DESC, Links_ID DESC ";
		
		$db->query_sql( $Sheet , '*' );
		while( $row = $db->query_fetch() ){
					
			$_data[$row['Links_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Links_Title'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Links_Title'] );
		}
		
		$_rs['Data'] 		= $_data;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}
		
//-------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	function GET_EDITER( $input = '' ){
		
		
		$db = new MySQL();
		$db->Where = " WHERE Admin_ID = '".$input."'";
		$db->query_sql( 'sys_admin' , 'Admin_Name' );
		if( $row = $db->query_fetch() ){
					
			$_rs = $row['Admin_Name'];
		}else{
			
			$_rs = "暫無作者";
		}
		
		return $_rs;
	}
	
		
}
?>