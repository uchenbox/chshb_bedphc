<?php require_once("include/web.config.php");

$Input = GDC( $_GET['c'] , 'links');

$search = $Input['links_searchwords'];
$mode 	= $Input['mode'];

if( $mode=='search' ){
	
	$_Result 		= $CM->GET_LINK_SEARCH( $search );

	$_html			= $_Result['Data'];
	// print_r($_Result);
}else{
	
	$PageData['p'] = $_GET['p'];
	
	$_Result 		= $CM->GET_LINK_LIST( $_ID , $PageData );

	$Pages_Data 	= $_Result['PageData'];
	$_html			= $_Result['Data'];

}

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];

$_Title = "網網相連";
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="opc_info.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">便民服務</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="links.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">網網相連</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('service_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">網網相連</h2>
					<div class="mainContent nopd">
						<form id="forms" onSubmit="return false;">
							<div class="mainSearch">
								<label for="searchlinks">請輸入關鍵字查詢網網相連</label>
								<input type="text" class="input__style01" id="searchlinks" title="請輸入關鍵字查詢網網相連" placeholder="請輸入關鍵字" input-type="special" value="<?=$search?>">
								<button class="searchlinksbtn">查詢</button>
							</div>
						</form>
						<?php if( !empty($_html) ){ ?>
							<table class="mainTable">
								<tr>
									<th class="tb1"><span>類別</span></th>
									<th class="tb6"><span>標題</span></th>
								</tr>
								<?php foreach( $_html as $key => $val ){?>
								<tr>
									<td class="tb1" data-title="類別"><span><?=$val['LinksC_Name']?></span></td>
									<td class="tb6 left" data-title="標題"><a href="<?=$val['Links_Link']?>" target="_blank" title="另開新分頁前往<?=$val['Links_Title']?>"><?=$val['Links_Title']?></a></td>
								</tr>
								<?php }?>
							</table>
						<?php }else{?>
						
							<p class="nodata">尚無資料</p>
							
						<?php }?>
						<?php 
							if( $mode != 'search' && !empty($_html) ) { 
								require('page.php'); 
							}
						?>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>
<script>
$(document).ready(function(e) {
	
	$('.searchlinksbtn').click(function() {
		
		var searchlinks = $('#searchlinks').val();
		var type  = $(this).attr('data-type');
		var field = '#'+$(this).closest('form').attr('id');
		if( CheckInput(field) ){
			
			var Form_Data = '';
			Form_Data += '_href=links';
			Form_Data += '&_searchkey='+searchlinks;
			Form_Data += '&_mode=links';
			Form_Data += '&_modekey=links_searchwords';
			Form_Data += '&_type=links_search';
			
			Post_JS(Form_Data, 'web_post.php');
		}
	});
});
</script>