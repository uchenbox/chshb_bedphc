<?php require_once("include/web.config.php");

$PageData['p'] = $_GET['p'];

$_Result 		= $CM->GET_GLORY_LIST( $PageData );

$Pages_Data 	= $_Result['PageData'];
$_html			= $_Result['Data'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];

$_Title = "榮耀軌跡";
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="about.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">關於本所</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="glory.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">榮耀軌跡</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('about_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">榮耀軌跡</h2>
					<div class="mainContent nopd">
						<table class="mainTable">
							<tr>
								<th class="tb1"><span>年度</span></th>
								<th class="tb6"><span>項目</span></th>
								<th class="tb2"><span>得獎成績</span></th>
							</tr>
							<?php foreach( $_html as $key => $val ){?>
								<tr>
									<td class="tb1" data-title="年度"><span><?=$val['Glory_Year']?></span></td>
									<td class="tb6 left" data-title="項目"><span><?=nl2br($val['Glory_Title'])?></span></td>
									<td class="tb2" data-title="得獎成績"><span><?=$val['Glory_Content']?></span></td>
								</tr>
							<?php }?>
						</table>
					</div>
					<?php require('page.php')?>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>