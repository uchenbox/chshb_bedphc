<?php require_once("include/web.config.php");

$Input = GDC( $_GET['c'] , 'healthin' );

$DATA_Input = array(

	'ID' => $Input['healthin']
);

$_html 	= $CM->GET_HEALTH_DATA( $DATA_Input );


$_setting_['WO_Keywords'] 		.= $_html['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_html['SEO']['WO_Description'];

$_Title = $_html['Data']['Topic_Title'];
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="" rel="nofollow"  itemprop="url">
								<span itemprop="title">健康主題</span>
							</a> ›
						</li>

						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="" rel="nofollow"  itemprop="url">
								<span itemprop="title"><?=$_html['Data']['Topic_Title']?></span>
							</a>
						</li>


					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('health_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">健康主題</h2>
					<div class="mainContent">
						<h3 class="photoTitle"><?=$_html['Data']['Topic_Title']?></h3>
						<p class="photoDate"><?=$_html['Data']['Topic_Postdate']?></p>
						<div class="textcontent">
							<?=TurnSymbol($_html['Data']['Topic_Content'])?>
						</div>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>