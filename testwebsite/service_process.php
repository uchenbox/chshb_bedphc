<?php require_once("include/web.config.php");

$_Result 		= $CM->GET_SP_LIST( $_ID );

$_html			= $_Result['Data'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];

$_Title = "為民服務流程";
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="opc_info.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">便民服務</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="" rel="nofollow"  itemprop="url">
								<span itemprop="title">為民服務流程</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('service_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">為民服務流程</h2>
					<div class="mainContent">
						<!--<h3 class="photoTitle">預防注射服務流程</h3><br>-->
						<table class="mainTable">
							<tr>
								<th class="tb1"><span>日期</span></th>
								<th class="tb6"><span>標題</span></th>
							</tr>
							<?php foreach( $_html as $key => $val ){?>
								<tr>
									<td class="tb1" data-title="日期"><span><?=$val['SP_Postdate']?></span></td>
									<td class="tb6 left" data-title="標題"><a href="service_processin.php?c=<?=OEncrypt('sp_id='.$key , 'spin')?>" title="<?=$val['SP_Title']?>"><?=$val['SP_Title']?></a></td>
								</tr>
							<?php }?>
						</table>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>