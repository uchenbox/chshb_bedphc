<?php require_once("include/web.config.php");

$Input = GDC( $_GET['c'] , 'spin' );

$_SP		= $Input['sp_id'];

$_Result 		= $CM->GET_SP_LIST( $_SP );

$_html			= $_Result['Data'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];

$_Title = $_html[$_SP]['SP_Title'];
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="opc_info.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">便民服務</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="service_process.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">為民服務流程</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="" rel="nofollow"  itemprop="url">
								<span itemprop="title"><?=$_html[$_SP]['SP_Title']?></span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('service_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">為民服務流程</h2>
					<div class="mainContent">
						<h3 class="photoTitle"><?=$_html[$_SP]['SP_Title']?></h3><br>
						<div class="textcontent">
							<?=TurnSymbol($_html[$_SP]['SP_Content'])?>
							<!--行政組織<br>
鄉公所、衛生所、派出所、消防隊、分駐所<br><br>

學校與幼托園所<br>
二水國中、二水國小、源泉國小、二水國小附幼、翰霖幼稚園、二水鄉立托兒所、親親托兒所、可愛托兒所、大時代托兒所<br><br>

社區組織資源<br>
二水社區發展協會、聖化社區發展協會、文化社區發展協會、惠民社區發展協會、裕民社區發展協會、光化社區發展協會、過圳社區發展協會、五伯社區發展協會、海豐社區發展協會、合和社區發展協會、十五社區發展協會、復興社區發展協會、合興社區發展協會 
源泉社區發展協會、倡和社區發展協會、大園社區發展協會、修仁社區發展協會<br><br>

資源地點<br>
贊修宮、戶政事務所、安德宮、衛生所、二水鄉立圖書館、派出所、惠民社區活動中心、消防隊、受玄宮、南天宮、分駐所、光化社區活動中心、過圳社區活動中心、上豐社區活動中心、神農宮、受德宮、桃山廟、十五社區活動中心、奉天宮、復興社區活動中心、武天宮<br><br>-->
 	
						</div>
						<button class="btnstyle_return" onClick="location='service_process.php';">回列表</button>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>