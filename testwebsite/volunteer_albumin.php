<?php require_once("include/web.config.php");

$PageData['p'] = $_GET['p'];
$Input = GDC( $_GET['c'] , 'vtalbumin' );

$_ID		= $Input['vtalbumin_id'];

//----------------------------相簿----------------------------
$_Result 		= $CM->GET_VAL_ALBUM_SINGLE( $_ID );

$_html 			= $_Result['Data'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
//------------------------------------------------------------
//--------------------------照片list--------------------------

$_Result 		= $CM->GET_VAL_ALBUM_DATA( $_ID , $PageData );

$Pages_Data 	= $_Result['PageData'];
$_photo 		= $_Result['Data'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
//------------------------------------------------------------
// print_r($_photo);
$_Title = $_html['VTalbum_Title'];
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<link rel="stylesheet" href="stylesheets/baguetteBox.min.css">
	<script type="text/javascript"  src="js/baguetteBox.js"></script>
	<script>
		$(document).ready(function(){
			baguetteBox.run('.photolist', {});
		});
	</script>
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="volunteer.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">志工專區</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="volunteer_album.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">志工花絮</span>
							</a> <!--›-->
						</li>
						<!--<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="" rel="nofollow"  itemprop="url">
								<span itemprop="title"><?=$_html['VTalbum_Title']?></span>
							</a>
						</li>-->
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('volunteer_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">志工花絮</h2>
					<div class="mainContent">
						<h3 class="photoTitle"><?=$_html['VTalbum_Title']?></h3>
						<p class="photoDate"><?=$_html['VTalbum_Postdate']?></p>
						<ul class="photolist">
							<?php foreach( $_photo as $key => $val ){?>
								<li class="photolist__item">
									<a class="photolist__item__link" data-caption="<?=$val['VTalbuml_Title']?>" title="<?=$val['VTalbuml_Title']?>.<?=explode('.', $val['VTalbuml_Img'])[1]?>(點擊放大瀏覽)" href="<?=VTAlbum_Url.$val['VTalbum_ID'].'/'.$val['VTalbuml_Img']?>" >
										<div class="img" style="background-image: url('<?=VTAlbum_Url.$val['VTalbum_ID'].'/'.$val['VTalbuml_Img']?>');"></div>
										<p class="photolist__item__content"><?=$_html['VTalbum_Title']?>照片</p>
									</a>
								</li>
							<?php }?>
							
						</ul>

					</div>
					
					<?php require('page.php')?>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>