<?php require('header.php') ?>

	<article>
		<section class="bannerslider">
			<ul class="banner__slider">
				<!--banner圖 1600x800 -->
				<?php foreach( $_html_banner as $key => $val ){?>
					<li><a href="<?=$val['Banner_Link']?>" target="_blank" title=""><img src="<?=Banner_Url.'/'.$val['Banner_Mcp']?>" width="100%" alt="<?=$val['Banner_Title']?>(另開新視窗)"></a></li>
				<?php }?>
			</ul>
		</section>

		<section class="idxmarquee">
			<div class="container">
				<ul class="idxmarquee__list">
					<?php foreach( $_html_marquee as $key => $val ){?>
						<li><a href="<?=$val['Marquee_Link']?>" title="<?=$val['Marquee_Title']?>(另開新視窗)" target="_blank"><?=$val['Marquee_Title']?></a></li>
					<?php }?>
				</ul>
			</div>
			
		</section>

		<section id="main" class="idxservice">
			<div class="container">
				<a class="go_header" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
				<ul class="idxservice__list">
					<?php foreach( $_html_fast_links as $key => $val ){ ?>
						<li class="idxservice__list__item"><a href="<?=$val['IL_Content']?>"  title="<?=$val['IL_Title']?>"><img src="<?=Index_Links_Url.$val['IL_Mcp']?>" alt=""><p><?=$val['IL_Title']?></p></a></li>
					<?php } ?>
				</ul>
			</div>
		</section>


		<section class="idx idxnews">
			<div class="container">
				
				<h2 class="maintit maintit--news">訊息專區</h2>
				<ul class="idxnews__tab">
					<li class="idxnews__tab__item active" ><a onclick="return false;" href="#idxnews001" data-list="" title="最新消息" >最新消息</a></li>
					<ul class="idxnews__list active" id="idxnews001">
						<?php foreach( $_html_new as $key => $val ){?>
							<li class="idxnews__list__item">
								<p class="date"><?=explode(" ",$val['News_PostDate'])[0]?></p>
								<a href="newsin.php?c=<?=OEncrypt('news_in='.$key.'_new' , 'newsin')?>" title="<?=$val['News_Title']?>"><?=$val['News_Title']?></a>
							</li>
						<?php }?>
						<a class="more" href="news.php?c=<?=OEncrypt('news_cat=new' , 'newscat')?>" title="更多最新消息"><i class="fas fa-plus"></i>更多</a>
					</ul>
					<li class="idxnews__tab__item idxnews__tab__item--right" ><a onclick="return false;" href="#idxnews002" data-list="" title="熱門議題">熱門議題</a></li>
					<ul class="idxnews__list" id="idxnews002">
						<?php foreach( $_html_hot as $key => $val ){?>
							<li class="idxnews__list__item">
								<p class="date"><?=explode(" ",$val['News_PostDate'])[0]?></p>
								<a href="newsin.php?c=<?=OEncrypt('news_in='.$key.'_hot' , 'newsin')?>" title="<?=$val['News_Title']?>"><?=$val['News_Title']?></a>
							</li>
						<?php }?>
						<a class="more" href="news.php?c=<?=OEncrypt('news_cat=hot' , 'newscat')?>" title="更多熱門議題"><i class="fas fa-plus"></i>更多</a>
					</ul>
				</ul>
				<div class="newsbox">
					<h3 class="idxnews__tit">最新消息</h3>
					
					<h3 class="idxnews__tit">熱門議題</h3>
					
				</div>
				

			</div>
			
		</section>


		<section class="idx idxlink">
			<div class="container">
				<h2 class="maintit maintit--link">網網相連</h2>
				<ul class="idxlink__list">
					<!--圖 189x76 -->
					<?php foreach( $_html_links as $key => $val ){?>
					<li><a href="<?=$val['Links_Link']?>" target="_blank" title="另開新分頁前往<?=$val['Links_Title']?>" style="background-image: url(<?=Links_Url.'/'.$val['Links_Mcp']?>);"><?=$val['Links_Title']?></a></li>
					<?php }?>
				</ul>
				<a class="more" href="links.php" title="看更多連結"><i class="fas fa-plus"></i>更多</a>
			</div>
		</section>
	</article>

	<?php require('footer.php')?>