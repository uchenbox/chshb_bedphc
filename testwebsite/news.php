<?php require_once("include/web.config.php");

$Input = GDC( $_GET['c'] , 'newscat' );

//$news_cat		= $Input['news_cat'];
$news_cat 		= $Input['news_cat']??'new';
$PageData['p'] = $_GET['p'];
$_Result 		= $CM->GET_NEWS_LIST( $_ID , $news_cat , $PageData );

$Pages_Data 	= $_Result['PageData'];
$_html			= $_Result['Data'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];


if( $news_cat=='new' ) {
	
	$_Title = "最新消息";
}else if( $news_cat=='hot' ){

	$_Title = "熱門議題";
}



?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="news.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">訊息專區</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="glory.php" rel="nofollow"  itemprop="url">
								<span itemprop="title"><?=$_Title?></span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('news_aside.php')?>


				<div class="main">
					<h2 class="mainTitle"><?=$_Title?></h2>
					<div class="mainContent nopd">
						<table class="mainTable">
							<tr>
								<th class="tb1"><span>日期</span></th>
								<th class="tb6"><span>標題</span></th>
							</tr>
							<?php foreach( $_html as $key => $val ){?>
								<tr>
									<td class="tb1" data-title="日期"><span><?=explode(" ",$val['News_PostDate'])[0]?></span></td>
									<td class="tb6 left" data-title="標題"><a href="newsin.php?c=<?=OEncrypt('news_in='.$key.'_'.$news_cat , 'newsin')?>" title="<?=$val['News_Title']?>"><?=$val['News_Title']?></a></td>
								</tr>
							<?php }?>
						</table>
					</div>
					<?php require('page.php')?>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>