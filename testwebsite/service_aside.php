<aside class="aside">
	<p class="aside__tit">便民服務</p>
	<ul class="mnav">

		<li class="mnav__item <?=PHP_NAME=='opc_info'?'current':''?>"><a href="opc_info.php" title="門診資訊">門診資訊</a></li>
		<li class="mnav__item" titlex="為民服務流程"><a href="" title="為民服務流程(點擊展開次選單)">為民服務流程</a>
			<ul class="mnav__item__list">
				<li class="mnav__item <?=!isset($_SP)&&PHP_NAME=='service_process'?'current':''?>"><a href="service_process.php" title="為民服務流程全部分類">全部</a></li>
				<?php foreach( $_html_SP as $key => $val ){?>
				<li class="mnav__item <?=$_SP==$key?'current':''?>"><a href="service_processin.php?c=<?=OEncrypt('sp_id='.$key , 'spin')?>" title="<?=$val['SP_Title']?>"><?=$val['SP_Title']?></a></li>
				<?php }?>
			</ul>
		</li>
		<?php if( $GOV_TF==true ){?>
			<li class="mnav__item <?=PHP_NAME=='gov_info'?'current':''?>"><a href="gov_info.php" title="政府公開資訊">政府公開資訊</a></li>
		<?php }?>
		<li class="mnav__item <?=PHP_NAME=='service_plan'||PHP_NAME=='service_planin'?'current':''?>"><a href="service_plan.php" title="服務品質計畫">服務品質計畫</a></li>
		<li class="mnav__item <?=PHP_NAME=='download_all'?'current':''?>" titlex="檔案下載"><a href="" title="檔案下載(點擊展開次選單)" >檔案下載</a>
			<ul class="mnav__item__list">
				<li class="mnav__item <?=!isset($_DL)&&PHP_NAME=='download_all'?'current':''?>"><a href="download_all.php" title="所有檔案">所有檔案</a></li>
				<?php foreach( $_html_DL as $key => $val ){?>
					<li class="mnav__item <?=$_DL==$key?'current':''?>" ><a href="download_all.php?c=<?=OEncrypt('dl_cat='.$key , 'dlcat')?>" title="<?=$val['DownloadC_Name']?>"><?=$val['DownloadC_Name']?></a></li>
				<?php }?>
			</ul>
		</li>
		<?php if( $VIDEO_TF==true ){?>
			<li class="mnav__item <?=PHP_NAME=='video'?'current':''?>"><a href="video.php" title="影音資料">影音資料</a></li>
		<?php }?>
		<li class="mnav__item" titlex="網網相連"><a href="" title="網網相連(點擊展開次選單)">網網相連</a>
			<ul class="mnav__item__list">
				<li class="mnav__item <?=!isset($_LK)&&PHP_NAME=='links'?'current':''?>"><a href="links.php"title="所有連結">所有連結</a></li>
				<?php foreach( $_html_LK as $key => $val ){?>
					<li class="mnav__item <?=$_LK==$key?'current':''?>"><a href="linksin.php?c=<?=OEncrypt('lk_id='.$key , 'lkin')?>" title="<?=$val['LinksC_Name']?>"><?=$val['LinksC_Name']?></a></li>
				<?php }?>
			</ul>
		</li>
	</ul>
</aside>