<?php require_once("include/web.config.php");

$Input = GDC( $_GET['c'] , 'gov_info');

$search		= $Input['gov_searchwords'];
$mode 		= $Input['mode'];

if( $mode=='search' ){

	$_Result 		= $CM->GET_GOV_SEARCH( $search );
	
	$_htmlS1		= $_Result['DataS'];
	if( empty($_Result['DataS2']) ){
		
		$_htmlS2		= $_Result['DataS'];
	}else if( !empty($_Result['DataS']) ){
		
		$_htmlS2		= $_Result['DataS2'];
	}
	if( !empty($_Result['DataX']) ){
		
		$_htmlX			= $_Result['DataX'];
	}
	if( !empty($_Result['DataX1']) ){
		
		$_htmlX1		= $_Result['DataX1'];
	}
// print_r($_htmlX1);
}else{
	
	$PageData['p'] = $_GET['p'];

	$_Result 		= $CM->GET_GOV_LIST( $PageData );

	$Pages_Data 	= $_Result['PageData'];
	$_htmlA			= $_Result['DataA'];
	$_htmlB			= $_Result['DataB'];

}
$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];

$_Title = "政府公開資訊";
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="opc_info.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">便民服務</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="gov_info.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">政府公開資訊</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('service_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">政府公開資訊</h2>
					<div class="mainContent nopd">
						<form id="forms" onSubmit="return false;">
							<div class="mainSearch">
								<label for="searchgov">請輸入關鍵字查詢政府公開資訊</label>
								<input type="text" class="input__style01" id="searchgov" title="請輸入關鍵字查詢政府公開資訊" placeholder="請輸入關鍵字" input-type="special" value="<?=$search?>">
								<button class="searchgovbtn">查詢</button>
							</div>
						</form>
						<?php if( !empty($_htmlA)&&!empty($_htmlB)&&$mode!='search' ){ ?>
							<table class="infoTable">
								<tr>
									<th class="tb3 center"><span>公開項目</span></th>
									<th class="tb2 center"><span>公開情形及相關連結</span></th>
								</tr>
								<?php foreach( $_htmlA as $keyA => $valA ){?>
									<tr>
										<td class="tb3" data-title="公開項目">
											<span><?=$valA['GPI_Name']?></span>
										</td>
										<td class="tb2" data-title="公開情形及相關連結">
											<div>
											<?php foreach( $_htmlB as $keyB => $valB ){?>
												<?php if( $valB['GPI_UpMID']==$keyA ){?>
													<?php if( !empty($valB['GPI_Link']) ){?>
														<a href="<?=$valB['GPI_Link']?>" target="_blank" title="另開新視窗前往<?=$valB['GPI_Name']?>"><?=$valB['GPI_Name']?></a>
													<?php }else if( empty($valB['GPI_Link']) ){?>
														<p><?=$valB['GPI_Name']?></p>
													<?php }?>
												<?php }?>
											<?php }?>
											</div>
										</td>
									
									</tr>
								<?php }?>
							</table>
						<?php }else if( !empty($_htmlS1)&&!empty($_htmlS2)&&$mode=='search' ){ ?>
							<table class="infoTable">
								<tr>
									<th class="tb3 center"><span>公開項目</span></th>
									<th class="tb2 center"><span>公開情形及相關連結</span></th>
								</tr>
								<?php foreach( $_htmlS1 as $keyA => $valA ){?>
									<tr>
										<td class="tb3" data-title="公開項目">
											<span><?=$valA['GPI_Name']?></span>
										</td>
										<td class="tb2" data-title="公開情形及相關連結">
											<div>
											<?php foreach( $_htmlS2 as $keyB => $valB ){?>
												<?php if( $valB['GPI_UpMID']==$valA['GPI_ID'] ){?>
													<?php if( !empty($valB['GPI_Link']) ){?>
														<a href="<?=$valB['GPI_Link']?>" target="_blank" title="另開新視窗前往<?=$valB['GPI_Name']?>"><?=$valB['GPI_Name']?></a>
													<?php }else if( empty($valB['GPI_Link']) ){?>
														<p><?=$valB['GPI_Name']?></p>
													<?php }?>
												<?php }?>
											<?php }?>
											</div>
										</td>
									</tr>
								<?php }?>
							</table>
						<?php }else if( !empty($_htmlX1)&&!empty($_htmlX)&&$mode=='search' ){ ?>
							<table class="infoTable">
								<tr>
									<th class="tb3 center"><span>公開項目</span></th>
									<th class="tb2 center"><span>公開情形及相關連結</span></th>
								</tr>
								<?php foreach( $_htmlX1 as $keyA => $valA ){?>
									<tr>
										<td class="tb3" data-title="公開項目">
											<span><?=$valA['GPI_Name']?></span>
										</td>
										<td class="tb2" data-title="公開情形及相關連結">
											<div>
											<?php foreach( $_htmlX as $keyB => $valB ){?>
												<?php if( $valB['GPI_UpMID']==$valA['GPI_ID'] ){?>
													<?php if( !empty($valB['GPI_Link']) ){?>
														<a href="<?=$valB['GPI_Link']?>" target="_blank" title="另開新視窗前往<?=$valB['GPI_Name']?>"><?=$valB['GPI_Name']?></a>
													<?php }else if( empty($valB['GPI_Link']) ){?>
														<p><?=$valB['GPI_Name']?></p>
													<?php }?>
												<?php }?>
											<?php }?>
											</div>
										</td>
									</tr>
								<?php }?>
							</table>
						<?php }else{?>
						
							<p class="nodata">尚無資料</p>
							
						<?php }?>
						<?php 
							if( $mode != 'search' ) { 
								require('page.php'); 
							}
						?>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>
<script>
$(document).ready(function(e) {
	
	$('.searchgovbtn').click(function() {
		
		var searchgov = $('#searchgov').val();
		var type  = $(this).attr('data-type');
		var field = '#'+$(this).closest('form').attr('id');
		if( CheckInput(field) ){
			
			var Form_Data = '';
			Form_Data += '_href=gov_info';
			Form_Data += '&_searchkey='+searchgov;
			Form_Data += '&_mode=gov_info';
			Form_Data += '&_modekey=gov_searchwords';
			Form_Data += '&_type=gov_search';
			
			Post_JS(Form_Data, 'web_post.php');
		}
	});
});
</script>