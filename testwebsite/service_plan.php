<?php require_once("include/web.config.php");

$PageData['p'] = $_GET['p'];

$_Result 		= $CM->GET_SQ_LIST( $_ID, $PageData );

$Pages_Data 	= $_Result['PageData'];
$_html			= $_Result['Data'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];

$_Title = "服務品質計畫";
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="opc_info.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">便民服務</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="service_plan.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">服務品質計畫</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('service_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">服務品質計畫</h2>
					<div class="mainContent nopd">
						<table class="mainTable">
							<tr>
								<th class="tb1"><span>項目</span></th>
							</tr>
							<?php foreach( $_html as $key => $val ){?>
								<tr>
									<td class="tb1 left" data-title="項目"><a href="service_planin.php?c=<?=OEncrypt('sq_id='.$key , 'sqin')?>" title="<?=$val['SQplan_Title']?>"><?=$val['SQplan_Title']?></a></td>
								</tr>
							<?php }?>
						</table>
						<?php require('page.php')?>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>