<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=1"  /> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author"  content="華越資通,www.bm888.com.tw" />
<meta name="keywords" Lang="EN" content="<?=$_setting_['WO_Keywords']?>"/>
<meta name="keywords" Lang="zh-TW" content="<?=$_setting_['WO_Keywords']?>" />
<meta name="Description" Content="<?=$_setting_['WO_Description']?>"/>
<meta name="COPYRIGHT" content="Copyright (c) by 華越資通,www.bm888.com.tw">
<?php if( PHP_NAME=='en' ){ ?>
	
	<title><?=$_Title.'-'.$_setting2_['Setting_Title_EN']?></title>
<?php }else{?>

	<title><?=$_Title.'-'.$_setting_['WO_Title']?></title>
<?php }?>
<meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="shortcut icon" href="favicon.ico"/>
<link rel="bookmark" href="favicon.ico"/>


<link rel="stylesheet" type="text/css" href="stylesheets/cssreset.css" />
<script src="js/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="stylesheets/base.css?v=<?=$version?>" />
<link rel="stylesheet" type="text/css" href="stylesheets/nav.css?v=<?=$version?>" />
<link rel="stylesheet" type="text/css" href="stylesheets/<?=$_CurrentStyle['css']?>.css?v=<?=$version?>" />


<link rel="stylesheet" type="text/css" href="./slick/slick.css">
<link rel="stylesheet" type="text/css" href="./slick/slick-theme.css">
<script type="text/javascript" src="slick/slick.js" defer></script>

<script src="js/ani.js" defer></script>
<script src="js/jquery.cookie.js" defer></script>
<script src="js/jquery.navgoco.js" defer></script>


<script src="js/web-main.js" defer></script>
<script src="js/web-checkinput.js" defer></script>
<script src="js/jquery-loading.js" defer></script>

<noscript>
	<link rel="stylesheet" type="text/css" href="stylesheets/nos_nav.css?v=<?=$version?>" />
</noscript>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script defer src="https://www.googletagmanager.com/gtag/js?id=<?=$_setting_['WO_GAnalytics']?>"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '<?=$_setting_['WO_GAnalytics']?>');
</script>