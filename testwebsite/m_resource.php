<?php require_once("include/web.config.php");

$_Result 		= $CM->GET_MR_LIST( $_ID );

$_html			= $_Result['Data'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];

$_Title = "本鄉鎮醫療資源";
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="" rel="nofollow"  itemprop="url">
								<span itemprop="title">醫療及社區資源</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="m_resource.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">本鄉鎮醫療資源</span>
							</a> 
						</li>
						<!--
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="m_resource.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">醫療資源A</span>
							</a>
						</li>
						-->
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('resource_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">本鄉鎮醫療資源</h2>
					<div class="mainContent nopd">
						<!--<h4 class="photoTitle">醫療資源A</h4><br>-->
						<table class="mainTable">
							<tr>
								<th class="tb1"><span>日期</span></th>
								<th class="tb6"><span>標題</span></th>
							</tr>
							<?php foreach( $_html as $key => $val ){?>
								<tr>
									<td class="tb1" data-title="日期"><span><?=$val['MR_Postdate']?></span></td>
									<td class="tb6 left" data-title="標題"><a href="m_resourcein.php?c=<?=OEncrypt('mr_id='.$key , 'mrin')?>" title="<?=$val['MR_Title']?>"><?=$val['MR_Title']?></a></td>
								</tr>
							<?php }?>
						</table>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>