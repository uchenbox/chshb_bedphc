<script>
	$(document).ready(function(){

		  $('.aside__cat__sec').find("li.current").parents('ul').css('display','block').parents('li').addClass('current');


  $('.nav li').has("ul").children('a').attr('href','javascript:void(0);');
  $(".nav>li>a").click(function(){
       $(".nav__item__list").slideUp();
        

        if(($(this).siblings(".nav__item__list")).is(":visible")){
          $(this).siblings(".nav__item__list").slideUp();
        }else{
         $(this).siblings(".nav__item__list").slideDown();
        }
  });

  $('.nav__item__list li').has("ul").children('a').attr('href','javascript:void(0);');
  $(".nav__item__list li>a").click(function(){
        $(".nav__item__second__list").slideUp();
        if(($(this).siblings(".nav__item__second__list")).is(":visible")){
          $(this).siblings(".nav__item__second__list").slideUp();
        }else{
         $(this).siblings(".nav__item__second__list").slideDown();
        }
  });


 	 });
</script>

<div class="htmlTop">
	<div class="headerWrapper">
		<header id="header">
			<div class="header__top">
				<a class="go_main"  href="#Accesskey_M" title="前往主要區塊">前往主要區塊</a>
				
				<h1><a href="index.php" title="<?=$_setting_['WO_Name']?>"><img src="images/logo.png" alt=""><?=$_setting_['WO_Name']?></a></h1>
				<button class="mobile_btn">手機板網站選單開關按鈕<div id="nav-icon4"> <span></span> <span></span> <span></span></div>
				</button>
				
			</div>

		</header>

		<nav class="header__nav">
			<ul class="nav">
				<li class="nav__item <?=PHP_NAME=='about'||PHP_NAME=='business'||PHP_NAME=='glory'||PHP_NAME=='headman'||PHP_NAME=='traffic'||PHP_NAME=='album'||PHP_NAME=='albumin'||PHP_NAME=='contact'?'current':''?>"><a class="nav__item__a" href="about.php"  title="關於本所"><img src="images/nav01.png" alt="">關於本所</a>
					<ul class="nav__item__list">
						<li><a href="about.php" title="本所簡介">本所簡介</a></li>
						<li><a href="business.php" title="業務職掌">業務職掌</a></li>
						<li><a href="glory.php" title="榮耀軌跡">榮耀軌跡</a></li>
						<li><a href="headman.php" title="歷任首長">歷任首長</a></li>
						<li><a href="traffic.php" title="交通資訊">交通資訊</a></li>
						<li><a href="album.php" title="活動花絮">活動花絮</a></li>
						<li><a href="<?=$_setting2_['Setting_OverviewofPHI']?>" title="另開新視窗前往<?=$_setting_['WO_Title']?>公衛指標概覽" target="_blank">公衛指標概覽</a></li>
						<li><a href="contact.php" title="民意信箱">民意信箱</a></li>
					</ul>
					
				</li>
				<li class="nav__item <?=PHP_NAME=='m_resource'||PHP_NAME=='m_resourcein'||PHP_NAME=='c_resource'?'current':''?>"><a href="m_resource.php"  title="醫療及社區資源"><img src="images/nav02.png" alt="">醫療及社區資源</a>
					<ul class="nav__item__list">
						<li class="nav__item__second"><a href="m_resource.php" title="本鄉鎮醫療資源">本鄉鎮醫療資源</a>
							<ul class="nav__item__list nav__item__second__list">
								<li><a href="m_resource.php" title="本鄉鎮醫療資源全部資源">全部</a></li>
								<?php foreach( $_html_MR as $key => $val ){?>
									<li><a href="m_resourcein.php?c=<?=OEncrypt('mr_id='.$key , 'mrin')?>" title="<?=$val['MR_Title']?>"><?=$val['MR_Title']?></a></li>
								<?php }?>
							</ul>
						</li>
						<li><a href="c_resource.php" title="社區資源">社區資源</a></li>
					</ul>
				</li>
				<li class="nav__item <?=PHP_NAME=='opc_info'||PHP_NAME=='service_process'||PHP_NAME=='gov_info'||PHP_NAME=='service_plan'||PHP_NAME=='download_all'||PHP_NAME=='video'||PHP_NAME=='links'||PHP_NAME=='linksin'?'current':''?>" tabindex=""><a href="opc_info.php"  title="便民服務"><img src="images/nav03.png" alt="">便民服務</a>
					<ul class="nav__item__list">
						<li ><a href="opc_info.php" title="門診資訊">門診資訊</a></li>
						<li class="nav__item__second" tabindex=""><a href="" title="為民服務流程">為民服務流程</a>
							<ul class="nav__item__list nav__item__second__list">
								<li><a href="service_process.php" title="為民服務流程全部分類">全部</a></li>
								<?php foreach( $_html_SP as $key => $val ){?>
									<li><a href="service_processin.php?c=<?=OEncrypt('sp_id='.$key , 'spin')?>" title="<?=$val['SP_Title']?>"><?=$val['SP_Title']?></a></li>
								<?php }?>
							</ul>
						</li>
						<?php if( $GOV_TF==true ){?>
							<li><a href="gov_info.php" title="政府公開資訊">政府公開資訊</a></li>
						<?php }?>
						<li class="nav__item__second"><a href="service_plan.php" title="服務品質計畫">服務品質計畫</a></li>
						<li class="nav__item__second"><a href="download_all.php" title="檔案下載">檔案下載</a>
							<ul class="nav__item__list nav__item__second__list">
								<li><a href="download_all.php" title="所有檔案">所有檔案</a></li>
								<?php foreach( $_html_DL as $key => $val ){?>
									<li><a href="download_all.php?c=<?=OEncrypt('dl_cat='.$key , 'dlcat')?>" title="<?=$val['DownloadC_Name']?>"><?=$val['DownloadC_Name']?></a></li>
								<?php }?>
							</ul>
						</li>
						<?php if( $VIDEO_TF==true ){?>
							<li class="nav__item__second"><a href="video.php" title="影音資料">影音資料</a></li>
						<?php }?>
						<li class="nav__item__second"><a href="links.php" title="網網相連">網網相連</a>
							<ul class="nav__item__list nav__item__second__list">
								<li><a href="links.php" title="所有連結">所有連結</a></li>
								<?php foreach( $_html_LK as $key => $val ){?>
									<li><a href="linksin.php?c=<?=OEncrypt('lk_id='.$key , 'lkin')?>" title="<?=$val['LinksC_Name']?>"><?=$val['LinksC_Name']?></a></li>
								<?php }?>
							</ul>
						</li>
					</ul>
				</li>
				<li class="nav__item <?=PHP_NAME=='news'?'current':''?>" tabindex=""><a href="news.php?c=<?=OEncrypt('news_cat=new' , 'newscat')?>" title="訊息專區"><img src="images/nav04.png" alt="">訊息專區</a>
					<ul class="nav__item__list">
						<li class="nav__item__second"><a href="news.php?c=<?=OEncrypt('news_cat=new' , 'newscat')?>" title="最新消息">最新消息</a></li>
						<li class="nav__item__second"><a href="news.php?c=<?=OEncrypt('news_cat=hot' , 'newscat')?>" title="熱門議題">熱門議題</a></li>
					</ul>
				</li>
				<li class="nav__item <?=PHP_NAME=='volunteer'||PHP_NAME=='volunteer_album'||PHP_NAME=='volunteer_albumin'?'current':''?>" tabindex=""><a href="volunteer.php"  title="志工專區"><img src="images/nav05.png" alt="">志工專區</a>
					<ul class="nav__item__list">
						<li class="nav__item__second"><a href="volunteer.php" title="志工服務">志工服務</a></li>
						<li class="nav__item__second"><a href="volunteer_album.php" title="志工花絮">志工花絮</a></li>
					</ul>
				</li>
				<li class="nav__item <?=PHP_NAME=='health'||PHP_NAME=='healthin'?'current':''?>" tabindex=""><a href="" title="健康主題"><img src="images/nav06.png" alt="">健康主題</a>
				<ul class="nav__item__list">
					<?php foreach( $_html_HEALTH_MENU as $key => $val ){?>
						<li class="nav__item__second"><a href="healthin.php?c=<?=OEncrypt('healthin='.$key , 'healthin')?>" title="<?=$val['Topic_Title']?>"><?=$val['Topic_Title']?></a></li>
					<?php }?>
				
				</ul>
			</li>
				
			</ul>
		</nav>
	</div>
	



<div class="body">
	<div class="header__top__link">
		<div class="top">
			<a id="Accesskey_U" accesskey="U" href="#Accesskey_U" title="上方選單連結區，此區塊列有本網站的主要連結">:::</a><a href="index.php" title="回首頁">回首頁</a> |<a   title="網站導覽" href="sitemap.php">網站導覽</a>	|<a href="en.php" title="前往彰化衛生局英文版網頁">English</a>
		</div>
		<!--<div class="searchbox">
			<label for="search">請輸入搜尋文字</label><input id="search" type="text">
			<button><i class="fas fa-search"></i></button>

		</div>-->
		
		<script async src="https://cse.google.com/cse.js?cx=018145196379303660891:jtpwfmbyjnl"></script>
		<div class="gcse-search"></div>
	</div>