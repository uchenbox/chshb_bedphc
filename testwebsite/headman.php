<?php require_once("include/web.config.php");

$_Result 		= $CM->GET_HEADMAN_LIST();
$_html 			= $_Result['Data'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];

$_Title = "歷任首長";
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="about.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">關於本所</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="headman.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">歷任首長</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('about_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">歷任首長</h2>
					<div class="mainContent nopd">
						<table class="mainTable">
							<tr>
								<th class="tb1"><span>任別</span></th>
								<th class="tb1"><span>姓名</span></th>
								<th class="tb1 "><span>到職日期</span></th>
								<th class="tb1"><span>離職日期</span></th>
								<th class="tb3"><span>備註</span></th>

							</tr>
							<?php foreach( $_html as $key => $val ){?>
								<tr>
									<td class="tb1" data-title="任別"><span><?=$val['Head_Position']?></span></td>
									<td class="tb1" data-title="姓名"><span><?=$val['Head_Name']?></span></td>
									<td class="tb1" data-title="到職日期"><span><?=$val['Head_StartDate']?></span></td>
									<td class="tb1" data-title="離職日期"><span><?=$val['Head_EndDate']?></span></td>
									<td class="tb3" data-title="備註"><span><?=$val['Head_Note']?></span></td>
								</tr>
							<?php }?>
							
						</table>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>