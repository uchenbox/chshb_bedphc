<?php require_once("include/web.config.php");

$_SESSION['chshb']['website']['formsigncode'] = md5(time());

$_Title = "民意信箱";
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="about.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">關於本所</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="contact.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">民意信箱</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('about_aside.php')?>

				<form id="form1" method="post" enctype="multipart/form-data" action="uploadfile.php">
					<div class="main">
						<h2 class="mainTitle">民意信箱</h2>
						<div class="mainContent contact">
							<div class="contact__imfo">
								<!-- 純文字 -->
								<div class="textcontent"><?=$_setting2_['Setting_MailBoxIntro']?></div>
							</div>

							<div class="formbox">
								<p class="fontstyle01">標示  <span class="star">*</span> 為必填欄位</p>
							</div>
							<div class="formbox">
								<label class="fontstyle01" for="name"><span class="star">*</span>請填入真實姓名(必填)</label>
								<input title="請填入真實姓名" id="name" name="name" class="input__style01" type="text" placeholder="ex:王小明" input-type="realname" msg="請輸入姓名" onkeyup="ValidateName($(this),value,'請輸入姓名')">
							</div>
							<div class="formbox">
								<label class="fontstyle01" for="email"><span class="star">*</span>請填入Email(必填)</label>
								<input title="請填入Email" id="email" name="email" class="input__style01" type="text" placeholder="ex:abc@abc.com" input-type="email" msg="請輸入Email" onkeyup="ValidateEmail($(this),value)">
								<p class="fontstyle02">【用於案件驗證及處理情形回覆，請確認正確及有效，因Yahoo或hotmail等免費信箱常將通知信誤認為廣告信件，以致您收不到信，建議改用其他信箱。】</p>
							</div>
							<div class="formbox">
								<label class="fontstyle01" for="tel">請填入電話</label>
								<input id="tel" title="請填入電話" name="tel" class="input__style01" type="text" placeholder="ex:04-8794080#0000" input-type="tel" minlength="7" maxlength="15" onkeyup="ValidateTel($(this),value,'7','15')">
								<p class="fontstyle02">格式為：04-8794080#0000</p>
							</div>
							<div class="formbox">
								<label class="fontstyle01" for="title"><span class="star">*</span>請填入主旨(必填)</label>
								<input id="title" title="請填入主旨" name="title" class="input__style01"  type="text" placeholder="ex:留言主題" input-type="title" msg="請填入主旨" onkeyup="ValidateTitle($(this),value)">
							</div>
							<div class="formbox">
								<label class="fontstyle01" for="content"><span class="star">*</span>請填入留言內容(必填)</label>
								<textarea class="textarea__style01" title="請填入留言內容" name="content" id="content"placeholder="ex:留言內容" input-type="message" msg="請填入留言內容" onkeyup="ValidateMsg($(this),value)"></textarea>
							</div>
							<div class="formbox formbox--file">
							
								<br>
								
								<div class="formbox__file">
									<label class="fontstyle01"  for="file01">上傳附加檔案一</label>
									<input id="file01" name="file[]" type="file" title="上傳附加檔案一">
								</div>
								<div class="formbox__file">
									<label class="fontstyle01"  for="file02">上傳附加檔案二</label>
									<input id="file02" name="file[]" type="file" title="上傳附加檔案二">
								</div>
								<div class="formbox__file">
									<label class="fontstyle01"  for="file03">上傳附加檔案三</label>
									<input id="file03" name="file[]" type="file" title="上傳附加檔案三">
								</div>
								<div class="formbox__file">
									<label class="fontstyle01"  for="file04">上傳附加檔案四</label>
									<input id="file04" name="file[]" type="file" title="上傳附加檔案四">
								</div>
								<div class="formbox__file">
									<label class="fontstyle01"  for="file05">上傳附加檔案五</label>
									<input id="file05" name="file[]" type="file" title="上傳附加檔案五">
								</div>

							</div>
							<div class="formbox">
								<label class="fontstyle01">【請注意:可上傳之附加檔案格式為PDF，JPG，BMP和PNG檔，單檔大小不大於5MB，每案可上傳5個附件。】</label>
							</div>
							
							<!--<!--<div class="formbox formbox--code">
								<label class="fontstyle01" for=""><span class="star">*</span>驗證碼</label>
								<div class="code">
									<input type="text">
									<img src="" alt="">
									<button>換一個驗證碼</button>
								</div>
							</div>-->
							<!--<input type="hidden" name="formsigncode" value="<?=$_SESSION['chshb']['website']['formsigncode']?>">-->
							<div class="btnlist">
								<button type="reset" class="btnstyle02 btnstyle02--gray">重填</button>
								<button class="btnstyle02 btnstyle02--green submit">送出</button>
							</div>
							<input type="hidden" name="type" value="msg_save">
						</div>
					</div>
				</form>
			
			</section>
			</div>

		</article>

		<?php require('footer.php')?>

	</div>
	
</body>
</html>
<script>

$(document).ready(function(e) {
	
	$('.submit').click(function() {
		loading('open');
		
		if( CheckInput('#form1') ){
			
			$('#form1').submit();	
				
		}else{
			loading('close');
			return false;
		}
	});
	
});

</script>