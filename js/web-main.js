var Exec_Url = 'web_post.php';

var allow_file = new Array('jpg', 'jpeg', 'png', 'gif', 'doc', 'docx', 'pdf', 'ppt', 'pptx', 'xls', 'xlsx', 'csv');

function Post_JS( FormData, ExecUrl, Field ){
	
	loading('open');
	
	$.post( ExecUrl, FormData 
		,function( data ){
			
			loading('close');
			
			if( data.html_msg != '' && data.html_msg != null ){
				
				alert(data.html_msg);
				if( data.html_eval != '' && data.html_eval != null ){
					
					setTimeout(data.html_eval, 1);
				}else if( data.html_href != '' && data.html_href != null ){
					
					window.location.href = data.html_href;
				}
				
				return false;
			}
			
			if( data.html_href != '' && data.html_href != null ){
				
				window.location.href = data.html_href;
				return false;
			}

			if( data.html_content != '' && data.html_content != null ){
				
				$(Field).html(data.html_content);
				
				return false;
			}
			
			if( data.html_eval != '' && data.html_eval != null ){
				
				setTimeout(data.html_eval, 1);
				return false;
			}
		},'json'
	);
}

function Ajax_Post( FormData, ExecUrl, Field ){
	
	loading('open');
		
	$.ajax({
		url: ExecUrl,
		data: FormData,
		type: "POST",
		dataType: 'json',
		mimeType:"multipart/form-data",
		contentType: false,
		cache: false,
		processData:false,
		success: function(data){
				
			loading('close');
											
			if( data.html_msg != '' && data.html_msg != null ){
				
				alert(data.html_msg);
				if( data.html_eval != '' && data.html_eval != null ){
					
					setTimeout(data.html_eval, 1);
				}else if( data.html_href != '' && data.html_href != null ){
					
					window.location.href = data.html_href;
				}
				
				return false;
			}
			
			if( data.html_href != '' && data.html_href != null ){
				
				window.location.href = data.html_href;
				return false;
			}

			if( data.html_content != '' && data.html_content != null ){
				
				$(Field).html(data.html_content);
				
				return false;
			}
			
			if( data.html_eval != '' && data.html_eval != null ){
				
				setTimeout(data.html_eval, 1);
				return false;
			}
		},
		error:function(xhr, ajaxOptions, thrownError){ 
			
			alert(thrownError);
		}
	});
}

//網址刷新
function Reload(){
	
	window.location.reload();
}

var myVar;

function loading( Type ){
	
	if( Type == 'open' ){
		
		myVar = setTimeout(loading_show, 1);//12秒
		//$(document).Loading({ _action : true });
		
	}else if( Type == 'close' ){
		
		$(document).Loading({ _action : false });	
		
		clearTimeout(myVar);	
	}	
}

function loading_show(){
	
	$(document).Loading({ _action : true });
}

function loading_longtime(){
	
	$(".tc_box").BoxWindow({
		_msg: '讀取太久, 請重新登入',//訊息
		_url: 'index.php'
	});
}

function reimg(){
	
	var _this = $('.reimg');
	var src   = _this.attr('src').split("?");
	
	_this.attr('src', src[0]+'?code=' + Math.random());
}

function logout(){
	
	var Form_Data = '_type=mlogout';
		
	Post_JS(Form_Data, Exec_Url);
}

function Ajax_Chk( _this ){
		
	var msg		 		= '';
	var type	 		= _this.attr('check-type') ? _this.attr('check-type') : '';//資料第一種類
	var id	  	 		= _this.attr('check-id') ? _this.attr('check-id') : '';//主鍵
	var field			= _this.attr('check-field') ? _this.attr('check-field') : '';//更新欄位
	var data_new 		= _this.val() ? _this.val() : '';//資料值
	var data_old 		= _this.attr('check-data') ? _this.attr('check-data') : '';//資料舊值
	var sn 		 		= _this.attr('check-sn') ? _this.attr('check-sn') : '';//資料序號
	var datatype 		= _this.attr('check-datatype') ? _this.attr('check-datatype') : '';//資料第二種類
	
	var Min 	 		= _this.attr('check-min') ? _this.attr('check-min') : '';//資料最小值
	var Max 	 		= _this.attr('check-max') ? _this.attr('check-max') : '';//資料最大值
	
	var name 	 		= _this.attr('check-name') ? _this.attr('check-name') : '';//資料名稱
	
	var connectfield 	= _this.attr('check-connectfield') ? _this.attr('check-connectfield') : '';//相互作用欄位
	
	if( datatype == 'datestart' && connectfield != '' ){
		
		var datatime	= $('#'+connectfield).val();
		var name2		= checkin($('#'+connectfield).attr('check-name'));
		if( data_new > datatime && datatime != 0 ){
			
			msg = name+'要小於'+name2;
		}
	}else if( datatype == 'dateend' && connectfield != '' ){
		
		var datatime 	= $('#'+connectfield).val();
		var name2		= checkin($('#'+connectfield).attr('check-name'));
		if( data_new < datatime ){
			
			msg = name+'要大於'+name2;
		}
	}else if( type == 'number' ){
		
		var r_number = /^[\-0-9]+$/;
		if( !r_number.test(data_new) ){
						
			msg = '請輸入數字';
		}else if( parseInt(data_new) < Min && Min != '' ){
			
			msg = '數字不能小於 ( ' + Min+ ' )';
		}else if( parseInt(data_new) > Max && Max != '' ){
			
			msg = '數字不能大於 ( ' + Max + ' )';
		}
	}else if( type == 'checkbox' || type == 'checkboxs' ){
		
		data_new = _this.prop('checked');//資料值
		data_old = data_new ? false : true;//資料值
	}
			
	if( msg != '' ){//錯誤訊息印出
		
		if( type != 'checkbox' ){
			
			_this.val(data_old);
		}
		
		$(".tc_box").BoxWindow({
			_msg: msg,//訊息
			//_focus: _this
		});
	}else if( data_new != data_old ){//不一樣值才執行
		
		if( ( checkin(id) != '' && checkin(field) != '' ) || checkin(type) == 'norepeat' ){//有主鍵和欄位才執行或者判斷不重複
		
			var Form_Data = new FormData();
	
			Form_Data.append('_type', 'Table_Data_Change');
			Form_Data.append('_type1', type);
			Form_Data.append('id', id);
			Form_Data.append('field', field);
			Form_Data.append('data', data_new);
			
			loading('open');
			
			$.ajax({
				url: Exec_Url,
				data: Form_Data,
				type: "POST",
				dataType: 'json',
				mimeType:"multipart/form-data",
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){
					
					loading('close');
					
					if( data.html_msg != '' ){
					
						$(".tc_box").BoxWindow({
							_msg: data.html_msg,
						});
						
						_this.val(data_old);
					}else{
						
						if( type != 'norepeat' ){
							
							_this.attr('check-data', data_new);
						}
					}
					
					if( data.html_eval != '' && data.html_eval != null ){
				
						setTimeout(data.html_eval, 1);
					}
				},
				error:function(xhr, ajaxOptions, thrownError){ 
					
					loading('close');
					
					$(".tc_box").BoxWindow({
						_msg: thrownError,
					});
				}
			});
		}
	}
}
/*
function Datetimepicker( Field, Format ){
	
	if( !checkin( Format ) ){
		
		Format = 'YYYY-MM-DD HH:mm:ss';
	}
	
	$(Field).datetimepicker({
		ignoreReadonly: true,
		format: Format,
		locale: 'zh-tw'
	}).on("dp.hide",function (e) {
								
		Ajax_Chk($(this));
		//$(this).data("DateTimePicker").hide();
		//$(this).data("DateTimePicker").maxDate(e.date);
	}).on("dp.change",function(e) {
		
		$('#Lunar').append().attr("value","OP" );
	});
}
*/

function Datetimepicker( Field, Format , type){
	
	if( !checkin( Format ) ){
		
		Format = 'YYYY-MM-DD HH:mm:ss';
	}
	
	Today=new Date();
	realtime = Today.getFullYear()+'-'+(Today.getMonth()+1)+'-'+Today.getDate();
	
	if($(Field).data("DateTimePicker") !== undefined){
		$(Field).data("DateTimePicker").destroy();
	}
	
	if(type == 'week'){
		
		$(Field).datetimepicker({
			ignoreReadonly: true,
			format: Format,
			locale: 'zh-tw',
			maxDate : realtime,
			calendarWeeks:true,
		}).on("dp.hide",function (e) {
									
		}).on("dp.change",function(e) {
			Ajax_Chk($(this));
		});
	}else{
		
		$(Field).datetimepicker({
			ignoreReadonly: true,
			format: Format,
			maxDate : realtime,
			locale: 'zh-tw',
		}).on("dp.hide",function (e) {
									
		}).on("dp.change",function(e) {
			Ajax_Chk($(this));
		});
	}
}
$(document).ready(function(e) {
	
	$('.fsubmit').click(function() {
		
		var type  = $(this).attr('data-type');
		var field = '#'+$(this).closest('form').attr('id');
		if( CheckInput(field) ){
						
			var Form_Data = '';
			
			Form_Data += $(field).serialize();
			Form_Data += '&_type=' + type;
			Post_JS(Form_Data, Exec_Url);
		}
		
	});
	
	$('.dfsubmit').click(function() {
		
		
		var type  = $(this).attr('data-type');
		var field = '#'+$(this).closest('form').attr('id');
		if( CheckInput(field) ){
						
			var Form_Data = '';
			
			Form_Data += $(field).serialize();
			Form_Data += '&_type=' + type;
			Post_JS(Form_Data, 'web_dealerpost.php');
		}
		
	});
	
	$('.logout').click(function(){
		var Form_Data = '';
		Form_Data += '&_type=mlogout';	
		Post_JS(Form_Data, Exec_Url);	
	});
	
	$('#search_btn').click(function(){
		
		var search_key = $('#search_key').val();
		
		if( checkin(search_key) != ''){
			
			location.href = 'search.php?search='+search_key;	
		}else{
			
			alert('請輸入搜尋條件');	
		}
			
	});
	
	$('#mobile_search_btn').click(function(){
		
		var search_key = $('#mobile_search_key').val();
		if( checkin(search_key) != ''){
			
			location.href = 'search.php?search='+search_key;	
		}else{
			
			alert('請輸入搜尋條件');	
		}
		
	});

	
});


function Chk_FBShare( url, href ){
	
	if( confirm('是否發送到FaceBook?') ){
		
		FBShare(url);
		window.location.href = href;
	}else{
		
		window.location.href = href;	
	}
}

function FBShare( url ){
	
	var url = 'http://www.facebook.com/sharer/sharer.php?u=' + url;
	window.open(url, '_blank');
}

function Url_Open( url, target){
	
	if( checkin(target) == '' ){
		
		target = '_blank';
	}
	
	window.open(url, '_blank');
}

var auth2;
var myVar;
function initClient(){
	
	gapi.load('auth2', function(){

		auth2 = gapi.auth2.init({
			client_id: '973960356855-dldk204r0g4mndi0sk8lif5517e560vg.apps.googleusercontent.com',
		});
		
		auth2.attachClickHandler('signin-button', {}, onSuccess, onFailure);
	});
}

function onSuccess(user){
	
	var profile = user.getBasicProfile();
	
	var Form_Data = 'id=' +profile.getId()+ '&name=' +profile.getName()+ '&email=' +profile.getEmail()+ '&_type=googlelogin';
	
	Post_JS(Form_Data, Exec_Url);
}

function onFailure(error){
	
	console.log(error);
}

function getUrlVal( url, str ){
	
	var strUrl = url;
	var getPara, ParaVal;
	
	if ( strUrl.indexOf("?") != -1 ) {
		
		var getSearch = strUrl.split("?");
		getPara = getSearch[1].split("&");
		
		for( i = 0; i < getPara.length; i++ ) {
			
			ParaVal = getPara[i].split("=");
			
			if( ParaVal[0] == str ){
				
				return ParaVal[1];
			}
		}
	}
}

/*function GetLatLng( str ){
	
	/*<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCKqGleS7rA7weez1aWJ3TKSBK6vcQKyf0&signed_in=true" async defer></script>*/
	        
	/*var address = str;

	var geocoder = new google.maps.Geocoder();
	
	geocoder.geocode({'address': address}, function(results, status) {
		
		if (status === google.maps.GeocoderStatus.OK) {
			
			document.getElementById('taxi_Lat').value = results[0].geometry.location.lat();
			document.getElementById('taxi_Lng').value = results[0].geometry.location.lng();
			document.getElementById('addressTF').innerHTML = '';
		} else {
			
			document.getElementById('taxi_Lat').value = '';
			document.getElementById('taxi_Lng').value = '';
			document.getElementById('addressTF').innerHTML = '找不到這地址的緯度';
		}
	});
}*/

function CREAT_CKEDITOR( Field, Height ){
	
	if( !checkin( Height ) ){
		
		Height = 400;
	}
	
	var editor = CKEDITOR.replace( Field, {
		enterMode 					: CKEDITOR.ENTER_BR,
		height 						: Height,
		toolbar						: 'APPToolbar',
        filebrowserImageUploadUrl 	: 'system/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
		//可上傳圖檔
	});
	
	return editor;
}

//判斷是否為空值
function checkin( val ){
	
	if( val == '' || val == null ){
		
		val = '';
	}
	
	return val;
}

function OEncrypt( data , key ){
	
	if(checkin(key) == '')key = 'null';
	
	var key 	= md5(key);
	var x 		= 0;
	var len 	= data.length;
	var l 		= key.length;
	var char  	= '';
	var str 	= '';

	for(var i = 0 ; i<len ; i++){
		
		if(x==l){
			x=0;
		}
		char += key.substr(i,1);
		x++; 
	}
	
	for(var i = 0 ; i<len ; i++){
		
		str += String.fromCharCode( data.substr(i,1).charCodeAt(0) + ((char.substr(i,1).charCodeAt(0)) % 256 ));
	}
	
	return btoa(str);

}

function ODecrypt( data , key ){
	
	if(checkin(key) == '')key = 'null';
	
	var key 	= md5(key);
	var x 		= 0;
	var data	= atob(data);
	var len 	= data.length;
	var l 		= key.length;
	
	var char  	= '';
	var str 	= '';

	for(var i = 0 ; i<len ; i++){
		
		if(x==l){
			x=0;
		}
		char += key.substr(i,1);
		x++; 
	}
	
	for(var i = 0 ; i<len ; i++){

		if( data.substr(i,1).charCodeAt(0) < char.substr(i,1).charCodeAt(0)){
			str += String.fromCharCode( (data.substr(i,1).charCodeAt(0) + 256) - (char.substr(i,1).charCodeAt(0)) );
		}else{
			str += String.fromCharCode( data.substr(i,1).charCodeAt(0) - char.substr(i,1).charCodeAt(0) );
		}
	}
	
	return str;

}

$(document).on('click', '.goback', function(){
	
	history.go(-1);
});