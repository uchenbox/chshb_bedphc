<?php 
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

require_once("../../include/inc.config.php");
require_once("../../include/inc.check_login.php");

$msg  = '';
$code = $_GET['code'];

if( $code != $_SESSION['system']['downloadcode'] || empty($code) || empty($_SESSION['system']['downloadcode']) ){
	
	$msg = '匯出編碼錯誤';
}
$db = new MySQL();
$db->Where = " WHERE DL_Session = '" .$db->val_check($code). "'";
$db->query_sql(Download_DB, '*');
$_row = $db->query_fetch();

ob_end_clean();

if( empty($_row) ){
	
	$msg = '編碼錯誤';
}else{
	
	$db = new MySQL();
	
	require_once(SYS_PATH.'plugins'.DIRECTORY_SEPARATOR.'PHPExcels'.DIRECTORY_SEPARATOR.'PHPExcel.php');

	$Menu_Data 		= MATCH_MENU_DATA(FUN);//取得目前目錄資料
	
	$Main_Key  		= $Menu_Data['Menu_TableKey'];//資料表主健
	
	$Main_Table 	= $Menu_Data['Menu_TableName'];//目錄使用的資料表
		
	$Main_Key3		= $Menu_Data['Menu_TableKey2'];//擴充資料表主健
	
	$Main_Table3 	= $Menu_Data['Menu_TableName2'];//擴充資料表
	
	$Table_Field_Arr = $db->get_table_info( array("web_reglog", "web_member"), 'Comment' );
	
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
	header("Content-Type:application/vnd.ms-execl");
	header('Content-Disposition:attachment;filename="output.xls"');
	header("Content-Transfer-Encoding:binary");
	
	//------------------------------------------------------------
	//設定印出欄位及標題
	//依照此順序排列
	$_Explode_List = array(
	
		'MI_MachCode' 				=> $Table_Field_Arr['MI_MachCode'],
		'DR_Reason' 				=> $Table_Field_Arr['DR_Reason'],
		'Member_Name' 				=> $Table_Field_Arr['Member_Name'],
		'Member_DLL' 				=> $Table_Field_Arr['Member_DLL'],
		'RL_Sdate' 					=> $Table_Field_Arr['RL_Sdate']
	);
	
	//------------------------------------------------------------
	//格式化標題陣列及主資料欄位陣列
	//自動抓取EXCEL行標支持(A TO ZZ)
	$_CHR_SN 	= 1;
	
	$Title_Arr = $Field_Arr = array();
	
	foreach( $_Explode_List as $field => $Name ) {
			
		$_ACI = EXCEL_SWITCH_TITLE($_CHR_SN);
		
		$Title_Arr[$_ACI] = $Name;	//標題陣列
		$Field_Arr[$_ACI] = $field;	//欄位陣列
		
		$_CHR_SN++;
    }
    //------------------------------------------------------------
	//INIT
    $spreadsheet = new Spreadsheet();
    $worksheet = $spreadsheet->getActiveSheet();
    $worksheet->setTitle('消息匯出');

    foreach( $Title_Arr as $chr => $name ){
        $worksheet->setCellValueByColumnAndRow($chr, 1, '学生成绩表');
    
        // $objActSheet->setCellValue($chr.'1', $name);// 字符串內容
    }

    //------------------------------------------------------------
	//主要資料區
	
	$col 	= 2;	// 第二列開始
	
	$Sheet = ' web_reglog as a';
	$Sheet .= ' LEFT JOIN web_member as b ON a.Member_ID = b.Member_ID';
	$Sheet .= ' LEFT JOIN web_machid as c ON a.MI_MachCode = c.MI_MachCode';

	$db->Where = str_replace('Member_ID', 'a.Member_ID', $_row['DL_DownLoadInfo']);
	$db->Order_By = ' ORDER BY RL_Sdate ASC';
	$db->query_sql($Sheet, '*');
	
	while( $row = $db->query_fetch('','assoc') ){
		
		foreach( $Field_Arr as $_ACI => $Field ) {
			
            $_Header = $_ACI;
            $worksheet->setCellValueByColumnAndRow($_Header, $col, $rows[$Field]);

            // $objActSheet->setCellValueExplicit($_Header.$col, $row[$Field] );// 轉成字串setCellValueExplicit ['Marquee_Title']
			// $objActSheet->getColumnDimension($_Header)->setAutoSize(true);
        }
        $col++;
    }
    ob_end_clean();
    // 下载
    $filename = 'TEST.xlsx';
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
    $writer->save('php://output');
    exit;
}
if( !empty($msg) ){
	
	header("Content-Type:text/html; charset=utf-8");
	echo "<script>alert('" .$msg. "');history.go(-1); </script>";
	exit;
} 
// include('conn.php');

/*

//表头
//设置单元格内容

    $worksheet->setCellValueByColumnAndRow(1, 2, '姓名');
    $worksheet->setCellValueByColumnAndRow(2, 2, '语文');
    $worksheet->setCellValueByColumnAndRow(3, 2, '数学');
    $worksheet->setCellValueByColumnAndRow(4, 2, '外语');
    $worksheet->setCellValueByColumnAndRow(5, 2, '总分');

    //合并单元格
    $worksheet->mergeCells('A1:E1');

    $styleArray = [
        'font' => [
            'bold' => true
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
    ];
    //设置单元格样式
    $worksheet->getStyle('A1')->applyFromArray($styleArray)->getFont()->setSize(28);

    $worksheet->getStyle('A2:E2')->applyFromArray($styleArray)->getFont()->setSize(14);
    //$worksheet->getStyle('A2:D2')->getFont()->setSize(14);


    //读取数据
    $sql = "SELECT * FROM `web_marquee`";
    $stmt = $db->query($sql);
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $len = count($rows);
    $j = 0;
    for ($i=0; $i < $len; $i++) { 
        $j = $i + 3;
        $worksheet->setCellValueByColumnAndRow(1, $j, $rows[$i]['Marquee_Title']);
        $worksheet->setCellValueByColumnAndRow(2, $j, $rows[$i]['Marquee_Link']);
        $worksheet->setCellValueByColumnAndRow(3, $j, $rows[$i]['Marquee_Sort']);
        $worksheet->setCellValueByColumnAndRow(4, $j, $rows[$i]['Marquee_Open']);
        $worksheet->setCellValueByColumnAndRow(5, $j, $rows[$i]['Marquee_Sdate']);
    }


    $styleArrayBody = [
        'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '666666'],
            ],
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
    ];
    $total_rows = $len + 2;
    //添加所有边框/居中
    $worksheet->getStyle('A1:E'.$total_rows)->applyFromArray($styleArrayBody);
*/



