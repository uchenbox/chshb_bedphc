<?php
if( !function_exists('Chk_Login') ) header('Location: ../../index.php');

global $open_states;

$Table_Field_Arr = $db->get_table_info($Main_Table, 'Comment');

$Sheet = $Main_Table;

//查詢欄位名稱設定
// $_Search_Option['Product_ID']['name']		= $Table_Field_Arr['Product_ID'];
$_Search_Option['News_Title']['name']		= $Table_Field_Arr['News_Title'];
$_Search_Option['News_PostDate']['name']	= $Table_Field_Arr['News_PostDate'];
$_Search_Option['News_EndDate']['name']		= $Table_Field_Arr['News_EndDate'];
$_Search_Option['News_Sort']['name']		= $Table_Field_Arr['News_Sort'];
$_Search_Option['News_Open']['name']		= $Table_Field_Arr['News_Open'];
$_Search_Option['News_Hot']['name']			= $Table_Field_Arr['News_Hot'];
//查詢欄位設定種類
// $_Search_Option['Product_ID']['type']		= 'text';
$_Search_Option['News_Title']['type']			= 'text';
$_Search_Option['News_PostDate']['type']		= 'datetime';
$_Search_Option['News_EndDate']['type']			= 'datetime';
$_Search_Option['News_Sort']['type']			= 'datetime';
$_Search_Option['News_Open']['type']			= 'select';
$_Search_Option['News_Hot']['type']				= 'select';
//查詢欄位設定選擇內容
$_Search_Option['News_Open']['select']	= $open_states;
$_Search_Option['News_Hot']['select']	= $open_states;

if( !empty($POST['search_field']) ){
				
	$db->Where = Search_Fun($db->Where, $POST, $_Search_Option);
}

$_Excel_Option	= true;//進階功能->開啟匯出功能

if( $_Excel_Option && $POST['search_type'] == 'excel' ){//清除資料
	
	echo $db->Where;
	return;
}

$db->Search = array("News_Title", "News_PostDate", "News_EndDate", "News_Sort", "News_Open", "News_Hot");//設定可搜尋欄位
$db->query_search($SearchKey);//串接搜尋子句

$Page_Total_Num = $db->query_count($Sheet);//總資料筆數

$Page_Calss  = new Pages($Pages, $Page_Total_Num, $Page_Size);//頁碼程式
$Pages    = $Page_Calss->Pages;//頁碼
$StartNum = $Page_Calss->StartNum;//從第幾筆開始撈
	
$Title_Array['Ordersn']			= "序號";
// $Title_Array['Product_ID']		= $Table_Field_Arr['Product_ID'];
$Title_Array['News_Title']		= $Table_Field_Arr['News_Title'];
$Title_Array['News_PostDate']	= $Table_Field_Arr['News_PostDate'];
$Title_Array['News_EndDate']	= $Table_Field_Arr['News_EndDate'];
// $Title_Array['Product_Mcp']		= $Table_Field_Arr['Product_Mcp'];
$Title_Array['News_Sort']		= $Table_Field_Arr['News_Sort'];
$Title_Array['News_Open']		= $Table_Field_Arr['News_Open'];
$Title_Array['News_Hot']		= $Table_Field_Arr['News_Hot'];
$Title_Array['News_Click']		= $Table_Field_Arr['News_Click'];

$sn = ( $StartNum + 1 );

if( !empty($Order_By) ){
	
	$db->Order_By = $Order_By;
}else if( !empty($Now_List['Menu_OrderBy']) ){
	
	$db->Order_By = $Now_List['Menu_OrderBy'];
}else{
	
	$db->Order_By = " ORDER BY News_Sort DESC, News_Sdate DESC";
}

$db->query_sql($Sheet, "*", $StartNum, $Page_Size);
while( $row = $db->query_fetch() ){
	
	$Value_Array['Ordersn'][$sn]	 	= $sn;
	$Value_Array[$Main_Key][$sn]	 	= $row[$Main_Key];
	// $Value_Array['Product_ID'][$sn]		= $row['Product_ID'];
	$Value_Array['News_Title'][$sn]		= $row['News_Title'];
	$Value_Array['News_PostDate'][$sn]	= $row['News_PostDate'];
	$Value_Array['News_EndDate'][$sn]	= $row['News_EndDate'];
	
	// $Value_Array['Product_Mcp'][$sn]	= $row['Product_Mcp'];
	// if( !empty($row['Product_Mcp']) ){
			
		// $Value_Array['Product_Mcp_bUrl'][$sn] = $Pathweb.$row['Product_Mcp'];
		// $Value_Array['Product_Mcp_sUrl'][$sn] = $Pathweb.'sm_'.$row['Product_Mcp'].'?'.time();
	// }

	$Value_Array['News_Sort'][$sn]	= $row['News_Sort'];
	$Value_Array['News_Open'][$sn]	= $row['News_Open'];
	$Value_Array['News_Hot'][$sn]	= $row['News_Hot'];
	$Value_Array['News_Click'][$sn]	= $row['News_Click'];
	
	//呈現的種類
	// $Vtype_Array['Product_Mcp'][$sn]	= 'uploadimg';
	$Vtype_Array['News_Sort'][$sn]	= 'number';
	$Vtype_Array['News_Open'][$sn]	= 'checkbox';
	$Vtype_Array['News_Hot'][$sn]	= 'checkbox';
	$Vtype_Array['News_Click'][$sn]	= 'number';
	
	$sn++;
}

$table_option_arr['News_Sort']['TO_OutEdit']	= 1;
$table_option_arr['News_Open']['TO_OutEdit']	= 1;
$table_option_arr['News_Hot']['TO_OutEdit']	= 1;

$Order_Array = array("News_Title" => "", "News_PostDate" => "", "News_EndDate" => "", "News_Sort" => "", "News_Open" => "", "News_Hot" => "", "News_Click" => "");//允許排序項目	

//第一個值設定對應KEY,第二個值設定對應名稱
$_FT = new Fixed_Table( $Value_Array[$Main_Key], $Value_Array['Ordersn'] );
$_FT->Input_TitleArray( $Title_Array );//設定顯示欄位
$_FT->Input_ValueArray( $Value_Array );//設定顯示欄位裡面的值
$_FT->Input_VtypeArray( $Vtype_Array );//設定顯示欄位呈現方式
$_FT->Input_OrderArray( $Order_Array, $Sort, $Field );//設定可排序的欄位, 升序或降序, 要排序欄位名稱
$_FT->Pages_Data 		= $Page_Calss->Pages_Data;//頁碼資料
$_FT->Pages_Html 		= $Page_Calss->Pages_Html;//頁碼頁面
$_FT->SearchKey  		= $SearchKey;//搜尋字串
$_FT->Operating	 		= array("View" => $Now_List['Menu_View'], "Edit" => $Now_List['Menu_Edt'], "Delete" =>  $Now_List['Menu_Del']);//設定操作類型
$_FT->Path		 		= SYS_PATH.$Now_List['Menu_Path'].DIRECTORY_SEPARATOR;//檔案路徑
$_FT->TableHtml	 		= $Now_List['Menu_Exec_Name'].'.table.'.$Now_List['Exec_Sub_Name'];
$_FT->table_option_arr	= $table_option_arr;//資料表的設定陣列

$_html = $_FT->CreatTable();//創建表格

if( !empty($POST) ){
	
	echo $_html;	
}else{
?>

<script type="text/javascript" src="assets/js/sys-table.js"></script>
<script type="text/javascript">

var Exec_Url = '<?=$Now_List['Menu_Path']?>/<?=$Now_List['Menu_Exec_Name']?>.post.php?fun=' + getUrlVal(location.search, 'fun');


var allow_file = new Array('xls', 'xlsx', 'csv');

$(document).ready(function(e) {
    
	$('.import-button').click(function() {
		
		var file = $('#fileupload');
					
		if( checkin(file.val()) == '' ){
			
			$(".tc_box").BoxWindow({
				_msg: '請上傳EXCEL檔'
			});
		}else{
			
			var file_data = file[0].files; // for multiple files
			
			var ext = file_data[0].name.split('.').pop().toLowerCase();
		
			if( $.inArray(ext, allow_file) == -1 ){
				
				alert(file_data[0].name + ' ( 不允許上傳檔案格式 )');
				return false;
			}else if( file_data[0].size > 4098000 ){//2MB
				
				alert('檔案上傳限制 4MB');
				return false;
			}
			
			var Form_Data = new FormData();
			
			Form_Data.append(file.attr('name'), file_data[0]);
			
			Form_Data.append('_type', 'UploadExcel');
			
			Ajax_Table(Form_Data, Exec_Url);
		}
	});
});
</script>

<div class="table-header">
	<span><?=$Now_List['Menu_Name']?></span>
    	
<?php if( !empty($_Search_Option) ){ ?>
    <span class="extra-fun">
    	<i class="fa fa-tasks"></i>進階功能
    </span>
<?php } ?>
</div>

<?php require_once(SYS_PATH.'php_sys/extra_div.php')?>
<div class="extra-div col-sm-12" style="margin: 6px 0px 0px 13px;">
	
    <div class="col-sm-3">
       <input type="file" id="fileupload" name="fileupload" class="input-file" /> 
       
    </div>
   
   	<button type="button" class="btn btn-purple btn-sm import-button">匯入</button> 
	<script type="text/javascript">Upload_File_Class('#fileupload');</script> 
</div>
<div id="table_content"><?=$_html?></div>

<div id="table_content_edit" class="display_none"></div>

<div id="table_content_view" class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
    <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
    <div class="contents"></div>
</div>
<?php 
}?>