<?php if( !function_exists('Chk_Login') ) header('Location: ../../index.php'); ?>

<div class="tab_container">

    <form id="form_edit_save" class="form-horizontal">

    <?php foreach( $_html_ as $key => $val ){ 
            
        $table_sn = 'tabsn'.$key;
    ?>
        <div class="Table_border <?=$table_sn?>">
        
            <input type="hidden" id="<?=$Main_Key?>" name="<?=$Main_Key?>[]" value="<?=$val[$Main_Key]?>">

			<?php
			
			if( $Settype == 1 ){ 
			
				//-----------------------------------------//門診資訊
				$Arr_Name = 'Setting_OutpatientInfo';
		 
				echo $_TF->html_textedit($table_info[$Arr_Name]['Comment'], $Arr_Name, $val);
				//-----------------------------------------//
				
			}else if( $Settype == 2 ){
				
				//-----------------------------------------//公衛指標概覽
				$Arr_Name = 'Setting_OverviewofPHI';
		 
				echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '請輸入'.$table_info[$Arr_Name]['Comment'], 1);
				//-----------------------------------------//
			
			}else if( $Settype == 3 ){
				
				//-----------------------------------------//民意信箱說明
				$Arr_Name = 'Setting_MailBoxIntro';
		 
				echo $_TF->html_textarea($table_info[$Arr_Name]['Comment'], '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '');
				
			}else if( $Settype == 4 ){
				
				//-----------------------------------------//志工服務
				$Arr_Name = 'Setting_Volunteer';
		 
				echo $_TF->html_textedit($table_info[$Arr_Name]['Comment'], $Arr_Name, $val);
				
			}else if( $Settype == 5 ){
				
				//-----------------------------------------//社區資源
				$Arr_Name = 'Setting_CommunityResource';
		 
				echo $_TF->html_textedit($table_info[$Arr_Name]['Comment'], $Arr_Name, $val);
				
			}else if( $Settype == 6 ){
				
				//-----------------------------------------//禁止轉錄聲明
				$Arr_Name = 'Setting_PTS';
		 
				echo $_TF->html_textedit($table_info[$Arr_Name]['Comment'], $Arr_Name, $val);
				
			}else if( $Settype == 7 ){
				
				//-----------------------------------------//英文版頁面About Us
				$Arr_Name = 'Setting_AboutUs';
		 
				echo $_TF->html_textedit($table_info[$Arr_Name]['Comment'], $Arr_Name, $val);
				//-----------------------------------------//英文版頁面LOGO
				$Arr_Name = 'Setting_Title_EN';
				
				echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '請輸入'.$table_info[$Arr_Name]['Comment'], 1);
				//-----------------------------------------//英文版頁面地址
				$Arr_Name = 'Setting_Addr_EN';
		 
				echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '請輸入'.$table_info[$Arr_Name]['Comment'], 1);
				
			}else if( $Settype == 8 ){
				
				//-----------------------------------------//首頁捷徑1_名稱
				$Arr_Name = 'Setting_SC1_Name';
		 
				echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '限縮6個中文字內', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '請輸入'.$table_info[$Arr_Name]['Comment'], 1);
				//-----------------------------------------//首頁捷徑1_連結
				$Arr_Name = 'Setting_SC1_Link';
		 
				echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '請輸入'.$table_info[$Arr_Name]['Comment'], 1);
				//-----------------------------------------//首頁捷徑2_名稱
				$Arr_Name = 'Setting_SC2_Name';
		 
				echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '限縮6個中文字內', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '請輸入'.$table_info[$Arr_Name]['Comment'], 1);
				//-----------------------------------------//首頁捷徑2_連結
				$Arr_Name = 'Setting_SC2_Link';
		 
				echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '請輸入'.$table_info[$Arr_Name]['Comment'], 1);
				
			}else if( $Settype == 9 ){
				
				//-----------------------------------------//隱私權保護及資訊安全宣告
				$Arr_Name = 'Setting_Privacy';
		 
				echo $_TF->html_textedit($table_info[$Arr_Name]['Comment'], $Arr_Name, $val);
				
			}else{
				
				//-----------------------------------------//
				$Arr_Name = 'Setting_Forms';
		 
				echo $_TF->html_textarea($table_info[$Arr_Name]['Comment'], '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '');
				//-----------------------------------------//
				$Arr_Name = 'Ecpay_HashKey';
		 
				echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '請輸入'.$table_info[$Arr_Name]['Comment'], 1);
				//-----------------------------------------//
				$Arr_Name = 'Ecpay_HashIV';
		 
				echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '請輸入'.$table_info[$Arr_Name]['Comment'], 1);
				//-----------------------------------------//
				
			}

			?> 
        </div>
    <?php } ?>
    
        <div class="clear_both form-actions">
            <button id="saveb" class="btn btn-info" type="button" <?=(Menu_Use($Now_List, 'edit')||Menu_Use($Now_List, 'add'))?'onclick="form_edit_save()"':'disabled="disabled"'?>>
                <i class="ace-icon fa fa-check bigger-110"></i>儲存
            </button>&nbsp;&nbsp;&nbsp; 
                    
            <button id="rsetb" class="btn btn" type="reset">
                <i class="ace-icon fa fa-check bigger-110"></i>重設
            </button>&nbsp;&nbsp;&nbsp; 
            
        
        </div>
    </form>
</div>