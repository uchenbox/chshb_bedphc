<?php if( !function_exists('Chk_Login') ) header('Location: ../../index.php'); ?>

<div class="abgne_tab">
    <ul class="tabs">
        <li><a href="javascript:void(0)" for-id="tab1">基本資料</a></li>
    </ul>
    <div class="tab_container">

		<div id="tab1" class="tab_content">
            <form id="form_edit_save" class="form-horizontal">
            <?php foreach( $_html_ as $key => $val ){ 
					
					$table_sn = 'tabsn'.$key;
			?>
                <div class="Table_border <?=$table_sn?>">
                
                    <input type="hidden" id="<?=$Main_Key?>" name="<?=$Main_Key?>[]" value="<?=$val[$Main_Key]?>">
                
                    <?php 
					//-----------------------------------------//
					$Arr_Name = 'Member_Acc';
					
					echo $_TF->html_unique($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '請輸入'.$table_info[$Arr_Name]['Comment'], 1);
					//-----------------------------------------//
					$Arr_Name = 'Member_Pwd';
					
					echo $_TF->html_pwd($table_info[$Arr_Name]['Comment'], '', '(輸入新密碼, 儲存後直接更新密碼)', $table_info[$Arr_Name]['Field_Length'], $Arr_Name);
					//-----------------------------------------//
					$Arr_Name = 'Member_Email';
					
					echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					//-----------------------------------------//
					$Arr_Name = 'Member_Company';
					
					echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					//-----------------------------------------//
					$Arr_Name = 'Member_Uniform';
					
					echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					//-----------------------------------------//
					$Arr_Name = 'Member_Name';
					
					echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					//-----------------------------------------//
					$Arr_Name = 'Member_Sex';
					
					echo $_TF->html_radio($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val, array(1 => '先生', 2 => '小姐'));
					//-----------------------------------------//
					$Arr_Name = 'Member_Birthday';
					
					echo $_TF->html_datetime($table_info[$Arr_Name]['Comment'], '', '', $Arr_Name, $val, '', 1, $key, 'datetime', '', 'YYYY-MM-DD');
					//-----------------------------------------//
					$Arr_Name = 'Member_Tel';
					
					echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					//-----------------------------------------//
					$Arr_Name = 'Member_Fax';
					
					echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					//-----------------------------------------//
					$Arr_Name = 'Member_Mobile';
					
					echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					//-----------------------------------------//
					$Arr_Name = 'Member_City';
					
					$selcity  	= '#'.$Arr_Name;
					$selcounty	= '#Member_Area';
					$selzipcode	= '#Member_Zipcode';
					echo $_TF->html_city($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val, '', $table_sn, $selcity, $selcounty, $selzipcode);
					//-----------------------------------------//
					$Arr_Name = 'Member_Area';
					
					echo $_TF->html_county($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val, '');
					//-----------------------------------------//
					$Arr_Name = 'Member_Address';
					
					echo $_TF->html_address($table_info[$Arr_Name]['Comment'], '地址', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', true, '區域碼', $table_info['Member_Zipcode']['Field_Length'], 'Member_Zipcode', '');
					//-----------------------------------------//
					$Arr_Name = 'Member_Intro';
					
					echo $_TF->html_textarea($table_info[$Arr_Name]['Comment'], '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '');
					//-----------------------------------------//
					$Arr_Name = 'Member_Open';
					
					echo $_TF->html_checkbox($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val, '', 1);
					//-----------------------------------------//
					$Arr_Name = 'Member_Sdate';
					
					echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', '', $Arr_Name, $val, '', 0);
					//-----------------------------------------//
					$Arr_Name = 'Member_Edate';
					
					echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', '', $Arr_Name, $val, '', 0);
					//-----------------------------------------//
					$Arr_Name = 'Member_LastLogin';
					
					echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', '', $Arr_Name, $val, '', 0);
					?>     
                </div>
            <?php } ?>
            	
                
                <div class="clear_both form-actions">
                    <button id="saveb" class="btn btn-info" type="button" onclick="form_edit_save()">
                        <i class="ace-icon fa fa-check bigger-110"></i>儲存
                    </button>&nbsp;&nbsp;&nbsp; 
                            
                    <button id="rsetb" class="btn btn" type="reset">
                        <i class="ace-icon fa fa-check bigger-110"></i>重設
                    </button>
                </div>
            </form>
		</div>
    </div>
</div>

<script type="text/javascript">$('.imgajax').colorbox({width:"70%", height:"100%", rel:'imgajax'});</script>