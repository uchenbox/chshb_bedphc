<?php
//資料表設定用
$States_Type = array(
	//key最多30碼
	'open_states' 	=> '啟用/停用狀態 ( 預設 )',
	'sex_states' 	=> '性別狀態 ( 預設 )'
);

$open_states 	= array(0 => '停用', 1=> '啟用');//系統預設

$secure_states 	= array('' => '不加密', 'SSL' => 'SSL', 'TLS' => 'TLS');//加密方式, 系統預設

$menu_lv_states = array('1' => '一', '2' => '二', '3' => '三', '4' => '四', '5' => '五', '6' => '六', '7' => '七');//系統預設
	
$sex_states 	= array('1' => '先生', '2' => '小姐');//系統預設

$order_states	= array(0 => '未受理', 1=> '已受理', 2=> '已出貨', 3=> '已取消');//系統預設

$favor_states	= array(1 => '折', 2=> '元');//系統預設

$pickup_states	= array(0 => '宅配-不限時段', 1=> '宅配-上午時段', 2=> '宅配-下午時段', 3=> '宅配-晚上時段');//系統預設
?>