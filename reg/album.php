<?php require_once("include/web.config.php");

$PageData['p'] = $_GET['p'];

$_Result 		= $CM->GET_ALBUM_LIST( $Input, $PageData );

$Pages_Data 	= $_Result['PageData'];
$_html			= $_Result['Data'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];

$_Title = "活動花絮";
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="about.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">關於本所</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="album.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">活動花絮</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('about_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">活動花絮</h2>
					<div class="mainContent">
						<ul class="photolist">
							<?php foreach( $_html as $key => $val ){?>
								<li class="photolist__item">
									<a class="photolist__item__link" title="<?=$val['Album_Title']?>" href="albumin.php?c=<?=OEncrypt('albumin_id='.$key , 'albumin')?>">
										<div class="img" style="background-image: url('<?=Album_Url.$key.'/'.$val['Album_Mcp']?>');"></div>
										<div class="tit"><?=$val['Album_Title']?></div>
										<div class="date"><?=$val['Album_Postdate']?></div>
									</a>
								</li>
							<?php }?>
						</ul>
					</div>
					
					<?php require('page.php')?>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>