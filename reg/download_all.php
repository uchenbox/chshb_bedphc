<?php require_once("include/web.config.php");

$Input = GDC( $_GET['c'] , 'dlcat' );

$_DL		= $Input['dl_cat'];


$Input1 = GDC( $_GET['c'] , 'dl' );

$search = $Input1['dl_searchwords'];
$mode 	= $Input1['mode'];

if( $mode=='search' ){
	
	$_Result 		= $CM->GET_DOWNLOAD_SEARCH( $search );

	$_html			= $_Result['Data'];
// print_r($_Result);
}else{
	
	$_Result 		= $CM->GET_DOWNLOAD_LIST( $_DL );

	$_html			= $_Result['Data'];

}

// print_r($_Result);
$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];

$_Title = "檔案下載";
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="opc_info.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">便民服務</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="" rel="nofollow"  itemprop="url">
								<span itemprop="title">檔案下載</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('service_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">檔案下載</h2>
					<div class="text">為配合行政院推動政府文件標準格式(ODF-CNS15251)，本網站提供民眾下載檔案以開放性PDF及ODF檔案格式為主 ，ODF檔案可於網路上免費下載自由軟體開啟編輯 ，如 LibreOffice(https://zh-tw.libreoffice.org) ，亦可用微軟Office2007(含)以上版本開啟。
					</div>
					<form id="forms" onSubmit="return false;">
						<div class="mainSearch">
							<label for="searchdl">請輸入關鍵字查詢檔案下載</label>
							<input type="text" class="input__style01" id="searchdl" title="請輸入關鍵字查詢檔案下載" placeholder="請輸入關鍵字" value="<?=$search?>">
							<button class="searchdlbtn">查詢</button>
						</div>
					</form>
					<?php if( !empty($_DL) ){?>
					
						<h3 class="photoTitle"><?=$_html_DL[$_DL]['DownloadC_Name']?></h3>
						<div class="mainContent nopd">
							<table class="mainTable">
								<tr>
									<th class="tb1"><span>日期</span></th>
									<!--<th class="tb1"><span>類別</span></th>-->
									<th class="tb4 "><span>標題</span></th>
									<th class="tb1 dl"><span>下載</span></th>
								</tr>
								<?php foreach( $_html as $key => $val ){?>
									<tr>
										<td class="tb1" data-title="日期"><span><?=$val['Download_Postdate']?></span></td>
										<!--<td class="tb1" data-title="類別"><span>檢查表-3</span></td>-->
										<td class="tb4 left" data-title="標題"><span><?=$val['Download_Title']?></span></td>
										<td class="tb1 dl" data-title="下載">
											<div class="downloadlist">
												<?php if( !empty($val['Download_FilePDF']) ){?>
													<a class="pdf" href="include/downloadfile.php?code=<?=$val['PDF']?>" title="<?=$val['Download_Title']?>PDF檔下載(另開新視窗)"><?=$val['Download_Title']?>PDF檔下載(另開新視窗)</a>
												<?php }?>
												<?php if( !empty($val['Download_FileDOC']) ){?>
													<a class="doc" href="include/downloadfile.php?code=<?=$val['DOC']?>" title="<?=$val['Download_Title']?>DOC檔下載(另開新視窗)"><?=$val['Download_Title']?>DOC檔下載(另開新視窗)</a>
												<?php }?>
												<?php if( !empty($val['Download_FileODT']) ){?>
													<a class="odt" href="include/downloadfile.php?code=<?=$val['ODT']?>" title="<?=$val['Download_Title']?>ODT檔下載(另開新視窗)"><?=$val['Download_Title']?>ODT檔下載(另開新視窗)</a>
												<?php }?>
												<?php if( !empty($val['Download_FileJPG']) ){?>
													<a class="jpg" href="include/downloadfile.php?code=<?=$val['JPG']?>" title="<?=$val['Download_Title']?>JPG檔下載(另開新視窗)"><?=$val['Download_Title']?>JPG檔下載(另開新視窗)</a>
												<?php }?>
											</div>
										</td>
									</tr>
								<?php }?>
							</table>
						</div>
					<?php }else if( empty($_DL) ){?>
					
						<!--<h3 class="photoTitle"><?=$_html_DL[$_DL]['DownloadC_Name']?></h3>-->
						<div class="mainContent nopd">
							<table class="mainTable">
								<tr>
									<th class="tb1"><span>日期</span></th>
									<th class="tb1"><span>類別</span></th>
									<th class="tb4 "><span>標題</span></th>
									<th class="tb1 dl"><span>下載</span></th>
								</tr>
								<?php foreach( $_html as $key => $val ){?>
									<tr>
										<td class="tb1" data-title="日期"><span><?=$val['Download_Postdate']?></span></td>
										<td class="tb1" data-title="類別"><span><?=$val['DownloadC_Name']?></span></td>
										<td class="tb4 left" data-title="標題"><span><?=$val['Download_Title']?></span></td>
										<td class="tb1 dl" data-title="下載">
											<div class="downloadlist">
												<?php if( !empty($val['Download_FilePDF']) ){?>
													<a class="pdf" href="include/downloadfile.php?code=<?=$val['PDF']?>" title="<?=$val['Download_Title']?>PDF檔下載(另開新視窗)"><?=$val['Download_Title']?>PDF檔下載(另開新視窗)</a>
												<?php }?>
												<?php if( !empty($val['Download_FileDOC']) ){?>
													<a class="doc" href="include/downloadfile.php?code=<?=$val['DOC']?>" title="<?=$val['Download_Title']?>DOC檔下載(另開新視窗)"><?=$val['Download_Title']?>DOC檔下載(另開新視窗)</a>
												<?php }?>
												<?php if( !empty($val['Download_FileODT']) ){?>
													<a class="odt" href="include/downloadfile.php?code=<?=$val['ODT']?>" title="<?=$val['Download_Title']?>ODT檔下載(另開新視窗)"><?=$val['Download_Title']?>ODT檔下載(另開新視窗)</a>
												<?php }?>
												<?php if( !empty($val['Download_FileJPG']) ){?>
													<a class="jpg" href="include/downloadfile.php?code=<?=$val['JPG']?>" title="<?=$val['Download_Title']?>JPG檔下載(另開新視窗)"><?=$val['Download_Title']?>JPG檔下載(另開新視窗)</a>
												<?php }?>
											</div>
										</td>
									</tr>
								<?php }?>
							</table>
						</div>
					<?php }?>
					
						
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>
<script>
$(document).ready(function(e) {
	
	$('.searchdlbtn').click(function() {
		
		var searchdl = $('#searchdl').val();
		var type  = $(this).attr('data-type');
		var field = '#'+$(this).closest('form').attr('id');
		if( CheckInput(field) ){
			
			var Form_Data = '';
			Form_Data += '_href=download_all';
			Form_Data += '&_searchkey='+searchdl;
			Form_Data += '&_mode=dl';
			Form_Data += '&_modekey=dl_searchwords';
			Form_Data += '&_type=dl_search';
			
			Post_JS(Form_Data, 'web_post.php');
		}
	});
});
</script>