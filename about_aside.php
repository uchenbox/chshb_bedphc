<aside class="aside">
	<p class="aside__tit">關於本所</p>
	<ul class="mnav">
		<li class="mnav__item <?=PHP_NAME=='about'?'current':''?>" title="本所簡介"><a href="about.php">本所簡介</a></li>
		<li class="mnav__item <?=PHP_NAME=='business'?'current':''?>" title="業務職掌"><a href="business.php">業務職掌</a></li>
		<li class="mnav__item <?=PHP_NAME=='glory'?'current':''?>" title="榮耀軌跡"><a href="glory.php">榮耀軌跡</a></li>
		<li class="mnav__item <?=PHP_NAME=='headman'?'current':''?>" title="歷任首長"><a href="headman.php">歷任首長</a></li>
		<li class="mnav__item <?=PHP_NAME=='traffic'?'current':''?>" title="交通資訊"><a href="traffic.php">交通資訊</a></li>
		<li class="mnav__item <?=PHP_NAME=='album'||PHP_NAME=='albumin'?'current':''?>" title="活動花絮"><a href="album.php">活動花絮</a></li>
		<li class="mnav__item <?=PHP_NAME=='video'?'current':''?>" title="另開新視窗前往<?=$_setting_['WO_Title']?>公衛指標概覽"><a href="<?=$_setting2_['Setting_OverviewofPHI']?>.php" target="_blank">公衛指標概覽</a></li>
		<li class="mnav__item <?=PHP_NAME=='contact'?'current':''?>" title="民意信箱"><a href="contact.php">民意信箱</a></li>
	</ul>
</aside>