<div class="page">
  <ul>
    <li><a href="<?=$Pages_Data['Page_Url'].$Pages_Data['Page_Pre']?>" title="移至第一頁"><<</a></li>
    <li><a href="<?=$Pages_Data['Page_Url'].$Pages_Data['Page_Pre']?>" title="上一頁"><</a></li>
     <!--<li  class="current"> <a href="" title="第1頁">1</a></li>
     <li  class=""> <a href="" title="第2頁">2</a></li>
     <li  class=""> <a href="" title="第3頁">3</a></li>-->
    <?php for( $i = $Pages_Data['Pstart']; $i <= $Pages_Data['Pend']; $i++ ){ ?>
          <li  class="<?=$i==$Pages_Data['Pages']?"current":""?>">
              <a href="<?=$Pages_Data['Page_Url'].$i?>" title="第<?=$i?>頁"><?=$i?></a>
          </li>
      <?php } ?>
    <li><a href="<?=$Pages_Data['Page_Url'].$Pages_Data['Page_Next']?>" title="下一頁">></a></li>
     <li><a href="<?=$Pages_Data['Page_Url'].$Pages_Data['Page_Next']?>" title="移至最後一頁
      ">>></a></li>
  </ul>

  <div class="page__total">資料筆數 【<?=$Pages_Data['Page_Total_Num']?>】頁數【<?=$Pages_Data['Pages']?>/<?=$Pages_Data['Pend']?>】</div>
</div>

<link rel="stylesheet" type="text/css" href="stylesheets/page.css?v=<?=$version?>" />