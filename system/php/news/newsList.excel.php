<?php 
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

require_once("../../include/inc.config.php");
require_once("../../include/inc.check_login.php");

$msg  = '';
$code = $_GET['code'];

if( $code != $_SESSION['system']['downloadcode'] || empty($code) || empty($_SESSION['system']['downloadcode']) ){
	
	$msg = '匯出編碼錯誤';
}
$db = new MySQL();
$db->Where = " WHERE DL_Session = '" .$db->val_check($code). "'";
$db->query_sql(Download_DB, '*');
$_row = $db->query_fetch();

ob_end_clean();

if( empty($_row) ){
	
	$msg = '編碼錯誤';
}else{
	
	$db = new MySQL();
	
	$Menu_Data 		= MATCH_MENU_DATA(FUN);//取得目前目錄資料
	
	$Main_Key  		= $Menu_Data['Menu_TableKey'];//資料表主健
	
	$Main_Table 	= $Menu_Data['Menu_TableName'];//目錄使用的資料表
		
	$Main_Key3		= $Menu_Data['Menu_TableKey2'];//擴充資料表主健
	
	$Main_Table3 	= $Menu_Data['Menu_TableName2'];//擴充資料表
	
	$Table_Field_Arr = $db->get_table_info( array($Main_Table), 'Comment' );
	
	/**/
	// header("Pragma: public");
	// header("Expires: 0");
	// header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
	// header("Content-Type:application/vnd.ms-execl");
	// header('Content-Disposition:attachment;filename="output.xls"');
	// header("Content-Transfer-Encoding:binary");
	
	//------------------------------------------------------------
	//設定印出欄位及標題
	//依照此順序排列
	$_Explode_List = array(

		'News_Title' 			=> $Table_Field_Arr['News_Title'],
		'News_PostDate' 		=> $Table_Field_Arr['News_PostDate'],
		'News_EndDate' 			=> $Table_Field_Arr['News_EndDate'],
		'News_Content' 			=> $Table_Field_Arr['News_Content']
	);
	
	//------------------------------------------------------------
	//格式化標題陣列及主資料欄位陣列
	//自動抓取EXCEL行標支持(A TO ZZ)
	$_CHR_SN 	= 1;
	
	$Title_Arr = $Field_Arr = array();
	
	foreach( $_Explode_List as $field => $Name ) {
			
		$_ACI = EXCEL_SWITCH_TITLE($_CHR_SN);
		
		$Title_Arr[$_ACI] = $Name;	//標題陣列
		$Field_Arr[$_ACI] = $field;	//欄位陣列
		
		$_CHR_SN++;
	}
	
    //------------------------------------------------------------
	//INIT
    $spreadsheet = new Spreadsheet();
    $worksheet = $spreadsheet->getActiveSheet();
    $worksheet->setTitle('消息匯出');

    foreach( $Title_Arr as $chr => $name ){
        $worksheet->setCellValue($chr.'1', $name);
	
        // $objActSheet->setCellValue($chr.'1', $name);// 字符串內容
    }
	
    //------------------------------------------------------------
	//主要資料區
	
	$col 	= 2;	// 第二列開始
	// $type 	= PHPExcel_Cell_DataType::TYPE_STRING;
	//$Sheet = ' web_reglog as a';
	//$Sheet .= ' LEFT JOIN web_member as b ON a.Member_ID = b.Member_ID';
	//$Sheet .= ' LEFT JOIN web_machid as c ON a.MI_MachCode = c.MI_MachCode';

	$db->Where = str_replace('News_ID', 'News_ID', $_row['DL_DownLoadInfo']);
	$db->Order_By = ' ORDER BY News_Sort ASC';
	$db->query_sql($Main_Table, 'News_Title, News_PostDate, News_EndDate, News_Content');
	
	while( $row = $db->query_fetch('','assoc') ){
		
		foreach( $Field_Arr as $_ACI => $Field ) {
			
			$worksheet->getStyle($_Header.$col)->getFont()->setName('微軟正黑體');  
			$_Header = $_ACI;
            // $worksheet->setCellValue($_Header.$col, $row[$Field]);
            $worksheet->setCellValueExplicit($_Header.$col, $row[$Field], PHPExcel_Cell_DataType::TYPE_STRING);
			// $worksheet->getStyle($_Header.$col)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
        }
        $col++;
	}
	
    ob_end_clean();
    // 下载
    $filename = '最新消息'.date("YmdHis").'.xlsx';
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

	
    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
    $writer->save('php://output');
    exit;
}
if( !empty($msg) ){
	
	header("Content-Type:text/html; charset=utf-8");
	echo "<script>alert('" .$msg. "');history.go(-1); </script>";
	exit;
} 
