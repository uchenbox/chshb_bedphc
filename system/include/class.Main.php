<?php

class Main{
	
	function set_head(){
		
		include_once("php_sys/head.php");
	}
	function set_head_chpsd(){
		
		include_once("php_sys/head-chpsd.php");
	}
	
	function set_header(){
		
		global $DB_DataBase, $Admin_data, $Sys_Tables_Arr;
	
		include_once("php_sys/header.php");
	}
	
	function set_footer(){
		
		include_once("php_sys/footer.php");
	}
	
	function set_box(){
		
		include_once("php_sys/extra_box.php");
	}
}
?>