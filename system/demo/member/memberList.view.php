<?php if( !function_exists('Chk_Login') ) header('Location: ../index.php'); ?>

<div class="form-horizontal">
<?php 
foreach( $_html_ as $key => $val ){
	
	echo $_TF->html_view_content(array('comment' => $table_info['Member_Acc']['Comment'], 'data' => $val['Member_Acc']));

	echo $_TF->html_view_content(array('comment' => $table_info['Member_Email']['Comment'], 'data' => $val['Member_Email']));

	echo $_TF->html_view_content(array('comment' => $table_info['Member_Name']['Comment'], 'data' => $val['Member_Name']));
	
	echo $_TF->html_view_content(array('comment' => $table_info['Member_Sex']['Comment'], 'data' => $val['Member_Sex']?'先生':'小姐'));
	
	echo $_TF->html_view_content(array('comment' => $table_info['Member_Tel']['Comment'], 'data' => $val['Member_Tel']));

	echo $_TF->html_view_content(array('comment' => $table_info['Member_Mobile']['Comment'], 'data' => $val['Member_Mobile']));
	
	echo $_TF->html_view_content(array('comment' => $table_info['Member_Address']['Comment'], 'data' => $val['Member_City'].$val['Member_Area'].$val['Member_Address']));

	echo $_TF->html_view_content(array('comment' => $table_info['Member_Open']['Comment'], 'data' => $val['Member_Open']), 'checkbox');
} 
?>
</div>