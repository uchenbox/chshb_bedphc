<?php 
require_once("../include/web.config.php");


$Input = GDC($_GET['c'] , 'custom_contact');
$v = $Input['v'];

$db->Where = " WHERE Contact_Verify = '".$v."'";
$db->query_sql( $contact_db, '*');
$_html = $db->query_fetch();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<style>
		body{
			font-family: '微軟正黑體';
		}
		.mailbody{
			border: 3px solid #999;
			padding: 20px 30px;
			max-width: 700px;
			box-sizing: border-box;
		}
		.mailbody h2{
			text-align: center;
		}
		.mailbody p{
			font-size: 15px;
			line-height: 1.7;
		}

		.mailbody p.tip{
			color: #ff0000;
			font-weight: bold;
			font-size: 17px;
			margin-bottom: 20px;
		}
		.mailbody p.center{
			margin: 0;
			text-align: center;
			font-size: 14px;
		}

		.mailbody table{
			width: 100%;
			border: 2px solid #999;
			border-spacing: 0;
			margin: 30px 0;
		}
		.mailbody table tr td{
			padding: 10px;
			border-collapse: collapse;
			border: 1px solid #999;
			letter-spacing: 0.05em;
			font-size: 15px;
		}
		.mailbody span{
			margin: 50px 0;
			display: block;
		}
		.mailbody a{
			color: #4963ed;
			font-weight: bold;
			margin: 0 5px;
		}
	</style>
</head>
<body>
	<article class="mailbody">
		<h2>==自動發信功能，請勿直接回覆==聯絡表單成功送出通知信</h2>
        
		<p>親愛的民眾，您好：<br><br>
      
感謝您花時間來信洽詢。

針對您提出的意見或問題，我們將確認後主動回覆，並會盡可能的提供您需要的協助。
同時，感謝您對我們的信賴和肯定，祝您有個愉快的一天！<br><br>
        
		以下為您於「<?=$_setting_['WO_Title']?>」所留言的訊息：</p>
		<table cellpadding="0"  border-spacing="0" border-collapse="0">
			<tr>
				<td width="40%" bgcolor="#ebeefc">留言時間</td>
				<td width="60%"><?=$_html['Contact_Sdate']?></td>
			</tr>
			<tr>
				<td width="40%" bgcolor="#ebeefc">留言主旨</td>
				<td width="60%"><?=$_html['Contact_Title']?></td>
			</tr>
			<tr>
				<td width="40%" bgcolor="#ebeefc">留言內容</td>
				<td width="60%"><?=$_html['Contact_Content']?></td>
			</tr>
		</table>
		
		<span>祝 ! 使用愉快</span>
		<span>※為保護您的個資，本信件將不顯示您的基本資料※</span>
        
		<p class="tip">※本電子信箱為系統自動發送通知使用，請勿直接回覆， 如有任何疑問，歡迎來信以下客服專用信箱※</p>
		<p class="center"><?=$_setting_['WO_Name']?> </p>
		<p class="center">電話:<?=$_setting_['WO_Tel']?></p>
        <p class="center">信箱:<?=$_setting_['WO_Email']?></p>
	</article>
	
</body>
</html>