<?php require('header.php') ?>

	<article>
		<section class="bannerslider">
			<ul class="banner__slider">
				<!--banner圖 1600x800 -->
				<?php foreach( $_html_banner as $key => $val ){?>
					<li><a href="<?=$val['Banner_Link']?>" target="_blank" title=""><img src="<?=Banner_Url.'/'.$val['Banner_Mcp']?>" width="100%" alt="<?=$val['Banner_Title']?>(另開新視窗)"></a></li>
				<?php }?>
			</ul>
		</section>

		<section class="idxmarquee">
			<div class="container">
				<ul class="idxmarquee__list">
					<?php foreach( $_html_marquee as $key => $val ){?>
						<li><a href="<?=$val['Marquee_Link']?>" title="<?=$val['Marquee_Title']?>(另開新視窗)" target="_blank"><?=$val['Marquee_Title']?></a></li>
					<?php }?>
				</ul>
			</div>
			
		</section>

		<section id="main" class="mainC">
			<div class="container">


				<div class="idx idxnews" id="idxnews">
					<a class="go_header" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<h2 class="maintit maintit--news">最新消息</h2>
					<ul class="messageList">
						<?php foreach( $_html_new as $key => $val ){?>
							<li class="messageList__item">
								<div class="img" style="background-image: url(<?=!empty($val['News_Mcp'])?News_Url.$val['News_Mcp']:'images/nophoto.jpg'?>);"></div>
								<div class="content">
									<p class="date"><?=explode(" ",$val['News_PostDate'])[0]?></p>
									<a class="title" href="newsin.php?c=<?=OEncrypt('news_in='.$key.'_new' , 'newsin')?>" title="<?=$val['News_Title']?>"><?=$val['News_Title']?></a>
									<p class="content"><?=strip_tags(TurnSymbol($val['News_Content']))?></p>
									<a class="more" href="newsin.php?c=<?=OEncrypt('news_in='.$key.'_new' , 'newsin')?>" title="<?=$val['News_Title']?>">MORE</a>
								</div>
								
							</li>
						<?php }?>
					</ul>
				</div>
				<div class="idx idxservice" id="idxservice">
					
					<h2 class="maintit maintit-service">快速連結</h2>
					<ul class="idxservice__list">
						<?php foreach( $_html_fast_links as $key => $val ){ ?>
							<li class="idxservice__list__item"><a href="<?=$val['IL_Content']?>"  title="<?=$val['IL_Title']?>"><img src="<?=Index_Links_Url.$val['IL_Mcp']?>" alt=""><p><?=$val['IL_Title']?></p></a></li>
						<?php } ?>
						<!--
						<li class="idxservice__list__item"><a href="opc_info.php"  title="門診表"><img src="images/link01_C.png" alt=""><p>門診表</p></a></li>
						<li class="idxservice__list__item"><a href="news.php?c=<?=OEncrypt('news_cat=hot' , 'newscat')?>"  title="熱門議題"><img src="images/link02_C.png" alt=""><p>熱門議題</p></a></li>
						<li class="idxservice__list__item"><a href="<?=$_setting2_['Setting_SC1_Link']?>"  title="<?=$_setting2_['Setting_SC1_Name']?>"><img src="images/link03_C.png" alt=""><p><?=$_setting2_['Setting_SC1_Name']?></p></a></li>
						<li class="idxservice__list__item"><a href="<?=$_setting2_['Setting_SC2_Link']?>"  title="<?=$_setting2_['Setting_SC2_Name']?>"><img src="images/link04_C.png" alt=""><p><?=$_setting2_['Setting_SC2_Name']?></p></a></li>
						<li class="idxservice__list__item"><a href="opc_info.php"  title="門診表"><img src="images/link01_C.png" alt=""><p>門診表</p></a></li>
						<li class="idxservice__list__item"><a href="news.php?c=<?=OEncrypt('news_cat=hot' , 'newscat')?>"  title="熱門議題"><img src="images/link02_C.png" alt=""><p>熱門議題</p></a></li>
						-->
					</ul>
				</div>
				

				
			</div>
		</section>

		<section id="main" class="mainC">
			<div class="container">
				
				<div class="idx idxhot" id="idxhot">
				
					<h2 class="maintit maintit--hot">熱門議題</h2>

						<ul class="hotList" id="idxnews002">
							<li class="hotList__item hotList__item--title">
								<p class="date">日期</p>
								<a class="title">標題</a>
							</li>
							<?php foreach( $_html_hot as $key => $val ){?>
								<li class="hotList__item">
									<p class="date"><?=explode(" ",$val['News_PostDate'])[0]?></p>
									<a class="title" href="newsin.php?c=<?=OEncrypt('news_in='.$key.'_hot' , 'newsin')?>" title="<?=$val['News_Title']?>"><?=$val['News_Title']?></a>
								</li>
							<?php }?>
							<a class="more" href="news.php?c=<?=OEncrypt('news_cat=hot' , 'newscat')?>" title="更多熱門議題"><i class="fas fa-plus"></i>+更多</a>
						</ul>
				</div>

				<div class="idx idxvideo" id="idxvideo">
					<h2 class="maintit maintit--video">影音專區</h2>
					<div class="video">
						<iframe src="https://www.youtube.com/embed/<?=$_html_video['Video_YouTube']?>" frameborder="0" allowfullscreen="" title="影片標題"></iframe>
					</div>
				</div>
			</div>
		</section>


		
		<section class="mainC">
			<div class="container">
				<div class="idx idxlink" id="idxlink">
					<h2 class="maintit maintit--link">網網相連</h2>
					<ul class="idxlink__list">
						<!--圖 189x76 -->
						<?php foreach( $_html_links as $key => $val ){?>
						<li><a href="<?=$val['Links_Link']?>" target="_blank" title="另開新分頁前往<?=$val['Links_Title']?>" style="background-image: url(<?=Links_Url.'/'.$val['Links_Mcp']?>);"><?=$val['Links_Title']?></a></li>
						<?php }?>
					</ul>
					<a class="more" href="links.php" title="看更多連結"><i class="fas fa-plus"></i>+更多</a>
				</div>
			</div>
			<br>
		</section>


		
	</article>

	<?php require('footer.php')?>