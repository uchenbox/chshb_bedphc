<?php require_once("include/web.config.php");

$Input = GDC( $_GET['c'] , 'healthin' );

$_ID		= explode('_',$Input['health_in']);

$_Result 		= $CM->GET_HEALTH_LIST( $_ID[0] , '' , $PageData );

$_html			= $_Result['Data'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];

$_Title = $_html[$_ID[0]]['Topic_Title'];
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="" rel="nofollow"  itemprop="url">
								<span itemprop="title">健康主題</span>
							</a> ›
						</li>
						<?php if( $_html[$_ID[0]]['TopicC_Lv']=='2' ){?>
							<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
								<a href="health.php?c=<?=OEncrypt('health_cat='.$_html[$_ID[0]]['TopicC_UpMID'].'_all' , 'healthcat')?>" rel="nofollow"  itemprop="url">
									<span itemprop="title"><?=$_ID[1]?></span>
								</a> ›
							</li>
							<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
								<a href="health.php?c=<?=OEncrypt('health_cat='.$_html[$_ID[0]]['TopicC_ID'] , 'healthcat')?>" rel="nofollow"  itemprop="url">
									<span itemprop="title"><?=$_html[$_ID[0]]['TopicC_Name']?></span>
								</a>
							</li>
						<?php }?>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('health_aside.php')?>


				<div class="main">
					<h3 class="mainTitle"><?=$_ID[1]?></h3>
					<div class="mainContent">
						<h4 class="photoTitle"><?=$_html[$_ID[0]]['Topic_Title']?></h4>
						<p class="photoDate"><?=$_html[$_ID[0]]['Topic_Postdate']?></p>
						<div class="textcontent">
							<?=TurnSymbol($_html[$_ID[0]]['Topic_Content'])?>
						</div>
						
						<button class="btnstyle_return" onClick="location='health.php?c=<?=OEncrypt('health_cat='.$_html[$_ID[0]]['TopicC_ID'] , 'healthcat')?>';">回列表</button>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>