</div>
</div>
<footer>
	<div class="footerLink">
		<div class="container">
			<button class="footerBtn footerBtn--zhtw" data-switch="1" title="關閉選單">▼ 關閉選單</button>
			<ul class="footerLink__list footerLink__list--zhtw">
				<li class="footerLink__list__item">
					<p>關於本所</p>
					<a href="about.php" title="本所簡介">本所簡介</a>
					<a href="business.php" title="業務職掌">業務職掌</a>
					<a href="glory.php" title="榮耀軌跡">榮耀軌跡</a>
					<a href="headman.php" title="歷任首長">歷任首長</a>
					<a href="traffic.php" title="交通資訊">交通資訊</a>
					<a href="album.php" title="活動花絮">活動花絮</a>
					<a href="<?=$_setting2_['Setting_OverviewofPHI']?>" title="另開新視窗前往<?=$_setting_['WO_Title']?>公衛指標概覽" target="_blank">公衛指標概覽</a>
					<a href="contact.php" title="民意信箱">民意信箱</a>
				</li>
				<li class="footerLink__list__item">
					<p>醫療及社區資源</p>
					<a href="m_resource.php" title="本鄉鎮醫療資源">本鄉鎮醫療資源</a>
					<a href="c_resource.php" title="社區資源">社區資源</a>
				</li>
				<li class="footerLink__list__item">
					<p>便民服務</p>
					<a href="opc_info.php" title="門診資訊">門診資訊</a>
					<a href="service_process.php" title="為民服務流程">為民服務流程</a>
					<?php if( $GOV_TF==true ){?>
						<a href="gov_info.php" title="政府公開資訊">政府公開資訊</a>
					<?php }?>
					<a href="service_plan.php" title="服務品質計畫">服務品質計畫</a>
					<a href="download_all.php" title="檔案下載">檔案下載</a>
					<?php if( $VIDEO_TF==true ){?>
						<a href="video.php" title="影音資料">影音資料</a>
					<?php }?>
					<a href="links.php" title="網網相連">網網相連</a>
				</li>
				<li class="footerLink__list__item">
					<p>訊息專區</p>
					<a href="news.php?c=<?=OEncrypt('news_cat=new' , 'newscat')?>" title="最新消息">最新消息</a>
					<a href="news.php?c=<?=OEncrypt('news_cat=hot' , 'newscat')?>" title="熱門議題">熱門議題</a>
				</li>
				<li class="footerLink__list__item">
					<p>志工專區</p>
					<a href="volunteer.php" title="志工服務">志工服務</a>
					<a href="volunteer_album.php" title="志工花絮">志工花絮</a>
				</li>
				<li class="footerLink__list__item">
					<p>健康主題</p>
					<?php foreach( $_html_HEALTH_MENU as $key => $val ){?>
						<a href="healthin.php?c=<?=OEncrypt('healthin='.$key , 'healthin')?>" title="<?=$val['Topic_Title']?>"><?=$val['Topic_Title']?></a>
					<?php }?>
				</li>
			</ul>
		</div>
	</div>
	<div class="footer">
		<div class="container">
			<a id="Accesskey_Z" accesskey="Z" href="#Accesskey_Z" title="版尾聯絡資訊區，此區塊列有本網站的聯絡資訊">:::</a>
			<a href="privacy.php" title="隱私權保護及資訊安全宣告">隱私權保護及資訊安全宣告</a>　│　<a href="prohibit.php" title="禁止轉錄聲明">禁止轉錄聲明</a>
			</br></br>
			<div class="footer__imfo">
				<p>© 2019 <?=$_setting_['WO_Name']?></p>
				<p>
					<span>地址：<?=$_setting_['WO_Addr'].$_setting_['WO_Addr1'].$_setting_['WO_Addr2']?></span>
					<span>電話：<?=$_setting_['WO_Tel']?></span>
					<span>傳真：<?=$_setting_['WO_Fax']?></span>
					<span>e-mail：<?=$_setting_['WO_Email']?></span>
				</p>
				<p class="statement">【聲明禁止任何網際網路服務業者轉錄其網路資訊內容供人點閱】</p>
			</div>
			<div class="AA">
				<a  href="https://www.handicap-free.nat.gov.tw/Applications/Detail?category=20190930190731" title="無障礙網站"><img src="images/AA.png" alt="通過AA檢測等級無障礙網頁檢測"></a>
			</div>
			
		</div>
	</div>

</footer>
