<aside class="aside">
	<p class="aside__tit">醫療及社區資源</p>
	<ul class="mnav">
		<li class="mnav__item" titlex="本鄉鎮醫療資源"><a href="" title="本鄉鎮醫療資源(點擊展開次選單)">本鄉鎮醫療資源</a>
			<ul class="mnav__item__list">
				<li class="mnav__item <?=!isset($_ID)&&PHP_NAME=='m_resource'?'current':''?>"><a href="m_resource.php"title="本鄉鎮醫療資源全部資源">全部</a></li>
				<?php foreach( $_html_MR as $key => $val ){?>
					<li class="mnav__item <?=$_ID==$key?'current':''?>"><a href="m_resourcein.php?c=<?=OEncrypt('mr_id='.$key , 'mrin')?>" title="<?=$val['MR_Title']?>"><?=$val['MR_Title']?></a></li>
				<?php }?>
			</ul>
		</li>
		<li class="mnav__item <?=PHP_NAME=='c_resource'?'current':''?>"><a href="c_resource.php" title="社區資源">社區資源</a></li>
	</ul>
</aside>