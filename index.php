<?php require_once("include/web.config.php");

if( !empty($_setting2_['Setting_SetIndex']) ){
	
	header("Location: ".$_setting2_['Setting_SetIndex']); 
}


$_Result 		= $CM->GET_BANNER_LIST();
$_html_banner 	= $_Result['Data'];

$_Result 		= $CM->GET_MARQUEE_LIST();
$_html_marquee 	= $_Result['Data'];

$_Result 		= $CM->GET_NEWS_INDEX();
$_html_new	 	= $_Result['Data_new'];
$_html_hot	 	= $_Result['Data_hot'];

$_Result 		= $CM->GET_LINK_INDEX();
$_html_links 	= $_Result['Data'];

$_html_fast_links 		= $CM->GET_FAST_LINK_INDEX()['Data'];

//版型D才有
$_html_video 		= $CM->GET_INDEX_VIDEO()['Data'];

$_Title = "首頁";
?>
<!DOCTYPE html>
<html  lang="zh-TW">


<head>
	<?php require('head.php')?>
	<link rel="stylesheet" type="text/css" href="stylesheets/index.css?v=<?=$version?>" />
	
	<script>
		$(function(){
			var $li = $('ul.idxnews__tab>li>a');
			//$($li. eq(0) .addClass('active').find('a').attr('href')).siblings('.idxnews__list').css('opacity',1).hide();
			$($li. eq(0) .parent('.idxnews__tab__item').addClass('active').attr('href')).siblings('.idxnews__list').css('opacity',1).hide();

			$li.focus(function(){
				$($(this).attr('href')).fadeIn().siblings('.idxnews__list').hide();
				$('.idxnews__tab__item').removeClass('active');
				$(this).parent('.idxnews__tab__item').addClass('active');
			});
		});
	</script>
</head>
<body class="Index">

	<?php require( $_CurrentStyle['index'].".php");?>
	
	
</body>
</html>