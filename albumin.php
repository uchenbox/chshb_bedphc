<?php require_once("include/web.config.php");

$PageData['p'] = $_GET['p'];
$Input = GDC( $_GET['c'] , 'albumin' );

$_ID		= $Input['albumin_id'];

//----------------------------相簿----------------------------
$_Result 		= $CM->GET_ALBUM_SINGLE( $_ID );

$_html 			= $_Result['Data'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
//------------------------------------------------------------
//--------------------------照片list--------------------------

$_Result 		= $CM->GET_ALBUM_DATA( $_ID , $PageData );

$Pages_Data 	= $_Result['PageData'];
$_photo 		= $_Result['Data'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
//------------------------------------------------------------
// print_r($_photo);
$_Title = $_html['Album_Title'];
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<link rel="stylesheet" href="stylesheets/baguetteBox.min.css">
	<script type="text/javascript"  src="js/baguetteBox.js"></script>
	<script>
		$(document).ready(function(){
			baguetteBox.run('.photolist', {});
		});
	</script>
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="about.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">關於本所</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="album.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">活動花絮</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('about_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">活動花絮</h2>
					<div class="mainContent">
						<h3 class="photoTitle"><?=$_html['Album_Title']?></h3>
						<p class="photoDate"><?=$_html['Album_Postdate']?></p>
						<ul class="photolist">
							<?php foreach( $_photo as $key => $val ){?>
								<li class="photolist__item">
									<a class="photolist__item__link" data-caption="<?=$val['Albuml_Title']?>"  title="<?=$val['Albuml_Title']?>.<?=explode('.', $val['Albuml_Img'])[1]?>(點擊放大瀏覽)" href="<?=Album_Url.$val['Album_ID'].'/'.$val['Albuml_Img']?>" >
										<div class="img" style="background-image: url('<?=Album_Url.$val['Album_ID'].'/'.$val['Albuml_Img']?>');"></div>
										<p class="photolist__item__content"><?=$_html['Album_Title']?>照片</p>
									</a>
								</li>
							<?php }?>
						</ul>
					</div>
					
					<?php require('page.php')?>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>