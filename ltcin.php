<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">中央內容區塊，為本頁主要內容區</a>
					<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="agefriendly.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">健康主題</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="ltc.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">長期照護-C據點A</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="" rel="nofollow"  itemprop="url">
								<span itemprop="title">【彰化縣 二林鎮】巷弄長照站- C 級據點</span>
							</a>
						</li>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('health_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">長期照護-C據點</h2>
					<div class="mainContent">
						<h3 class="photoTitle">【彰化縣 二林鎮】巷弄長照站- C 級據點</h3>
						<p class="photoDate">2019-07-08</p>
						<div class="textcontent">
							【彰化縣 彰化市】巷弄長照站- C 級據點<br>【彰化縣 彰化市】巷弄長照站- C 級據點
						</div>
						<button class="btnstyle_return">回列表</button>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>