<?php require('header.php') ?>

	<article>
		<section class="bannerslider">
			<ul class="banner__slider">
				<!--banner圖 1600x800 -->
				<?php foreach( $_html_banner as $key => $val ){?>
					<li><a href="<?=$val['Banner_Link']?>" target="_blank" title=""><img src="<?=Banner_Url.'/'.$val['Banner_Mcp']?>" width="100%" alt="<?=$val['Banner_Title']?>(另開新視窗)"></a></li>
				<?php }?>
			</ul>
		</section>

		<section class="idxmarquee">
			<div class="container">
				<ul class="idxmarquee__list">
					<?php foreach( $_html_marquee as $key => $val ){?>
						<li><a href="<?=$val['Marquee_Link']?>" title="<?=$val['Marquee_Title']?>(另開新視窗)" target="_blank"><?=$val['Marquee_Title']?></a></li>
					<?php }?>
				</ul>
			</div>
			
		</section>

		<section id="main" class="mainC">
			<div class="container">
				<div class="idx idxservice" id="idxservice">
					<a class="go_header" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<h2 class="maintit maintit-service">快速連結</h2>
					<ul class="idxservice__list">
						<?php foreach( $_html_fast_links as $key => $val ){ ?>
							<li class="idxservice__list__item"><a href="<?=$val['IL_Content']?>"  title="<?=$val['IL_Title']?>"><img src="<?=Index_Links_Url.$val['IL_Mcp']?>" alt=""><p><?=$val['IL_Title']?></p></a></li>
						<?php } ?>
						<!--
						<li class="idxservice__list__item"><a href="opc_info.php"  title="門診表"><img src="images/link01_C.png" alt=""><p>門診表</p></a></li>
						<li class="idxservice__list__item"><a href="news.php?c=<?=OEncrypt('news_cat=hot' , 'newscat')?>"  title="熱門議題"><img src="images/link02_C.png" alt=""><p>熱門議題</p></a></li>
						<li class="idxservice__list__item"><a href="<?=$_setting2_['Setting_SC1_Link']?>"  title="<?=$_setting2_['Setting_SC1_Name']?>"><img src="images/link03_C.png" alt=""><p><?=$_setting2_['Setting_SC1_Name']?></p></a></li>
						<li class="idxservice__list__item"><a href="<?=$_setting2_['Setting_SC2_Link']?>"  title="<?=$_setting2_['Setting_SC2_Name']?>"><img src="images/link04_C.png" alt=""><p><?=$_setting2_['Setting_SC2_Name']?></p></a></li>
						<li class="idxservice__list__item"><a href="opc_info.php"  title="門診表"><img src="images/link01_C.png" alt=""><p>門診表</p></a></li>
						<li class="idxservice__list__item"><a href="news.php?c=<?=OEncrypt('news_cat=hot' , 'newscat')?>"  title="熱門議題"><img src="images/link02_C.png" alt=""><p>熱門議題</p></a></li>
						-->
					</ul>
				</div>
				

				<div class="idx idxnews" id="idxnews">
				
					<h2 class="maintit maintit--news">訊息專區</h2>
					<ul class="idxnews__tab">
						<li class="idxnews__tab__item active" ><a onclick="return false;" href="#idxnews001" data-list="" title="最新消息" >最新消息</a></li>
						<ul class="idxnews__list active" id="idxnews001">
							<?php foreach( $_html_new as $key => $val ){?>
								<li class="idxnews__list__item">
									<p class="date"><?=explode(" ",$val['News_PostDate'])[0]?></p>
									<a href="newsin.php?c=<?=OEncrypt('news_in='.$key.'_new' , 'newsin')?>" title="<?=$val['News_Title']?>"><?=$val['News_Title']?></a>
								</li>
							<?php }?>
							<a class="more" href="news.php?c=<?=OEncrypt('news_cat=new' , 'newscat')?>" title="更多最新消息"><i class="fas fa-plus"></i>+更多</a>
						</ul>
						<li class="idxnews__tab__item idxnews__tab__item--right" ><a onclick="return false;" href="#idxnews002" data-list="" title="熱門議題">熱門議題</a></li>
						<ul class="idxnews__list" id="idxnews002">
							<?php foreach( $_html_hot as $key => $val ){?>
								<li class="idxnews__list__item">
									<p class="date"><?=explode(" ",$val['News_PostDate'])[0]?></p>
									<a href="newsin.php?c=<?=OEncrypt('news_in='.$key.'_hot' , 'newsin')?>" title="<?=$val['News_Title']?>"><?=$val['News_Title']?></a>
								</li>
							<?php }?>
							<a class="more" href="news.php?c=<?=OEncrypt('news_cat=hot' , 'newscat')?>" title="更多熱門議題"><i class="fas fa-plus"></i>+更多</a>
						</ul>
					</ul>
				</div>
			</div>
		</section>


		
		<section class="mainC mainC--gray">
			<div class="container">
				<div class="idx idxlink" id="idxlink">
					<h2 class="maintit maintit--link">網網相連</h2>
					<ul class="idxlink__list">
						<!--圖 189x76 -->
						<?php foreach( $_html_links as $key => $val ){?>
						<li><a href="<?=$val['Links_Link']?>" target="_blank" title="另開新分頁前往<?=$val['Links_Title']?>" style="background-image: url(<?=Links_Url.'/'.$val['Links_Mcp']?>);"><?=$val['Links_Title']?></a></li>
						<?php }?>
					</ul>
					<a class="more" href="links.php" title="看更多連結"><i class="fas fa-plus"></i>+更多</a>
				</div>

				<div class="idx idxsocial" id="idxsocial">
					<h2 class="maintit maintit--social">社群專區</h2>
					<div class="facebook" >
						<?php require('facebook.php')?>
					</div>
					
				</div>
			</div>
		</section>

		
	</article>

	<?php require('footer.php')?>