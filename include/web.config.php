<?php
if( !isset($_SESSION) ){
	session_start();
}

date_default_timezone_set("Asia/Taipei");//設置時區為台灣台北

define( 'PHP_SELF', basename($_SERVER['PHP_SELF']) );//目前檔案名稱含副檔名

define( 'PHP_NAME', basename($_SERVER['PHP_SELF'], '.php') );//目前檔案名稱不含副檔名

define( 'SYS_PATH', dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'system'.DIRECTORY_SEPARATOR );//系統路徑

define( 'WEB_PATH', dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR );//網頁路徑

define( 'WEB_URL', 'http://'.dirname($_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']).'/' );//網頁網址

define( 'WEB_Mail_URL', 'http://'.dirname($_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']).'/mailbody/' );//網頁網址

require_once(SYS_PATH.'include'.DIRECTORY_SEPARATOR.'class.MySQL.php');//載入MYSQL語法class

require_once(SYS_PATH.'include'.DIRECTORY_SEPARATOR.'class.MyCurl.php');//載入MyCurl語法class
require_once(SYS_PATH.'include'.DIRECTORY_SEPARATOR.'class.Pages.php');
require_once(SYS_PATH.'include'.DIRECTORY_SEPARATOR.'class.Upload.php');//載入上傳class
require_once(SYS_PATH.'include'.DIRECTORY_SEPARATOR.'class.Custom.php');//載入客製class
require_once(SYS_PATH.'config'.DIRECTORY_SEPARATOR.'cfg.picsize.php');
require_once(SYS_PATH.'config'.DIRECTORY_SEPARATOR.'cfg.states.php');//狀態
require_once(SYS_PATH.'config'.DIRECTORY_SEPARATOR.'cfg.turncode.php');
require_once(SYS_PATH.'sys_config.php');//載入MYSQL語法class
require_once(SYS_PATH.'plugins'.DIRECTORY_SEPARATOR.'PHPMailer'.DIRECTORY_SEPARATOR.'PHPMailerAutoload.php');//寄件class
require_once(SYS_PATH.'include'.DIRECTORY_SEPARATOR.'inc.connect.php');//載入連結MYSQL資訊

require_once(SYS_PATH.'include'.DIRECTORY_SEPARATOR.'inc.function.php');//載入方法
require_once(SYS_PATH.'include'.DIRECTORY_SEPARATOR.'class.Custom.php');//載入客製class

require_once(WEB_PATH.'include'.DIRECTORY_SEPARATOR.'web.function.php');//載入方法
require_once(WEB_PATH.'include'.DIRECTORY_SEPARATOR.'downloadfile.php');//檔案下載class
//require_once(WEB_PATH.'include'.DIRECTORY_SEPARATOR.'web.functionextend.php');//載入額外方法

ob_end_clean();

$db = new MySQL();
$db->Where = " WHERE Admin_ID = '2'";
$db->query_sql('sys_web_option', '*');
$_setting_ = $db->query_fetch();

$_setting_['keywords']		= $_setting_['WO_Keywords'];
$_setting_['description'] 	= $_setting_['WO_Description'];
$version					= $_setting_['WO_Version'];


$db->query_sql('web_setting', '*');
$_setting2_ = $db->query_fetch();

if( $_setting_['WO_Debug'] == 1 ){//網站Debug開啟

	error_reporting(1);
	ini_set('display_errors','On');
}else{
	
	error_reporting(1);
	ini_set('display_errors','Off');
}

$website_open = true;


if( $website_open ){
	
	$Now_Table		= $db->get_use_table();
	$contact_db		= 'web_contact';
	$news_db 		= 'news';
	$product_db 	= 'product';
	$about_db   	= 'web_about';
	$banner_db 		= 'web_banner';
	$catalog_db 	= 'web_catalog';
	$link_db 		= 'web_link';
	$qa_db 			= 'web_qa';
	$albums_db 		= 'albums';
	
	$new_Pre 		= 'News';
	$product_Pre	= 'Product';
	$productMcp_Pre	= 'PM';
	$about_Pre 		= 'About';
	$banner_Pre 	= 'Banner';
	$catalog_Pre 	= 'CA';
	$link_Pre  		= 'Link';
	$qa_Pre			= 'QA';
	$albums_Pre		= 'albums';
	$albumsMcp_Pre	= 'albums';
	
	define( 'IMG_PATH', WEB_PATH.'sys_images'.DIRECTORY_SEPARATOR.$Now_Table.DIRECTORY_SEPARATOR );//圖片路徑
	
	define( 'IMG_URL', WEB_URL.'sys_images/'.$Now_Table.'/' );//圖片網址
	
	define( 'FILE_PATH', WEB_PATH.'sys_files'.DIRECTORY_SEPARATOR.$Now_Table.DIRECTORY_SEPARATOR );//檔案路徑
	
	define( 'FILE_URL', WEB_URL.'sys_files/'.$Now_Table.'/' );//檔案網址
	
	
	define( 'Banner_Url', IMG_URL.'Banner/' );
	define( 'Links_Url', IMG_URL.'Links/' );
	define( 'Index_Links_Url', IMG_URL.'IL/' );
	define( 'News_Url', IMG_URL.'News/' );	
	define( 'Album_Url', IMG_URL.'Album/' );
	define( 'VTAlbum_Url', IMG_URL.'VTalbum/' );
	
	$CM 			= new Custom();
	
	$_Result 		= $CM->GET_MR_LIST( $_ID );
	$_html_MR		= $_Result['Data'];
	
	$_Result 		= $CM->GET_SP_LIST( $_ID );
	$_html_SP		= $_Result['Data'];
	
	$_Result 		= $CM->GET_GOV_LIST( $PageData );
	if( !empty($_Result['DataA']) ){
		
		$GOV_TF = true;
	}else if( empty($_Result['DataA']) ){
		
		$GOV_TF = false;
	}
	
	$_Result 		= $CM->GET_DOWNLOAD_CAT( );
	$_html_DL		= $_Result['Data'];
	
	$_Result 		= $CM->GET_VIDEO_LIST( $PageData );
	if( !empty($_Result['Data']) ){
		
		$VIDEO_TF = true;
	}else if( empty($_Result['Data']) ){
		
		$VIDEO_TF = false;
	}
	
	
	$_Result 		= $CM->GET_LINK_CAT( );
	$_html_LK		= $_Result['Data'];
	
	$_html_HEALTH_MENU 		= $CM->GET_HEALTH_MENU()['Data'];

	$_Type = $_setting2_['Setting_CurrentStyle']??'A';
	
	$_CurrentStyle = array();
	
	switch( $_Type ){
	
		case 'A':
			$_CurrentStyle['header'] 	= 'header_styleA';
			$_CurrentStyle['index'] 	= 'styleA';
			$_CurrentStyle['css'] 		= 'styleA';
		break;
		
		case 'B':
			$_CurrentStyle['header'] 	= 'header_styleB';
			$_CurrentStyle['index'] 	= 'styleB';
			$_CurrentStyle['css'] 		= 'styleB';
		break;
		
		case 'C':
			$_CurrentStyle['header'] 	= 'header_styleC';
			$_CurrentStyle['index'] 	= 'styleC';
			$_CurrentStyle['css'] 		= 'styleC';
		break;
		
		case 'D':
			$_CurrentStyle['header'] 	= 'header_styleD';
			$_CurrentStyle['index'] 	= 'styleD';
			$_CurrentStyle['css'] 		= 'styleD';
		break;
		
		case 'E':
			$_CurrentStyle['header'] 	= 'header_styleE';
			$_CurrentStyle['index'] 	= 'styleE';
			$_CurrentStyle['css'] 		= 'styleE';
		break;
	}

}
?>