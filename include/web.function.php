<?php
define("ST", "<script>");
define("SD", "</script>");

	//JS訊息,導向新網頁
	function JSAH( $STR, $PATH ){
		
		header("Content-Type:text/html; charset=utf-8");
		echo "<html lang='zh-TW'>".ST."alert('" .$STR. "');location.href='" .$PATH. "';".SD."<title>".$STR."</title><style>h1.bold{ font-weight:bold;}</style><body><h1 class='bold'>".$STR."</h1></body></html>";
		//echo "<script>alert('".$STR."')</script>";
		//echo "<script>window.location.href = '".$PATH."'</script>";
		exit;
	}
	
	function SafeFilter (&$arr) {
		
	   $ra=Array('/([\x00-\x08,\x0b-\x0c,\x0e-\x19])/','/script/','/javascript/','/vbscript/','/expression/','/applet/'
	   ,'/meta/','/xml/','/blink/','/link/','/style/','/embed/','/object/','/frame/','/layer/','/title/','/bgsound/'
	   ,'/base/','/onload/','/onunload/','/onchange/','/onsubmit/','/onreset/','/onselect/','/onblur/','/onfocus/',
	   '/onabort/','/onkeydown/','/onkeypress/','/onkeyup/','/onclick/','/ondblclick/','/onmousedown/','/onmousemove/'
	   ,'/onmouseout/','/onmouseover/','/onmouseup/','/onunload/','/%21/','/%22/','/%27/','/%28/','/%29/','/%2a/','/%2b/','/%2c/','/%5b/','/%5d/');
     
	   if (is_array($arr)) {
			
			foreach ($arr as $key => $value) {
				
				if (!is_array($value)) {
					
					if (!get_magic_quotes_gpc()) {             //不對magic_quotes_gpc轉義過的字元使用addslashes(),避免雙重轉義。
					
						$value  = addslashes($value);           //給單引號（'）、雙引號（"）、反斜線（\）與 NUL（NULL 字元）加上反斜線轉義
					}
					
					$value       = preg_replace($ra,'',$value);     //刪除非列印字元，粗暴式過濾xss可疑字串
					$arr[$key]     = htmlentities(strip_tags($value) , ENT_QUOTES ); //去除 HTML 和 PHP 標記並轉換為 HTML 實體
				} else {
					SafeFilter($arr[$key]);
				}
			}
		}
	}
	
	function CHECK_FILTER(){
		

		$ra=Array('/([\x00-\x08,\x0b-\x0c,\x0e-\x19])/','/script/','/javascript/','/vbscript/','/expression/','/applet/'
	   ,'/meta/','/xml/','/blink/','/link/','/style/','/embed/','/object/','/frame/','/layer/','/title/','/bgsound/'
	   ,'/base/','/onload/','/onunload/','/onchange/','/onsubmit/','/onreset/','/onselect/','/onblur/','/onfocus/',
	   '/onabort/','/onkeydown/','/onkeypress/','/onkeyup/','/onclick/','/ondblclick/','/onmousedown/','/onmousemove/'
	   ,'/onmouseout/','/onmouseover/','/onmouseup/','/onunload/','/%21/','/%22/','/%27/','/%28/','/%29/','/%2a/','/%2b/','/%2c/','/%5b/','/%5d/');
	   
	    $value = 0;

		$value       = strpos( preg_replace($ra,'WHATAREYOUDOING',$_SERVER['REQUEST_URI']) , 'WHATAREYOUDOING');
		
		if( $value != 0 || $value > 0 ) {
		   return true;
		}else{
		   return false;
		}
		
	}
	
	function get_check( $str ){
		
		$str = trim($str);
		$str = htmlspecialchars($str, ENT_QUOTES);
		
		return $str;
	}
	
	function clean_spaces( $str ){//清除所有空白
		
		$str = trim($str);
		$str = preg_replace('/\s(?=)/', '', $str);
		
		return $str;
	}
	
	function TurnDateFormat($date , $sign = '-'){ //轉換日期格式
		
		$year=((int)substr($date,0,4));//取得年份
		$month=((int)substr($date,5,2));//取得月份
		$day=((int)substr($date,8,2));//取得幾號
		
		$date = $year.$sign.$month.$sign.$day;
		
		return $date;
	}
	
	function TurnDateFormat2($date , $sign = ' '){ //轉換日期格式
		
		$year=((int)substr($date,0,4));//取得年份
		$month=((int)substr($date,5,2));//取得月份
		$day=((int)substr($date,8,2));//取得幾號
		
		$MonthArray = array(
			'1'=>'JAN',
			'2'=>'FEB',
			'3'=>'MAR',
			'4'=>'APR',
			'5'=>'MAY',
			'6'=>'JUN',
			'7'=>'JUL',
			'8'=>'AUG',
			'9'=>'SEP',
			'10'=>'OCT',
			'11'=>'NOV',
			'12'=>'DEC',
			
		);
		
		$month = $MonthArray[$month];
	
		$date = $day.$sign.$month.$sign.$year;
		
		return $date;
	}
	//移除IMG STYLE
	function remove_imgstyle( $str ){
		
		preg_match_all( '/<img .* style="(.*?)"/i', $str, $match );
		
		if( is_array($match[1]) ){//拿掉文字編輯圖片的style
			
			foreach( $match[1] as $val ){
				
				$str = str_replace( $val, 'max-width: 100%;', $str );
			}
		}	
		
		return $str;
	}
	//移除IMG標籤
	function remove_imgTag( $str ){
		
		$str= preg_replace('/<\s*img\s+[^>]*?src\s*=\s*(\'|\")(.*?)\\1[^>]*?\/?\s*>/i', '', $str);	
		return $str;
	}
	//前端 - 單個檔案下載 ( 路徑 , 資料表前綴 , 資料表, 資料陣列)
	function file_download($FilePath , $DataPre , $DataDB, array $DataArray){
		
		if( is_file(ICONV_CODE( 'UTF8_TO_BIG5', $FilePath.$DataArray[$DataPre.'_File'] )) ){
			$Code = Turnencode('?t='.$DataDB.'&k='.$DataPre.'_ID&i='.$DataArray[$DataPre.'_ID'].'&f='.$DataPre.'_File'.'&p='.$FilePath, 'downfile');
		}
		
		return urlencode($Code);
	
	
	}
	
	//智付通加密
	function create_mpg_aes_encrypt ($parameter = "" , $key = "", $iv = "") {
		$return_str = '';
		if (!empty($parameter)) {
			//將參數經過 URL ENCODED QUERY STRING
			$return_str = http_build_query($parameter);
		}
		return trim(bin2hex(mcrypt_encrypt(MCRYPT_RIJNDAEL_128,
		$key,addpadding($return_str), MCRYPT_MODE_CBC, $iv)));
	 }
	function addpadding($string, $blocksize = 32) {
		$len = strlen($string);
		$pad = $blocksize - ($len % $blocksize);
		$string .= str_repeat(chr($pad), $pad);
		return $string;
	}
	
	//智付通解密
	function create_aes_decrypt($parameter = "", $key = "", $iv = "") {
		 return strippadding(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key,
		hex2bin($parameter), MCRYPT_MODE_CBC, $iv));
	}
	function strippadding($string) {
		 $slast = ord(substr($string, -1));
		 $slastc = chr($slast);
		 $pcheck = substr($string, -$slast);
		 if (preg_match("/$slastc{" . $slast . "}/", $string)) {
		 $string = substr($string, 0, strlen($string) - $slast);
		 return $string;
		 } else {
		 return false;
		 }
	} 
	
	function get_MailBody( $_Type , $data = ''){
		
		$_SSLURL = "https://bedphc.chshb.gov.tw/mailbody/";
		
		switch( $_Type ){
			
			//聯絡我們-會員
			case "custom_contact":
				$url = $_SSLURL.'Mail_custom_contact.php?c='.OEncrypt('v='.$data , 'custom_contact');
			break;
			
			//聯絡我們-管理者
			case "admin_contact":
				$url = $_SSLURL.'Mail_admin_contact.php?c='.OEncrypt('v='.$data , 'admin_contact');
			break;
		}
		
		$ch = curl_init();
	
		curl_setopt($ch , CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch , CURLOPT_HEADER, false);
		curl_setopt($ch , CURLOPT_NOBODY , false);
		curl_setopt($ch , CURLOPT_URL , $url );
		
		
		$result = curl_exec($ch);
	
		curl_close($ch);
		
		return $result;
	
	
	}
	
	function GET_HEAD_KD( $Input , $type = '' ){
	
		global $_setting_;
		
		if( !empty($Input) ){
			
			if( $type == 'K' ){
				$rs= $_setting_['WO_Keywords'] ? ','.strip_tags($Input) : strip_tags($Input);
			}else{
				$rs= $_setting_['WO_Description'] ? ' '.strip_tags($Input) : strip_tags($Input);
			}
			
		}
		
		return $rs;
	}
	
	function GET_SETTING( $Input = array() ){
		
		global $setting_db;
		
		$db = new MySQL();
		$sn = 0;
		
		if( !empty($Input) ) {
			
			foreach( $Input as $key ){
				
				if( $sn == 0 ) {
					
					$Sel = $key;
				}else{
					
					$Sel .= ','.$key;
				}
				
				$sn++;
			}
		}else{
			
			$Sel = '*';
		}
		
		$db->query_sql( $setting_db , $Sel );
		
		if( $row = $db->query_fetch() ){
			
			$_rs = $row;
			
		}
		
		return $_rs;
	}	
?>