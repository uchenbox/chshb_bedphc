<?php require_once("include/web.config.php");

$Input = GDC( $_GET['c'] , 'sqin' );

$_SQ		= $Input['sq_id'];

$_Result 		= $CM->GET_SQ_LIST( $_SQ, $PageData );

$_html			= $_Result['Data'];

$_setting_['WO_Keywords'] 		.= $_Result['SEO']['WO_Keywords'];
$_setting_['WO_Description'] 	.= $_Result['SEO']['WO_Description'];

$_Title = $_html[$_SQ]['SQplan_Title'];
?>
<!DOCTYPE html>
<html lang="zh-TW">
<head>
	<?php require('head.php')?>
	<!--<link rel="stylesheet" type="text/css" href="stylesheets/layout.css?v=<?=$version?>" />-->
</head>
<body>

	<div class="Wrapper">
		<div class="Wrapper__mask"></div>
		<?php require('header.php') ?>

		<article class="layout" id="main">
			<div class="container">
				<section class="bread">
					<a class="go_header_layout" id="Accesskey_M" accesskey="M" href="#Accesskey_M" title="中央內容區塊，為本頁主要內容區">:::</a>
					<ul class="breadcrumb">
						<ul class="breadcrumb">
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="index.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">首頁</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="opc_info.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">便民服務</span>
							</a> ›
						</li>
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="service_plan.php" rel="nofollow"  itemprop="url">
								<span itemprop="title">服務品質計畫</span>
							</a>
						</li> ›
						<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="#" rel="nofollow"  itemprop="url">
								<span itemprop="title"><?=$_html[$_SQ]['SQplan_Title']?></span>
							</a>
						</li>
					</ul>
					</ul>
					<?php require('sociallink.php')?>
				</section>

				<section class="mainbody">
				
				<?php require('service_aside.php')?>


				<div class="main">
					<h2 class="mainTitle">服務品質計畫</h2>
					<div class="mainContent">
						<h3 class="photoTitle"><?=$_html[$_SQ]['SQplan_Title']?></h3><br>
						<div class="textcontent">
							<?=TurnSymbol($_html[$_SQ]['SQplan_Content'])?>
							
						</div>
						<button class="btnstyle_return" onClick="location='service_plan.php';">回列表</button>
					</div>
				</div>
				
			
			</section>
			</div>

			

			
			
		</article>

		<?php require('footer.php')?>


	</div>
	
</body>
</html>